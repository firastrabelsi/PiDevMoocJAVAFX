/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.ENTITIES;

import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

/**
 *
 * @author pc
 */
public class organisme {
    
    private int id;
    private String UserName;
    private String Email;
    private String Password;
    private String adresse;
    private String nomDeLaSociete;
    private int num;
    private String site;
    private String certificat;
    private String url;
    
 
    public organisme() 
    {}
    public organisme( String UserName, String Email ,String Password,String adresse,String nomDeLaSociete,String url,String site,String certificat,int num) {
     
        this.UserName = UserName;
        this.Email = Email;
        this.Password = Password;
        this.adresse = adresse;
        this.nomDeLaSociete = nomDeLaSociete;
        this.num = num;
        this.site = site;
        this.certificat = certificat;
        this.url = url;
            
     }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getCertificat() {
        return certificat;
    }

    public void setCertificat(String certificat) {
        this.certificat = certificat;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNomDeLaSociete() {
        return nomDeLaSociete;
    }

    public void setNomDeLaSociete(String nomDeLaSociete) {
        this.nomDeLaSociete = nomDeLaSociete;
    }
     

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

  

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

   

   

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }
    
        @Override
    public int hashCode() {
        int hash = 1;
        return hash;
    }
/*
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final apprenant other = (apprenant) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.Nom, other.Nom)) {
            return false;
        }
        if (!Objects.equals(this.Prenom, other.Prenom)) {
            return false;
        }
        if (!Objects.equals(this.UserName, other.UserName)) {
            return false;
        }
        if (!Objects.equals(this.Password, other.Password)) {
            return false;
        }
        return true;
    }
    
*/    
    @Override
    public String toString()
    {
       return "Apprenant{" + "id=" + id + ", UserName=" + UserName + ", Adresse=" + adresse + ", Nom de la societe=" + nomDeLaSociete + ", E_mail=" + 
              Email +  '}';
    }

   
     
    
}

    
    

