/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.ENTITIES;

import java.util.Objects;

/**
 *
 * @author Anouar
 */
public class SuivCours {
    private int id;
    private Cours cours;
    private apprenant apprenant;
    private int evaluation;
    private String nomcors;
    private String  nomappren;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cours getCours() {
        return cours;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }

    public apprenant getApprenant() {
        return apprenant;
    }

    public void setApprenant(apprenant apprenant) {
        this.apprenant = apprenant;
    }

    public int getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(int evaluation) {
        this.evaluation = evaluation;
    }

    public SuivCours() {
    }

    public SuivCours(int id, Cours cours, apprenant apprenant) {
        this.id = id;
        this.cours = cours;
        this.apprenant = apprenant;
    }

    @Override
    public String toString() {
        return "SuivCours{" + "id=" + id + ", cours=" + cours + ", apprenant=" + apprenant + ", evaluation=" + evaluation + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        hash = 97 * hash + Objects.hashCode(this.cours);
        hash = 97 * hash + Objects.hashCode(this.apprenant);
        hash = 97 * hash + this.evaluation;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SuivCours other = (SuivCours) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.cours, other.cours)) {
            return false;
        }
        if (!Objects.equals(this.apprenant, other.apprenant)) {
            return false;
        }
        if (this.evaluation != other.evaluation) {
            return false;
        }
        return true;
    }

    public String getNomcors() {
        return nomcors;
    }

    public void setNomcors(String nomcors) {
        this.nomcors = nomcors;
    }

    public String getNomappren() {
        return nomappren;
    }

    public void setNomappren(String nomappren) {
        this.nomappren = nomappren;
    }

   
   
    
    
}
