/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.ENTITIES;

import java.util.Objects;

/**
 *
 * @author pc
 */
public class apprenant {
    
    private int id;
    private String UserName;
    private String Nom;
    private String Prenom;
    private String Email;
    private String Password;
    private String adresse;
    
    
     public apprenant( String UserName, String Nom , String Prenom, String Email ,String Password,String adresse) {
     
        this.UserName = UserName;
        this.Nom = Nom;
        this.Prenom = Prenom;
        this.Email = Email;
        this.Password = Password;
        this.adresse = adresse;
       
       
     }



   
   
      public apprenant() {
    }

    public int getId() {
        return id;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setId(int id) {
        this.id = id;
    }

 
    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getNom() {
        return Nom;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public void setNom(String Nom) {
        this.Nom = Nom;
    }

    public String getPrenom() {
        return Prenom;
    }
      public String getUserName() {
        return UserName;
    }


    public void setPrenom(String Prenom) {
        this.Prenom = Prenom;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }
    
        @Override
    public int hashCode() {
        int hash = 1;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final apprenant other = (apprenant) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.Nom, other.Nom)) {
            return false;
        }
        if (!Objects.equals(this.Prenom, other.Prenom)) {
            return false;
        }
        if (!Objects.equals(this.UserName, other.UserName)) {
            return false;
        }
        if (!Objects.equals(this.Password, other.Password)) {
            return false;
        }
        return true;
    }
    
    
    @Override
    public String toString()
    {
       return "Apprenant{" + "id=" + id + ", UserName=" + UserName + ", Nom=" + Nom + ", Prenom=" + Prenom + ", E_mail=" + 
              Email +  '}';
    }

 
   
     
    
}
