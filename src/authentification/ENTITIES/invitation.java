/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.ENTITIES;

import java.sql.Date;

/**
 *
 * @author pc
 */
public class invitation {
    private int id;
    private int source;
    private int destination;
    private String dateInvitation;  

    @Override
    public String toString() {
        return "invitation{" + "id=" + id + ", source=" + source + ", destination=" + destination + ", dateInvitation=" + dateInvitation + ", etat=" + etat + '}';
    }

    public invitation(int id, int source, int destination, String dateInvitation, String etat) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.dateInvitation = dateInvitation;
        this.etat = etat;
    }
    
     public invitation() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public int getDestination() {
        return destination;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }

    public String getDateInvitation() {
        return dateInvitation;
    }

    public void setDateInvitation(String dateInvitation) {
        this.dateInvitation = dateInvitation;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
    private String etat;  
}
