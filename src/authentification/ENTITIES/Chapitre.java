/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.ENTITIES;

/**
 *
 * @author Dell
 */
public class Chapitre {

    private int id;
    private String nom;
    private String description;
    private Cours cour;

    public Chapitre() {
    }

    public Chapitre(String nom, String description) {
        this.nom = nom;
        this.description = description;
    }

    public Chapitre(String nom, String description, Cours cour) {
        this.nom = nom;
        this.description = description;
        this.cour = cour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Cours getCour() {
        return cour;
    }

    public void setCour(Cours cour) {
        this.cour = cour;
    }

    @Override
    public String toString() {
        return "Chapitre{" + "id=" + id + ", nom=" + nom + ", description=" + description + ", cour=" + cour + '}';
    }
    

}
