/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.ENTITIES;

import java.sql.Timestamp;

/**
 *
 * @author Aymen
 */
public class Formateur {
    
    public int id;
    public String nom;
    public String prenom;
    public String email;
    public String specialite;
    public int etat;
    public int note ;
    
    public String username;
    public String password;
    ////
    public Timestamp updated_at;
     
     public String name;
    public String path;
    ///

    public Formateur(int id, String nom, String email, int etat, int note, String username, String password) {
        this.id = id;
        this.nom = nom;
        this.email = email;
        this.etat = etat;
        this.note = note;
        this.username = username;
        this.password = password;
    }

    public Formateur() {
        this.etat = 0;
        this.note = 0;
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }


    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Formateur{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", specialite=" + specialite + ", etat=" + etat + ", note=" + note + ", username=" + username + ", password=" + password + ", updated_at=" + updated_at + ", name=" + name + ", path=" + path + '}';
    }

    public Formateur(int id, String nom, String prenom, String email, String specialite, int etat, int note, String username, String password, Timestamp updated_at, String name, String path) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.specialite = specialite;
        this.etat = etat;
        this.note = note;
        this.username = username;
        this.password = password;
        this.updated_at = updated_at;
        this.name = name;
        this.path = path;
    }
    

   

    ///////
    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    
    
    

    
    
}
