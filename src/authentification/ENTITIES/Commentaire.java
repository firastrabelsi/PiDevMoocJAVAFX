/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.ENTITIES;

import java.util.Date;

/**
 *
 * @author Bali Majdi
 */
public class Commentaire {
    
    private int id;
    private String contenu;
    private Date date;
    private Cours cours;
    private apprenant ut;

    public Commentaire(int id, String contenu, Date date, Cours cours, apprenant ut) {
        this.id = id;
        this.contenu = contenu;
        this.date = date;
        this.cours = cours;
        this.ut = ut;
    }
        public Commentaire(String contenu, Cours cours, apprenant ut) {
       
        this.contenu = contenu;
        
        this.cours = cours;
        this.ut = ut;
    }

 public Commentaire(int id,String contenu) {
       this.id = id;
        this.contenu = contenu;       
}
 
 public Commentaire(String contenu, Date date ,apprenant ut ) {
        this.ut = ut;
         this.date = date;
        this.contenu = contenu;       
}
 
 
 
  public Commentaire() {
       
}
    public Cours getCours() {
        return cours;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }

    public apprenant getUt() {
        return ut;
    }

    public void setUt(apprenant ut) {
        this.ut = ut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    @Override
    public String toString() {
        return ut.getNom()+"\n"+" "+contenu+"\n"+" "+date;
    }
}
