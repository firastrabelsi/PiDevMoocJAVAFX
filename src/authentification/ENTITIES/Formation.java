/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.ENTITIES;

import java.util.List;

/**
 *
 * @author Dell
 */
public class Formation {

    private int IdForm;
    private String nomFormation;
    private int duree;
    private String level;
   private List<Cours> cours;

    public Formation() {
    }

    public Formation(String nomFormation, int duree, String level) {
        this.nomFormation = nomFormation;
        this.duree = duree;
        this.level = level;
    }
    

    public int getIdForm() {
        return IdForm;
    }

    public void setIdForm(int IdForm) {
        this.IdForm = IdForm;
    }

    public String getNomFormation() {
        return nomFormation;
    }

    public void setNomFormation(String nomFormation) {
        this.nomFormation = nomFormation;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<Cours> getCours() {
        return cours;
    }

    public void setCours(List<Cours> cours) {
        this.cours = cours;
    }

    public Formation(int IdForm, String nomFormation, int duree, String level, List<Cours> cours) {
        this.IdForm = IdForm;
        this.nomFormation = nomFormation;
        this.duree = duree;
        this.level = level;
        this.cours = cours;
    }

    @Override
    public String toString() {
        return "FormationDao{" + "IdForm=" + IdForm + ", nomFormation=" + nomFormation + ", duree=" + duree + ", level=" + level + ", cours=" + cours + '}';
    }
    

    public Formation(int IdForm, String nomFormation, int duree, String level) {
        this.IdForm = IdForm;
        this.nomFormation = nomFormation;
        this.duree = duree;
        this.level = level;
    }

  //  @Override
   // public String toString() {
    //    return "Formation{" + "IdForm=" + IdForm + ", nomFormation=" + nomFormation + ", duree=" + duree + ", level=" + level + '}';
   // }

}
