package authentification.ENTITIES;

import java.sql.Date;
import java.util.Objects;

public class Cours {

    private int id;
    private String titre;
    private String description;
    private int dureedecours;
    private String difficulte;
    private String etat;
    private Categories categorie;
    private Formateur formateur;
    private apprenant appren;
    private organisme organi;
    private int vedio_id;
    private float note;
    private int nbVote;

    public float getNote() {
        return note;
    }

    public void setNote(float note) {
        this.note = note;
    }

    public int getNbVote() {
        return nbVote;
    }

    public void setNbVote(int nbVote) {
        this.nbVote = nbVote;
    }

    public Cours() {
    }

    public Cours(String titre, String description, int dureedecours, String difficulte, Categories categorie, Formateur formateur, organisme organi) {
        this.titre = titre;
        this.description = description;
        this.dureedecours = dureedecours;
        this.difficulte = difficulte;
        this.categorie = categorie;
        this.formateur = formateur;
        this.organi = organi;
    }

    public Cours(int id, String titre, String description, int dureedecours, String difficulte, String etat, Categories categorie, Formateur formateur, int vedio_id) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.dureedecours = dureedecours;
        this.difficulte = difficulte;
        this.etat = etat;
        this.categorie = categorie;
        this.formateur = formateur;
        this.vedio_id = vedio_id;
    }

    public Cours(int id, String titre, String description, int dureedecours, String difficulte, String etat, Categories categorie, apprenant appren) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.dureedecours = dureedecours;
        this.difficulte = difficulte;
        this.etat = etat;
        this.categorie = categorie;
        this.appren = appren;
    }

    public Cours(int id, String titre, String description, int dureedecours, String difficulte, String etat, Categories categorie, organisme organi, int vedio_id) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.dureedecours = dureedecours;
        this.difficulte = difficulte;
        this.etat = etat;
        this.categorie = categorie;
        this.organi = organi;
        this.vedio_id = vedio_id;
    }

    public Cours(String titre, String description, int dureedecours, String difficulte, String etat, Categories categorie, Formateur formateur, apprenant appren, organisme organi) {
        this.titre = titre;
        this.description = description;
        this.dureedecours = dureedecours;
        this.difficulte = difficulte;
        this.etat = etat;
        this.categorie = categorie;
        this.formateur = formateur;
        this.appren = appren;
        this.organi = organi;
    }
    
    public Cours(float note, int nbVote){
        this.note = note;
        this.note = note;        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDureedecours() {
        return dureedecours;
    }

    public void setDureedecours(int dureedecours) {
        this.dureedecours = dureedecours;
    }

    public String getDifficulte() {
        return difficulte;
    }

    public void setDifficulte(String difficulte) {
        this.difficulte = difficulte;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Categories getCategorie() {
        return categorie;
    }

    public void setCategorie(Categories categorie) {
        this.categorie = categorie;
    }

    public Formateur getFormateur() {
        return formateur;
    }

    public void setFormateur(Formateur formateur) {
        this.formateur = formateur;
    }

    public apprenant getAppren() {
        return appren;
    }

    public void setAppren(apprenant appren) {
        this.appren = appren;
    }

    public organisme getOrgani() {
        return organi;
    }

    public void setOrgani(organisme organi) {
        this.organi = organi;
    }

    public int getVedio_id() {
        return vedio_id;
    }

    public void setVedio_id(int vedio_id) {
        this.vedio_id = vedio_id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.id;
        hash = 31 * hash + Objects.hashCode(this.titre);
        hash = 31 * hash + Objects.hashCode(this.description);
        hash = 31 * hash + this.dureedecours;
        hash = 31 * hash + Objects.hashCode(this.difficulte);
        hash = 31 * hash + Objects.hashCode(this.etat);
        hash = 31 * hash + Objects.hashCode(this.categorie);
        hash = 31 * hash + Objects.hashCode(this.formateur);
        hash = 31 * hash + Objects.hashCode(this.appren);
        hash = 31 * hash + Objects.hashCode(this.organi);
        hash = 31 * hash + this.vedio_id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cours other = (Cours) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.titre, other.titre)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (this.dureedecours != other.dureedecours) {
            return false;
        }
        if (!Objects.equals(this.difficulte, other.difficulte)) {
            return false;
        }
        if (!Objects.equals(this.etat, other.etat)) {
            return false;
        }
        if (!Objects.equals(this.categorie, other.categorie)) {
            return false;
        }
        if (!Objects.equals(this.formateur, other.formateur)) {
            return false;
        }
        if (!Objects.equals(this.appren, other.appren)) {
            return false;
        }
        if (!Objects.equals(this.organi, other.organi)) {
            return false;
        }
        if (this.vedio_id != other.vedio_id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cours{" + "id=" + id + ", titre=" + titre + ", description=" + description + ", dureedecours=" + dureedecours + ", difficulte=" + difficulte + ", etat=" + etat + ", categorie=" + categorie + ", formateur=" + formateur + ", appren=" + appren + ", organi=" + organi + ", vedio_id=" + vedio_id + '}';
    }
    




}
