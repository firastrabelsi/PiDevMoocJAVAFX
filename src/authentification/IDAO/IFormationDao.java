/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.IDAO;

import authentification.ENTITIES.Formation;
import java.util.List;

/**
 *
 * @author Dell
 */
public interface IFormationDao {
    void insertObject(Formation val);
    void updateObject(Formation val);
    void deleteObject(Formation val);
    Formation findObjectById(int id);
    List<Formation> DisplayAllObject();
    Formation findByNom(String nomformation);
}
