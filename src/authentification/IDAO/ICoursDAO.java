/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.IDAO;

import authentification.ENTITIES.Cours;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author Anouar
 */
public interface ICoursDAO {
public String SelectVideo(Integer v_id);
 public void ajouterCous(Cours c);
public ObservableList<Cours> findAll();
public ObservableList<Cours> findCoursBytitre(String titre, int id);
 public ObservableList<Cours> findCoursBytitre2(String titre);
public ObservableList<Cours> listcoursFormateurOrOrganisme(int id);
public ObservableList<Cours> listcoursnonvalider();
public ObservableList<Cours> listcoursvalider();
public ObservableList<Cours> listeCoursformateurOrOrganismeNonValider(int id);
public ObservableList<Cours> listeCoursformateurOrOrganismeValider(int id);
 public void modifierCours(Cours c);
 public void modifieretat(int id);
  Cours findObjectById(int id);
  List<Cours> DisplayAllObject();
}
