/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.IDAO;

import authentification.ENTITIES.Chapitre;
import java.util.List;

/**
 *
 * @author Dell
 */
public interface IChapitreDao {
    void insertObject(Chapitre val);
    void updateObject(Chapitre val);
     void deleteObject(Chapitre val);
     Chapitre findObjectById(int id);
     List<Chapitre> DisplayAllObject();
     List<Chapitre> DisplayAllChapCours(int id);
    
}
