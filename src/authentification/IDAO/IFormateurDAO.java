/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.IDAO;

import authentification.ENTITIES.Formateur;
import java.util.List;

/**
 *
 * @author Aymen
 */
public interface IFormateurDAO {
    
    void inscritFormateur(Formateur f);
    
    List<Formateur> getFormateurEnCours();
    List<Formateur> addToComite();
    
    void accepteFormateur(Formateur f);
    void refuseFormateur(Formateur f);
    
    void aceepteToCommitte(Formateur f);
   
    Formateur findFormateurByUserName(String UserName);
    int findFormateurId(String Username);
    
    void editFormateur(Formateur f, int idF);
     
    void removeFormateur(int id);
    public String veriflogin1( String login,String Email);
    public int verifEtat(String username, String email);
    void editComite(Formateur f, int idF);
    
}
