
package authentification.IDAO;

import authentification.ENTITIES.apprenant;
import authentification.ENTITIES.apprenant;
import authentification.ENTITIES.organisme;
import java.util.ArrayList;
import java.util.List;

public interface IapprenantDAO {
    
    int insertApprenant(apprenant appr);
    
    //List<apprenant> getApprenantEnCours();
    
    void updateApprenant(apprenant appr, int id);
    
    int findApprenantId(String Username);
    
    void deleteApprenant(int id);
    
    String veriflogin( String login,String Email);
    
    apprenant findApprenantByEmail(String Email);
    
    apprenant findApprenantByUserName(String UserName);
    
    ArrayList<apprenant> displayAllApprenant();
    
   apprenant findApprenantById(int id);
    
 
}
