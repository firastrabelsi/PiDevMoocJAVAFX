/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.IDAO;

import authentification.ENTITIES.apprenant;
import authentification.ENTITIES.organisme;
import java.util.ArrayList;

/**
 *
 * @author pc
 */
public interface IorganismeDAO {

    void insertOrganisme(organisme org);

    String verifEtat(String username, String email);

       ////////////////////////ORGANISME//////////////////////////////////
    
    ArrayList<organisme> displayAllOrganisme();

    organisme findOrganismeByUserName(String UserName);

       ////////////////////////non valide ORGANISME/////////////////////////
    
    ArrayList<organisme> displayAllOrganismeNonValide();

    void updateOrganisme(organisme appr, int id);

    public void deleteOrganisme(int id);
    
    
    //////////////////////////////////////////////////////////////////////////
    
     organisme findOrganismeByUserNameTotal( String UserName);
     
      int findOrganismetId(String Username);
      
      void updateOrganismefinal(organisme appr, int id);
      
      int insertOrganismeATerminer(organisme org);
}
