/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.IDAO;

import authentification.ENTITIES.Categories;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author Anouar
 */
public interface ICategorieDAO {
void ajouterCategorie(Categories c);
void supprimerCategorieById(int id);
void modifierCategorie(Categories c);
List<Categories> findAll();
Categories findCategoriesById(int id);
public String SelectImage(Integer image_id);

    
}
