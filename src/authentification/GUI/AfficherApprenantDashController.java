/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.apprenant;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class AfficherApprenantDashController implements Initializable {

    @FXML
    private ImageView img;
    @FXML
    private ImageView img1;

    @FXML
    private ImageView img2;

    @FXML
    private ListView<String> listview;
    @FXML
    private Label labelUserName;
    @FXML
    private Label labelNom;
    @FXML
    private Label labelEmail;
    @FXML
    private Label UserName1;
    @FXML
    private Label Nom;
    @FXML
    private Label Email;
    /*  @FXML
     private Label labdatefin;
     @FXML
     private Label labcatégorie;
     @FXML
     private Label labdifficulte;
     private Window primaryStage;*/

    List<apprenant> Apprenants = new ArrayList<>();

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        showListApprenant();
        // TODO
    }

    public void showListApprenant() {
        ObservableList<String> listeApprenant = FXCollections.observableArrayList();

        apprenantDAO cd = new apprenantDAO();
        Apprenants = cd.displayAllApprenant();
        for (apprenant c : Apprenants) {
            listeApprenant.add(c.getUserName());
            listview.setItems(listeApprenant);

        }
    }

    @FXML
    private void RetourAction(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("acceuilDash.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    @FXML
    private void AfficheAction(ActionEvent event) {
        apprenantDAO apprenant = new apprenantDAO();
        String UserName = listview.getSelectionModel().getSelectedItem();
        apprenant appr = apprenant.findApprenantByUserName(UserName);

        // categories= new CategorieDAO().findAll();
        if (appr != null) {
            UserName1.setText(appr.getUserName());
            Nom.setText(appr.getNom());
            Email.setText(appr.getEmail());
            /* labdatefin.setText(cour.getDatefin()+"");
             labdifficulte.setText(cour.getDifficulte());
             //System.out.println(cour.getCategorie());
             //labcatégorie.setText(categories.stream().filter(e->cour.getCategorie().equals(e)).findFirst().get().getCate());
             */
        } else {

            UserName1.setText("");
            Nom.setText("");
            Email.setText("");
            /* labdatefin.setText("");
             labdifficulte.setText("");
             labcatégorie.setText("");*/
        }

    }

}
