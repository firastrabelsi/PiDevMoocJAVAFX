/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;


import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.apprenant;
import static authentification.GUI.LoginController.USERNAME;
import authentification.IDAO.IapprenantDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class AjoutApprenantController implements Initializable {

    @FXML
    private TextField UserName;

    @FXML
    private TextField Nom;
    
    @FXML
    private TextField Prenom;

    @FXML
    private TextField Email;
    

    @FXML
    private TextField Password;

    @FXML
    private Label label1;

    @FXML
    private ImageView image ;
    @FXML
    private ImageView img ;
    @FXML
    private ImageView img1 ;
    @FXML
    private ImageView img2 ;
   
    
    
    @FXML
    private void btnEnregistreraction(ActionEvent event) throws IOException
    {  int resultat;
         apprenant a = new apprenant();
         IapprenantDAO dao = new apprenantDAO();
           
         a.setEmail(Email.getText());
         a.setNom(Nom.getText());
         a.setPrenom(Prenom.getText());
         a.setUserName(UserName.getText());
         a.setPassword(Password.getText());
       
         
         resultat=dao.insertApprenant(a);
         if (resultat==1)
             
         { 
        ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Login.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(p));
        USERNAME=UserName.getText();
        stage.show();
        
         
         }
         else
         { 
         label1.setText("Erreur d'inscription !!!!!Veuillez essayer une autre fois");
         }
         
         
         
    }
    
    @FXML
    private void RetourAction(ActionEvent event) throws IOException
    { 
     ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("choix.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(p));
        stage.show();
        
    }
    
    /**
     * 
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
