/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.OrganismeDAO;
import authentification.ENTITIES.organisme;
import static authentification.GUI.LoginController.USERNAME;
import authentification.IDAO.IorganismeDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class OrganismeProfilController implements Initializable {

    @FXML
    private Label label;

    @FXML
    private ImageView img;
    @FXML
    private ImageView img1;
    @FXML
    private ImageView img2;

    @FXML
    private ImageView img5;

    @FXML
    private void profilAction(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("afficherDonneeProfilOrganisme.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    @FXML
    private void seDeconnecterAction(ActionEvent event) throws IOException {
USERNAME = null;
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("acceuil.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }

    @FXML
    private void cherhcherFormateur(ActionEvent event) throws IOException {
    }

    @FXML
    private void listerFormateur(ActionEvent event) throws IOException {

    }

    @FXML
    private void OrganismAction(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("OrganismeList.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    @FXML
    private void ContactUsAction(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/authentification/GUI/aymen/SendMail.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }

    @FXML
    private void CoursAction(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/authentification/GUI/anwer/Acceuilformorgan.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    @FXML
    private void SedésinscrireAction(ActionEvent event) throws IOException {
        USERNAME = null;
        organisme a = new organisme();
        IorganismeDAO dao = new OrganismeDAO();

        int id = dao.findOrganismetId(USERNAME);
        System.out.println(id);
        dao.deleteOrganisme(id);

        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("acceuil.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        label.setText(USERNAME);
        System.out.println(USERNAME);
    }

}
