/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.apprenant;
import static authentification.GUI.LoginController.USERNAME;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class AfficherDonneeProfilController implements Initializable {

    @FXML
    private TextField username;
    @FXML
    private TextField nom;
    @FXML
    private TextField email;
    @FXML
    private TextField adresse;
     
    @FXML
    private TextArea hh;

    
    
   //   background
    @FXML
    private ImageView img;
    @FXML
    private ImageView img1;
    @FXML
    private ImageView img2;
    @FXML
    private ImageView img3;
    @FXML
    private ImageView img4;
    @FXML
    private ImageView img5;
    @FXML
    private ImageView img6;
    @FXML
    private ImageView img7;
    @FXML
    private ImageView img8;
    @FXML
    private ImageView img9;
    @FXML
    private ImageView img10;
     
   
    private void AfficheAction() {
        apprenantDAO apprenant = new apprenantDAO();
        apprenant appr = apprenant.findApprenantByUserName(USERNAME);
        System.out.println(USERNAME);
        System.out.println(appr);

     
        if (appr!=null) {
            username.setText(appr.getUserName());
            nom.setText(appr.getNom());
            email.setText(appr.getEmail());
            adresse.setText(appr.getAdresse());
           /* labdatefin.setText(cour.getDatefin()+"");
            labdifficulte.setText(cour.getDifficulte());
            //System.out.println(cour.getCategorie());
            //labcatégorie.setText(categories.stream().filter(e->cour.getCategorie().equals(e)).findFirst().get().getCate());
            */
        }
        else{
        
        username.setText("Champs vide");
        nom.setText("Champs vide");
        email.setText("Champs vide");
        adresse.setText("Champs vide");
       /* labdatefin.setText("");
        labdifficulte.setText("");
        labcatégorie.setText("");*/
        }

        
    }
  
    @FXML
    private void ModifierAction(ActionEvent event) throws IOException 
    { int id=0;
        apprenantDAO apprenant = new apprenantDAO();
        apprenant appr = apprenant.findApprenantByUserName(USERNAME);
        
        appr.setUserName(username.getText());
        appr.setEmail(email.getText());
        appr.setNom(nom.getText());
         
        
        
        
         System.out.println(appr);
        
         id=apprenant.findApprenantId( USERNAME) ;
         System.out.println(id);
         apprenant.updateApprenant(appr,id);
        
        
    /*    
     ((Node) event.getSource()).getScene().getWindow().hide();
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("AjoutApprenant.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();*/
    }
    
    @FXML
    private void retourAction(ActionEvent event) throws IOException 
    {
        ((Node) event.getSource()).getScene().getWindow().hide();
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("ApprenantProfil.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
        
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
     AfficheAction();
    }    
    
}
