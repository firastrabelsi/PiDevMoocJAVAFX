/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.OrganismeDAO;
import authentification.ENTITIES.organisme;
import authentification.IDAO.IorganismeDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class AjoutOrganismeController implements Initializable {

    @FXML
    private Label l1;
    @FXML
    private Label l2;
    @FXML
    private Label l3;
    @FXML
    private Label l4;


    @FXML
    private TextField NomDeLaSociete;
    
    @FXML
    private TextField Password;
     
    @FXML
    private TextField UserName;
    
    @FXML
    private TextField Adresse;
    
    @FXML
    private TextField Email;
    
    @FXML
    private TextArea hh;
    
    @FXML
    private ImageView img;
    @FXML
    private ImageView img1;
    @FXML
    private ImageView img2;
    @FXML
    private ImageView img3;
    @FXML
    private ImageView img4;
    @FXML
    private ImageView img5;
    @FXML
    private ImageView img6;
    
    
    
    @FXML
    private void SauvegarderAction(ActionEvent event) throws IOException
    {
         organisme a = new organisme();
         IorganismeDAO dao = new OrganismeDAO();
         a.setUserName(UserName.getText());  
         a.setEmail(Email.getText());
         a.setNomDeLaSociete(NomDeLaSociete.getText());
         a.setAdresse(Adresse.getText());
         a.setPassword(Password.getText());
         dao.insertOrganisme(a);
        ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Login.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }
    
    @FXML
    private void RetourAction(ActionEvent event) throws IOException
    {
    ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("choix.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
