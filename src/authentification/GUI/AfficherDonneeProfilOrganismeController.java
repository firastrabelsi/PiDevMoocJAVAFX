/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.OrganismeDAO;
import authentification.ENTITIES.organisme;
import static authentification.GUI.LoginController.USERNAME;
import authentification.IDAO.IorganismeDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class AfficherDonneeProfilOrganismeController implements Initializable {

    
    @FXML
    private TextField NomDeLaSociete;
    
    @FXML
    private TextField Password;
     
    @FXML
    private TextField UserName;
    
    @FXML
    private TextField Adresse;
    
    @FXML
    private TextField Email;
    
    @FXML
    private TextField site;
     
    @FXML
    private TextField certificat;
      
      
    @FXML
    private TextField num;
    
    
 //   background
    @FXML
    private ImageView img;
    @FXML
    private ImageView img1;
    @FXML
    private ImageView img2;
    @FXML
    private ImageView img3;
    @FXML
    private ImageView img4;
    @FXML
    private ImageView img5;
    @FXML
    private ImageView img6;
    
       
    
   
    private void AfficheAction() {
         organisme a = new organisme();
         IorganismeDAO dao = new OrganismeDAO();
        
        
        organisme appr = dao.findOrganismeByUserName(USERNAME);
        System.out.println(USERNAME);
        System.out.println(appr);

     
        if (appr!=null) {
            UserName.setText(appr.getUserName());
            certificat.setText(appr.getCertificat());
            Password.setText(appr.getPassword());
            Adresse.setText(appr.getAdresse());
            Email.setText(appr.getEmail());
            NomDeLaSociete.setText(appr.getNomDeLaSociete());
            site.setText(appr.getSite());
            num.setText(Integer.toString(appr.getNum()));
          
        }
        else{
        
        UserName.setText("Champs vide");
        NomDeLaSociete.setText("Champs vide");
        Email.setText("Champs vide");
        Adresse.setText("Champs vide");
      
        }

        
    }
    
    
  
    @FXML
    private void ModifierAction(ActionEvent event) throws IOException 
    {   int id=0;
        organisme a = new organisme();
        IorganismeDAO dao = new OrganismeDAO();
        
        organisme appr = dao.findOrganismeByUserName(USERNAME);
        
        appr.setUserName(UserName.getText());
        appr.setEmail(Email.getText());
        appr.setCertificat(certificat.getText());
        appr.setAdresse(Adresse.getText());
        appr.setSite(site.getText());
        appr.setNum(Integer.parseInt(num.getText()));
        appr.setPassword(Password.getText());
        appr.setNomDeLaSociete(NomDeLaSociete.getText());

        
        
        
         System.out.println(appr);
        
         id=dao.findOrganismetId(USERNAME) ;
         System.out.println(id);
         dao.updateOrganismefinal(appr,id);
         USERNAME=UserName.getText();
        
     
     ((Node) event.getSource()).getScene().getWindow().hide();
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("OrganismeProfil.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
    }
    
    @FXML
    private void retourAction(ActionEvent event) throws IOException 
    {
        ((Node) event.getSource()).getScene().getWindow().hide();
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("OrganismeProfil.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
        
    }
    
    @FXML
    private void AcceuilAction(ActionEvent event) throws IOException
    { 
        ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("acceuil.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }
    
    @FXML
    private void FormateurAction(ActionEvent event) throws IOException
    { 
        
    }
    @FXML
    private void OrganismeAction(ActionEvent event) throws IOException
    { 
        
    }
    
    @FXML
    private void ContactUsAction(ActionEvent event) throws IOException
    { 
         ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/authentification/GUI/aymen/SendMail.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }
    @FXML
    private void CoursAction(ActionEvent event) throws IOException
    { 
         ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/authentification/GUI/anwer/Acceuil.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(p));
        stage.show();
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
     AfficheAction();
    }    
    
    
}
