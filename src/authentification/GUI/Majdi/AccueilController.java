/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Majdi;
import authentification.GUI.Majdi.UsersController;
import static authentification.GUI.Majdi.UsersController.cours;
import static authentification.GUI.Majdi.UsersController.user1;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Bali Majdi
 */
public class AccueilController implements Initializable {
    @FXML
    SplitPane sp ;
    @FXML
    Button b1;
    @FXML 
    AnchorPane an1 ;
    @FXML Label lu;
    @FXML Label lc;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lu.setText(user1);
        lc.setText(cours);
        // TODO
    }    
    
         @FXML
     private void handleButtonTOP5Action(ActionEvent event) {
          try {

            FXMLLoader update = new FXMLLoader(getClass().getResource("Comment.fxml"));

            Scene scene = new Scene((Pane) update.load());
            Stage stage = new Stage();
stage.getIcons().setAll(new Image(getClass().getResource("icon.png").toExternalForm()));
            
            stage.setScene(scene);

           // stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(CommentController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
     }
     
      @FXML
     private void handleButtonBadgeajAction(ActionEvent event) {
          try {

            FXMLLoader update = new FXMLLoader(getClass().getResource("uploadBadge.fxml"));

            Scene scene = new Scene((Pane) update.load());
            
            Stage stage = new Stage();
            stage.getIcons().setAll(new Image(getClass().getResource("icon.png").toExternalForm()));

            
            stage.setScene(scene);

           // stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(CommentController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
     }
          @FXML
     private void handleButtonBadgeupAction(ActionEvent event) {
          try {

            FXMLLoader update = new FXMLLoader(getClass().getResource("UpdateBadge.fxml"));
             

            Scene scene = new Scene((Pane) update.load());
            Stage stage = new Stage();
            stage.getIcons().setAll(new Image(getClass().getResource("icon.png").toExternalForm()));

            
            stage.setScene(scene);

           // stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(CommentController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
     }
              @FXML
     private void handleButtongoogleAction(ActionEvent event) {
          try {

            FXMLLoader update = new FXMLLoader(getClass().getResource("GoogleSearch.fxml"));
             

            Scene scene = new Scene((Pane) update.load());
            Stage stage = new Stage();
            stage.getIcons().setAll(new Image(getClass().getResource("icon.png").toExternalForm()));

            
            stage.setScene(scene);

           // stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(CommentController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
     }
    
}
