/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Majdi;

import authentification.DAO.BadgeDAO;
import authentification.ENTITIES.Commentaire;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author Bali Majdi
 */
public class UploadBadgeController implements Initializable {
   @FXML 
   Button up ;
   @FXML 
   Button aj ;
   @FXML
   TextField tf ;
   @FXML
   Button up2 ;
   @FXML
   TextField tf2 ;
   @FXML
   Button up3 ;
   @FXML
   TextField tf3 ;
   @FXML
   GridPane g ; 
   @FXML
   RowConstraints r ;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
     @FXML
    private void upload(ActionEvent event) throws IOException { 
       
        
        FileChooser fileChooser = new FileChooser();
       
        File file = fileChooser.showOpenDialog(null);
        String path = file.getAbsolutePath();
       // System.out.println(path);
        //System.out.println(file.getName());
        
        System.out.println("badge ajoutée");
        Path source = Paths.get(path);
        
        tf.setText(path);
//        //Path target = Paths.get("D:/"+file.getName());
//        
//        try {
//            System.out.println(Files.copy(source, target));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
 
    }
    
     @FXML
    private void upload2(ActionEvent event) throws IOException { 
       
        
        FileChooser fileChooser = new FileChooser();
       
        File file = fileChooser.showOpenDialog(null);
        String path = file.getAbsolutePath();
       // System.out.println(path);
        //System.out.println(file.getName());
        
        System.out.println("badge ajoutée");
        Path source = Paths.get(path);
        
        tf2.setText(path);
//        Path target = Paths.get("D:/"+file.getName());
//        
//        try {
//            System.out.println(Files.copy(source, target));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
// 
    }
    
    
     @FXML
    private void upload3(ActionEvent event) throws IOException { 
       
        
        FileChooser fileChooser = new FileChooser();
       
        File file = fileChooser.showOpenDialog(null);
        String path = file.getAbsolutePath();
       // System.out.println(path);
        //System.out.println(file.getName());
        
        System.out.println("badge ajoutée");
        Path source = Paths.get(path);
        
        tf3.setText(path);
//        Path target = Paths.get("D:/"+file.getName());
//        
//        try {
//            System.out.println(Files.copy(source, target));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
// 
    }
   
     @FXML
    private void ajouter(ActionEvent event) throws IOException { 
     BadgeDAO bgd = new BadgeDAO();
      List<File> LC = new ArrayList<File>();
     String s = tf.getText();
     String s2 = tf2.getText();
     String s3 = tf3.getText();
     Path source = Paths.get(s);
     Path source2 = Paths.get(s2);
     Path source3 = Paths.get(s3);
     File file = source.toFile();
     File file2 = source2.toFile();
     File file3 = source3.toFile();
     
     
     LC.add(0, file);
     LC.add(1, file2);
     LC.add(2, file3);
     bgd.badge2(LC);
         System.out.println("Succées d'ajout !!!!!");
    }
}
