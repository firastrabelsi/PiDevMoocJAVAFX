/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Majdi;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javafx.stage.Popup;
import javax.swing.ImageIcon;

/**
 *
 * @author Bali Majdi
 */
public class DisplayTrayIcon {
    
static TrayIcon trayIcon;
    public DisplayTrayIcon() {
    ShowTrayIcon();
    }

    public static void ShowTrayIcon() {
      if(!SystemTray.isSupported()){
          System.out.println("error");
          System.exit(0);
          return;
      }
      final PopupMenu popup = new PopupMenu();
        trayIcon= new TrayIcon(CreateIcon("icon.png", "Tray Icon"));
    Menu DisplayMenu = new Menu ("Menu");
        final SystemTray tary = SystemTray.getSystemTray();
    MenuItem ExitItem = new MenuItem("Exit");
    MenuItem InfoItem = new MenuItem("Info Trigger");
    
    popup.add(ExitItem);
       trayIcon.setPopupMenu(popup);
    ExitItem.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            
              System.exit(0);
          }
      });
    
    
    try{
        tary.add(trayIcon);
    }
    catch(AWTException e){
        
    }
    
    
    }
    protected  static Image CreateIcon(String path, String desc)
    {
        URL ImageURL = DisplayTrayIcon.class.getResource(path);
   return (new ImageIcon(ImageURL,desc)).getImage();
    }
}
