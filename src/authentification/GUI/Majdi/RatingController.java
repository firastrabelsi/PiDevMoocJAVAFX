/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Majdi;

import authentification.DAO.CommentaireDAO;
import authentification.DAO.CoursDAO;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.Commentaire;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.apprenant;
import static authentification.GUI.Majdi.UsersController.cours;
import static authentification.GUI.Majdi.UsersController.user1;
import javafx.scene.image.Image;
import java.awt.TrayIcon;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextFlow;
import javafx.scene.control.PopupControl;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.Region;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Bali Majdi
 */
public class RatingController implements Initializable {

    @FXML
    ImageView s1;
    @FXML
    ImageView s2;
    @FXML
    ImageView s3;
    @FXML
    ImageView s4;
    @FXML
    ImageView s5;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    public void handleMouseEntered(MouseEvent arg0) {
        Image img = new Image("/Views/starOn.png");
        Image img2 = new Image("/Views/starOff.png");
        s1.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                s1.setImage(img);
                s2.setImage(img2);
                s3.setImage(img2);
                s4.setImage(img2);
                s5.setImage(img2);
            }
        });
        s2.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                s1.setImage(img);
                s2.setImage(img);
                s3.setImage(img2);
                s4.setImage(img2);
                s5.setImage(img2);
            }
        });

        s3.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                s1.setImage(img);
                s2.setImage(img);
                s3.setImage(img);
                s4.setImage(img2);
                s5.setImage(img2);
            }
        });

        s4.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                s1.setImage(img);
                s2.setImage(img);
                s3.setImage(img);
                s4.setImage(img);
                s5.setImage(img2);
            }
        });

        s5.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                s1.setImage(img);
                s2.setImage(img);
                s3.setImage(img);
                s4.setImage(img);
                s5.setImage(img);
            }
        });

    }

    @FXML
    public void handleMouseClicked(MouseEvent arg0) {

        s1.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                               // int idu = Integer.parseInt(user1);
                //  int idc = Integer.parseInt(cours);
                Cours cr = new Cours();
                CoursDAO crDAO = new CoursDAO();
                cr = crDAO.rechercherCoursbyid2(1);
                System.out.println("r1 selected");

                float n = cr.getNote();
                int nbr = cr.getNbVote();
                float a = (((n * nbr) + 2) / (nbr + 1));
                int b = Math.round(a + (1 / 5));
                cr.setNote(b);
                nbr++;
                cr.setNbVote(nbr);
                crDAO.updateCours(cr);
            }
        });
        s2.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                               // int idu = Integer.parseInt(user1);
                //  int idc = Integer.parseInt(cours);
                Cours cr = new Cours();
                CoursDAO crDAO = new CoursDAO();
                cr = crDAO.rechercherCoursbyid2(1);
                System.out.println("r2 selected");

                float n = cr.getNote();
                int nbr = cr.getNbVote();
                float a = (((n * nbr) + 4) / (nbr + 1));
                int b = Math.round(a + (1 / 5));
                cr.setNote(b);
                nbr++;
                cr.setNbVote(nbr);
                crDAO.updateCours(cr);
            }
        });
        s3.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                               // int idu = Integer.parseInt(user1);
                //  int idc = Integer.parseInt(cours);
                Cours cr = new Cours();
                CoursDAO crDAO = new CoursDAO();
                cr = crDAO.rechercherCoursbyid2(1);
                System.out.println("r3 selected");

                float n = cr.getNote();
                int nbr = cr.getNbVote();
                float a = (((n * nbr) + 6) / (nbr + 1));
                int b = Math.round(a + (1 / 5));
                cr.setNote(b);
                nbr++;
                cr.setNbVote(nbr);
                crDAO.updateCours(cr);
            }
        });
        s4.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                               // int idu = Integer.parseInt(user1);
                //  int idc = Integer.parseInt(cours);
                Cours cr = new Cours();
                CoursDAO crDAO = new CoursDAO();
                cr = crDAO.rechercherCoursbyid2(1);
                System.out.println("r4 selected");

                float n = cr.getNote();
                int nbr = cr.getNbVote();
                float a = (((n * nbr) + 8) / (nbr + 1));
                int b = Math.round(a + (1 / 5));
                cr.setNote(b);
                nbr++;
                cr.setNbVote(nbr);
                crDAO.updateCours(cr);
            }
        });
        s5.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                               // int idu = Integer.parseInt(user1);
                //  int idc = Integer.parseInt(cours);
                Cours cr = new Cours();
                CoursDAO crDAO = new CoursDAO();
                cr = crDAO.rechercherCoursbyid2(1);
                System.out.println("r5 selected");

                float n = cr.getNote();
                int nbr = cr.getNbVote();
                float a = (((n * nbr) + 10) / (nbr + 1));
                int b = Math.round(a + (1 / 5));
                cr.setNote(b);
                nbr++;
                cr.setNbVote(nbr);
                crDAO.updateCours(cr);
            }
        });
    }

}
