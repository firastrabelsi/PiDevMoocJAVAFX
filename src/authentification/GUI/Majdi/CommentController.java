/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Majdi;

import authentification.DAO.CommentaireDAO;
import authentification.DAO.CoursDAO;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.Commentaire;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.apprenant;
import static authentification.GUI.Majdi.UsersController.cours;
import static authentification.GUI.Majdi.UsersController.user1;
import javafx.scene.image.Image;
import java.awt.TrayIcon;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextFlow;
import javafx.scene.control.PopupControl;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.Region;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Bali Majdi
 */
public class CommentController implements Initializable {

    DisplayTrayIcon DTI = new DisplayTrayIcon();
    @FXML
    ImageView s1;
    @FXML
    ImageView s2;
    @FXML
    ImageView s3;
    @FXML
    ImageView s4;
    @FXML
    ImageView s5;
    @FXML
    Button btnTelecharger;
    @FXML
    Button btnComment;
    @FXML
    TextArea txtC;

    @FXML
    ImageView pngmooc;
    @FXML
    ImageView pngpdf;
    @FXML
    Label txt;
    @FXML
    ListView idl;
    @FXML
    Button idBtnModif;
    @FXML
    Label ll;
    @FXML
    RadioButton r1;
    @FXML
    RadioButton r2;
    @FXML
    RadioButton r3;
    @FXML
    RadioButton r4;
    @FXML
    RadioButton r5;

    @FXML
    Button btnVoter;
    @FXML
    Pane p1;
    @FXML
    Label lb;
    @FXML
    HTMLEditor html;
    @FXML
    WebView wbv;
    @FXML
    Button top5;
//            private TEST application;

    @FXML
    private void handleButtonAction(ActionEvent event) {
        int idu = Integer.parseInt(user1);
        int idc = Integer.parseInt(cours);
        idc = 1;
        System.out.println("i d user est  **** **** *** *** *** *** *** *** **" + idu);
        System.out.println("i d user est  **** **** *** *** *** *** *** *** **" + idc);
        DisplayTrayIcon.trayIcon.displayMessage("Info", "Un commentaire est ajouté", TrayIcon.MessageType.INFO);

        CommentaireDAO cDao = new CommentaireDAO();

        CoursDAO cr = new CoursDAO();
        Cours cr2 = cr.findObjectById(idc);
        apprenantDAO ut = new apprenantDAO();
        apprenant ut2 = ut.findApprenantById(idu);

        Commentaire comm = new Commentaire(txtC.getText(), cr2, ut2);
        cDao.Commenter(comm);
        System.out.println("******Commenter*****");
//        this.application = application;
        txtC.clear();

        ObservableList<Pane> panes = FXCollections.observableArrayList();
        ObservableList<String> panes2 = FXCollections.observableArrayList();

        List<Commentaire> LC = new ArrayList<Commentaire>();
        LC = cDao.getList(1);
        int i = 0;
        for (Commentaire c : LC) {

//            txt.setText(c.toString()+" 2016 ");
            // System.out.println(c.toString()+"controllercontrollercontrollercontroller");
            panes.add(getComment(c, i));
            idl.setItems(panes);
            idl.setSelectionModel(null);
            i++;
            java.util.Date date_util2 = new java.util.Date();

            java.sql.Date date_sql = new java.sql.Date(date_util2.getTime());
            java.sql.Date date_sql2 = new java.sql.Date(c.getDate().getTime());

            long i2 = date_sql.getDate() - date_sql2.getDate();
            System.out.println("la diff est " + i2);

        }

        Date d2 = new Date();
//            d2.setYear(cmt.getDate().getYear());
//            d2.setMonth(cmt.getDate().getMonth());
//            d2.setTime(cmt.getDate().getTime());
//            d2.setDate(cmt.getDate().getDay());
//            d2.setHours(cmt.getDate().getHours());

    }

    @FXML
    public void handleMouseClick(MouseEvent arg0) {
        //System.out.println("clicked on " + idl.getSelectionModel().getSelectedIndex());
    }

    @FXML
    public int handleMouse2Click(MouseEvent arg0) {
        return idl.getSelectionModel().getSelectedIndex();
    }

    public int hhhhhh() {
        return idl.getSelectionModel().getSelectedIndex();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        int idu = Integer.parseInt(user1);
        int idc = Integer.parseInt(cours);
        ObservableList<Pane> panes = FXCollections.observableArrayList();
        ObservableList<String> panes2 = FXCollections.observableArrayList();

        CommentaireDAO cDao = new CommentaireDAO();

        List<Commentaire> LC = new ArrayList<Commentaire>();
        LC = cDao.getList(idc);
        int i = 0;
        for (Commentaire c : LC) {

//            txt.setText(c.toString()+" 2016 ");
            // System.out.println(c.toString()+"controllercontrollercontrollercontroller");
            panes.add(getComment(c, i));
            idl.setItems(panes);
            idl.setSelectionModel(null);
            i++;
        }
    }

    private Pane getComment(Commentaire c, int i) {
        int idu = Integer.parseInt(user1);
        int idc = Integer.parseInt(cours);
        CommentaireDAO cDAO = new CommentaireDAO();

        Pane pane = new Pane();
        Image img = new Image("/authentification/GUI/Majdi/face.png");
        Image edit = new Image("/authentification/GUI/Majdi/edit2.png");
        Image delete = new Image("/authentification/GUI/Majdi/del.png");
        ImageView imageView = new ImageView(img);

        pane.setMaxSize(Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);
        pane.setPrefWidth(300);
        pane.setPrefHeight(114);
        String style = "-fx-padding: 8 15 15 15;\n"
                + "    -fx-background-insets: 0,0 0 5 0, 0 0 6 0, 0 0 7 0;\n"
                + "    -fx-background-radius: 8;\n"
                + "    -fx-font-weight: bold;\n"
                + "    -fx-font-size: 14;";

        String style2 = "-fx-text-fill: #40bf80;";
        pane.setStyle(style);
        pane.setId("pane_onePays");

        // ImageView Proprieties
        imageView.setFitHeight(65);
        imageView.setFitWidth(65);

        imageView.setLayoutX(14);
        imageView.setLayoutY(15);

        Label UserName = new Label("" + c.getUt().getNom());
        Label Contenue = new Label("" + c.getContenu());
        Label dateCr = new Label("" + c.getDate());

        Button supp = new Button("", new ImageView(delete));
        Button mod = new Button("", new ImageView(edit));
        UserName.setLayoutX(93);
        UserName.setLayoutY(5);
        UserName.setId("UserName");
        UserName.setStyle(style2);

        Contenue.setLayoutX(93);
        Contenue.setLayoutY(25);
        Contenue.setId("Contenue");

        dateCr.setLayoutX(93);
        dateCr.setLayoutY(90);
        dateCr.setId("dateCr");

        //Button Position
        supp.setLayoutX(573);
        supp.setLayoutY(60);
        supp.setId(c.getId() + "");
        supp.getStyleClass().add("btn-primary");

        mod.setLayoutX(570);
        mod.setLayoutY(15);
        mod.setId(i + "");
        mod.getStyleClass().add("btn-primary");

        mod.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                try {

                    FXMLLoader update = new FXMLLoader(getClass().getResource("Update.fxml"));

                    Scene scene = new Scene((Pane) update.load());
                    Stage stage = new Stage();

                    UpdateController up = update.<UpdateController>getController();
                    // updating envoi de param.
                    String s = "";
                    CommentaireDAO cDao = new CommentaireDAO();
                    List<Commentaire> LC = new ArrayList<Commentaire>();
                    LC = cDao.getList(1);
                    s = LC.get(Integer.parseInt(mod.getId())).getContenu();

                    up.getIndice(s, Integer.parseInt(mod.getId()));
                    stage.setScene(scene);

                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.showAndWait();

                } catch (IOException ex) {
                    Logger.getLogger(CommentController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        });

        supp.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                DisplayTrayIcon.trayIcon.displayMessage("Info", "Un commentaire est supprimé", TrayIcon.MessageType.INFO);
                CommentaireDAO comDAO = new CommentaireDAO();
                comDAO.SupprimerCommentaire(Integer.parseInt(supp.getId()));

                ObservableList<Pane> panes = FXCollections.observableArrayList();

                CommentaireDAO cDao = new CommentaireDAO();

                List<Commentaire> LC = new ArrayList<Commentaire>();
                LC = cDao.getList(1);
                int i = 0;
                for (Commentaire c : LC) {

//            txt.setText(c.toString()+" 2016 ");
                    // System.out.println(c.toString()+"controllercontrollercontrollercontroller");
                    panes.add(getComment(c, i));
                    idl.setItems(panes);
                    idl.setSelectionModel(null);
                    i++;
                }
            }
        });

        pane.getChildren().add(imageView);

        pane.getChildren().add(UserName);
        pane.getChildren().add(Contenue);
        pane.getChildren().add(dateCr);
        if (c.getUt().getId() == idu) {
            pane.getChildren().add(supp);
            pane.getChildren().add(mod);
        }

        return pane;

    }

//    @FXML
//    private void OnDragAction(ActionEvent event) {
//        if (r1.isArmed()) {
//            r1.setSelected(true);
//            r2.setSelected(false);
//            r3.setSelected(false);
//            r4.setSelected(false);
//            r5.setSelected(false);
//        }
//
//        if (r2.isArmed()) {
//            r1.setSelected(true);
//            r2.setSelected(true);
//            r3.setSelected(false);
//            r4.setSelected(false);
//            r5.setSelected(false);
//        }
//
//        if (r3.isArmed()) {
//            r1.setSelected(true);
//            r2.setSelected(true);
//            r3.setSelected(true);
//            r4.setSelected(false);
//            r5.setSelected(false);
//        }
//        if (r4.isArmed()) {
//            r1.setSelected(true);
//            r2.setSelected(true);
//            r3.setSelected(true);
//            r4.setSelected(true);
//            r5.setSelected(false);
//
//        }
//        if (r5.isArmed()) {
//            r1.setSelected(true);
//            r2.setSelected(true);
//            r3.setSelected(true);
//            r4.setSelected(true);
//            r5.setSelected(true);
//
//        }
//
//    }
//
//    @FXML
//    private void handleButtonVoteAction(ActionEvent event) {
//        int idu = Integer.parseInt(user1);
//        int idc = Integer.parseInt(cours);
//        Cours cr = new Cours();
//        CoursDAO crDAO = new CoursDAO();
//        cr = crDAO.findCoursById(idc);
//        if ((r1.isSelected()) & (!r2.isSelected()) & (!r3.isSelected()) & (!r4.isSelected()) & (!r5.isSelected())) {
//            System.out.println("r1 selected");
//
//            cr = crDAO.findCoursById(1);
//            float n = cr.getNote();
//
//            int nbr = cr.getNbVote();
//
//            float a = (((n * nbr) +2) / (nbr + 1));
//
//            int b = Math.round(a+ (1/5));
//            cr.setNote(b);
//            cr.setNbVote(nbr++);
//            crDAO.updateCours(cr);
//        } 
//        if ((r2.isSelected()) & (!r3.isSelected()) & (!r4.isSelected()) & (!r5.isSelected())) {
//            System.out.println("r2 selected");
//
//            cr = crDAO.findCoursById(1);
//            float n = cr.getNote();
//            int nbr = cr.getNbVote();
//            float a = (((n * nbr) +4) / (nbr + 1));
//            int b = Math.round(a+ (1/5));
//            cr.setNote(b);
//            nbr++;
//            cr.setNbVote(nbr);
//            crDAO.updateCours(cr);
//
//        }
//        if ((r3.isSelected()) & (!r4.isSelected()) & (!r5.isSelected())) {
//            System.out.println("r3 selected");
//
//            cr = crDAO.findCoursById(1);
//            float n = cr.getNote();
//            int nbr = cr.getNbVote();
//            float a = (((n * nbr) +6 )/ (nbr + 1));
//            int b = Math.round(a+ (1/5));
//            cr.setNote(b);
//            nbr++;
//            cr.setNbVote(nbr);
//            crDAO.updateCours(cr);
//
//        } 
//        if ((r4.isSelected()) & (!r5.isSelected())) {
//            System.out.println("r4 selected");
//
//            cr = crDAO.findCoursById(1);
//            float n = cr.getNote();
//            int nbr = cr.getNbVote();
//            float a = (((n * nbr)+8) / (nbr + 1));
//            int b = Math.round(a+ (1/5));
//            cr.setNote(b);
//            nbr++;
//            cr.setNbVote(nbr);
//            crDAO.updateCours(cr);
//
//        } 
//        if ((r5.isSelected())) {
//            System.out.println("r5 selected");
//
//            cr = crDAO.findCoursById(1);
//            float n = cr.getNote();
//            int nbr = cr.getNbVote();
//            float a = (((n * nbr) +10 )/ (nbr + 1));
//            int b = Math.round(a+ (1/5));
//            cr.setNote(b);
//            nbr++;
//            cr.setNbVote(nbr);
//            crDAO.updateCours(cr);
//
//        }
//
//        lb.setText("Note du cours " + cr.getNote() + "/10");
//        p1.setVisible(false);
//        btnVoter.setVisible(false);
//    }
    @FXML
    private void handleButtonTOP5Action(ActionEvent event) {
        try {

            FXMLLoader update = new FXMLLoader(getClass().getResource("TOP5.fxml"));

            Scene scene = new Scene((Pane) update.load());
            Stage stage = new Stage();

            stage.setScene(scene);

            // stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(CommentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void handleButtonrefreshAction(ActionEvent event) {

        ObservableList<Pane> panes = FXCollections.observableArrayList();
        ObservableList<String> panes2 = FXCollections.observableArrayList();

        CommentaireDAO cDao = new CommentaireDAO();

        List<Commentaire> LC = new ArrayList<Commentaire>();
        LC = cDao.getList(1);
        int i = 0;
        for (Commentaire c : LC) {

//            txt.setText(c.toString()+" 2016 ");
            // System.out.println(c.toString()+"controllercontrollercontrollercontroller");
            panes.add(getComment(c, i));
            idl.setItems(panes);
            idl.setSelectionModel(null);
            i++;
        }

    }

    @FXML
    public void handleMouseEntered(MouseEvent arg0) {
        Image img = new Image("/authentification/GUI/Majdi/starOn.png");
        Image img2 = new Image("/authentification/GUI/Majdi/starOff.png");
        s1.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                s1.setImage(img);
                s2.setImage(img2);
                s3.setImage(img2);
                s4.setImage(img2);
                s5.setImage(img2);
            }
        });
        s2.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                s1.setImage(img);
                s2.setImage(img);
                s3.setImage(img2);
                s4.setImage(img2);
                s5.setImage(img2);
            }
        });

        s3.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                s1.setImage(img);
                s2.setImage(img);
                s3.setImage(img);
                s4.setImage(img2);
                s5.setImage(img2);
            }
        });

        s4.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                s1.setImage(img);
                s2.setImage(img);
                s3.setImage(img);
                s4.setImage(img);
                s5.setImage(img2);
            }
        });

        s5.setOnMouseEntered(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                s1.setImage(img);
                s2.setImage(img);
                s3.setImage(img);
                s4.setImage(img);
                s5.setImage(img);
            }
        });

    }

    @FXML
    public void handleMouseClicked(MouseEvent arg0) {

        s1.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                // int idu = Integer.parseInt(user1);
                int idc = Integer.parseInt(cours);
                Cours cr = new Cours();
                CoursDAO crDAO = new CoursDAO();
                cr = crDAO.rechercherCoursbyid2(idc);
                System.out.println("r1 selected");

                float n = cr.getNote();
                int nbr = cr.getNbVote();
                float a = (((n * nbr) + 2) / (nbr + 1));
                int b = Math.round(a + (1 / 5));
                cr.setNote(b);
                nbr++;
                cr.setNbVote(nbr);
                crDAO.updateCours(cr);
                p1.setVisible(false);
            }
        });
        s2.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                // int idu = Integer.parseInt(user1);
                int idc = Integer.parseInt(cours);
                Cours cr = new Cours();
                CoursDAO crDAO = new CoursDAO();
                cr = crDAO.rechercherCoursbyid2(idc);
                System.out.println("r2 selected");

                float n = cr.getNote();
                int nbr = cr.getNbVote();
                float a = (((n * nbr) + 4) / (nbr + 1));
                int b = Math.round(a + (1 / 5));
                cr.setNote(b);
                nbr++;
                cr.setNbVote(nbr);
                crDAO.updateCours(cr);
                p1.setVisible(false);
            }
        });
        s3.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                // int idu = Integer.parseInt(user1);
                int idc = Integer.parseInt(cours);
                Cours cr = new Cours();
                CoursDAO crDAO = new CoursDAO();
                cr = crDAO.rechercherCoursbyid2(idc);
                System.out.println("r3 selected");

                float n = cr.getNote();
                int nbr = cr.getNbVote();
                float a = (((n * nbr) + 6) / (nbr + 1));
                int b = Math.round(a + (1 / 5));
                cr.setNote(b);
                nbr++;
                cr.setNbVote(nbr);
                crDAO.updateCours(cr);
                p1.setVisible(false);
            }
        });
        s4.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                // int idu = Integer.parseInt(user1);
                int idc = Integer.parseInt(cours);
                Cours cr = new Cours();
                CoursDAO crDAO = new CoursDAO();
                cr = crDAO.rechercherCoursbyid2(idc);
                System.out.println("r4 selected");

                float n = cr.getNote();
                int nbr = cr.getNbVote();
                float a = (((n * nbr) + 8) / (nbr + 1));
                int b = Math.round(a + (1 / 5));
                cr.setNote(b);
                nbr++;
                cr.setNbVote(nbr);
                crDAO.updateCours(cr);
                p1.setVisible(false);
            }
        });
        s5.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                // int idu = Integer.parseInt(user1);
                int idc = Integer.parseInt(cours);
                Cours cr = new Cours();
                CoursDAO crDAO = new CoursDAO();
                cr = crDAO.rechercherCoursbyid2(idc);
                System.out.println("r5 selected");

                float n = cr.getNote();
                int nbr = cr.getNbVote();
                float a = (((n * nbr) + 10) / (nbr + 1));
                int b = Math.round(a + (1 / 5));
                cr.setNote(b);
                nbr++;
                cr.setNbVote(nbr);
                crDAO.updateCours(cr);
lb.setText("Note du cours " + cr.getNote() + "/10");
                p1.setVisible(false);
            }
        });
    }

}
