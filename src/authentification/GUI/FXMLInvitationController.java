/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.InvitationDAO;
import authentification.ENTITIES.invitation;
import authentification.ENTITIES.organisme;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

/**
 * FXML Controller class
 *
 * @author Firas
 */
public class FXMLInvitationController implements Initializable {

    @FXML
    private ListView<String> listview;
    @FXML
    private Label labelUserName;
    @FXML
    private Label labeladresse;
    @FXML
    private Label labelEmail;
    @FXML
    private Label UserName1;
    @FXML
    private Label adresse;
    @FXML
    private Label Email;
    @FXML
    private Label  nomsociete;

    
    List <invitation> invitations = new ArrayList<>();
    public void showListinvitation(){
       ObservableList<String> listinvitation = FXCollections.observableArrayList();
       
         InvitationDAO invitationn= new InvitationDAO();
        invitations =invitationn.AfficherInvitation(3);
        for (invitation c : invitations) {
                        listinvitation.add(Integer.toString(c.getId()));

        }
                                listview.setItems(listinvitation);

    }
    
    @FXML
    private void AfficheAction(ActionEvent event) {
         InvitationDAO invitationdo= new InvitationDAO();
        String UserName =listview.getSelectionModel().getSelectedItem();
        invitation appr=invitationdo.findinvitationbyid(Integer.parseInt(UserName));
       // categories= new CategorieDAO().findAll();
        if (appr!=null) {
            UserName1.setText(Integer.toString(appr.getSource()));
            
        }
        else{
        
        UserName1.setText("");
        }

    }
    @FXML
    private void confirmer(ActionEvent event) {
         InvitationDAO invitationdo= new InvitationDAO();
         invitationdo.confirmer(Integer.parseInt(listview.getSelectionModel().getSelectedItem()));
    }
    
    @FXML
    private void rejeter(ActionEvent event) {
InvitationDAO invitationdo= new InvitationDAO();
         invitationdo.Rejeter(Integer.parseInt(listview.getSelectionModel().getSelectedItem()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
showListinvitation();
    }
    }
    
    
    
   
    

