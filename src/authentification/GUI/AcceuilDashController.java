/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class AcceuilDashController implements Initializable {

    @FXML
    private ComboBox<String> combobox;
    @FXML
    private ImageView image;
    @FXML
    private ImageView img1;
    @FXML
    private ImageView img2;
    @FXML
    private ImageView img3;
    @FXML
    private ImageView img4;
    @FXML
    private ImageView img11;

    @FXML
    private void sedeconnecterAction(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("acceuil.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    @FXML
    private void afficherApprenantAction(ActionEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("AfficherApprenantDash.fxml"));
        try {
            loader.load();
        } catch (IOException ex) {
            Logger.getLogger(AcceuilDashController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    ObservableList<String> list = FXCollections.observableArrayList(
            "Liste des Organismes valides", "Liste des Organismes non valides"
    );

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        combobox.setItems(list);
    }

    public void comboChanged(ActionEvent event) {
        if (combobox.getSelectionModel().getSelectedItem().equals("Liste des Organismes valides")) {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("ListedesOrganismesValidesDASH.fxml"));
            try {
                loader.load();
            } catch (IOException ex) {
                Logger.getLogger(AcceuilDashController.class.getName()).log(Level.SEVERE, null, ex);
            }
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();

        } else if (combobox.getSelectionModel().getSelectedItem().equals("Liste des Organismes non valides")) {

            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("ListesdesOrganismeNONVALIDESDash.fxml"));
            try {
                loader.load();
            } catch (IOException ex) {
                Logger.getLogger(AcceuilDashController.class.getName()).log(Level.SEVERE, null, ex);
            }
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();

        }

    }
     @FXML
    private void NotifAction(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/authentification/GUI/Mouna/Reclamation.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

}
