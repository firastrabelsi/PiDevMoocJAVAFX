/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.apprenant;
import static authentification.GUI.LoginController.USERNAME;
import authentification.IDAO.IapprenantDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class ModifierController implements Initializable {

   @FXML
    private TextField UserName;

    @FXML
    private TextField Nom;
    
    @FXML
    private TextField Prenom;

    @FXML
    private TextField Email;
    

    @FXML
    private TextField Password;  
    
    private void AfficheAction() {
         apprenantDAO apprenant= new apprenantDAO();
        apprenant appr=apprenant.findApprenantByUserName(USERNAME);
       // categories= new CategorieDAO().findAll();
        if (appr!=null) {
            UserName.setText(appr.getUserName());
            Nom.setText(appr.getNom());
            Email.setText(appr.getEmail());
           /* labdatefin.setText(cour.getDatefin()+"");
            labdifficulte.setText(cour.getDifficulte());
            //System.out.println(cour.getCategorie());
            //labcatégorie.setText(categories.stream().filter(e->cour.getCategorie().equals(e)).findFirst().get().getCate());
            */
        }
        else{
        
        UserName.setText("");
        Nom.setText("");
        Email.setText("");
       /* labdatefin.setText("");
        labdifficulte.setText("");
        labcatégorie.setText("");*/
        }

        
    }
    
     @FXML
    private void btnEnregistreraction(ActionEvent event)
    {
         apprenant a = new apprenant();
         IapprenantDAO dao = new apprenantDAO();
           
         a.setEmail(Email.getText());
         a.setNom(Nom.getText());
         a.setPrenom(Prenom.getText());
         a.setUserName(UserName.getText());
         a.setPassword(Password.getText());
       
         dao.insertApprenant(a);
         ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("afficherDonneeProfil.fxml"));
       try {
           loader.load();
       } catch (IOException ex) {
           Logger.getLogger(ModifierController.class.getName()).log(Level.SEVERE, null, ex);
       }
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AfficheAction();
    }    
    
}
