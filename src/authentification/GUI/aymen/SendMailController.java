
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.aymen;

import java.io.File;
import java.io.IOException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * FXML Controller class
 *
 * @author Saidi
 */
public class SendMailController implements Initializable {
    @FXML
    private TextField us;
    @FXML
    private TextField mo;
    @FXML
    private TextField fr;
    @FXML
    private TextField t;
    @FXML
    private TextField su;
    @FXML
    private TextField te;
//    @FXML
//    private TextField fi;    
    @FXML
    private ImageView img3;
    @FXML
    private ImageView img5;
 

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

         
        
      
 
        t.setText("aymen.hosni@esprit.tn");

    }    

    @FXML
    private void send(ActionEvent event) throws MessagingException, IOException {
       final String username = us.getText();
		final String password = mo.getText();

		
/* L'adresse de l'expéditeur */
String from = fr.getText();

/* L'adresse du destinataire */
 String to =t.getText();

/* L'objet du message */
String objet = su.getText();

/* Le corps du mail */
String texte = te.getText();
String pieceJointe = null;
Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				return new javax.mail.PasswordAuthentication(username, password);
			}
		  });
            

///* Création du message*/
Message msg = new MimeMessage(session);

try {
      msg.setFrom(new InternetAddress(from));
      msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to, false));
      msg.setSubject(objet);
      msg.setText(texte);
      msg.setHeader("X-Mailer", "LOTONtechEmail");
      
       // Create the message part
         BodyPart messageBodyPart = new MimeBodyPart();

         // Now set the actual message
         messageBodyPart.setText("");
         // Create a multipar message
         Multipart multipart = new MimeMultipart();

         // Set text message part
         multipart.addBodyPart(messageBodyPart);

         
      Transport.send(msg);
}
catch (AddressException e) {
      e.printStackTrace();
} 
catch (MessagingException e) {
      e.printStackTrace();
}
 }

    @FXML
    private void retour(ActionEvent event) {
        try {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("/authentification/GUI/acceuil.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            app_stage.setScene(home_page_scene);
            app_stage.show();
        } catch (IOException ex) {
            Logger.getLogger(SendMailController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}