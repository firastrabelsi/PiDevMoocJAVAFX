/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.aymen;


import authentification.DAO.FormateurDAO;
import authentification.ENTITIES.Formateur;
import authentification.IDAO.IFormateurDAO;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Aymen
 */
public class AffichageController implements Initializable {
    Formateur f1 = new Formateur();
    @FXML
    private ListView<Formateur> affiche;
    ObservableList<Formateur> liste = FXCollections.observableArrayList();
    @FXML
    private Label nom;
    @FXML
    private Label prenom;
    @FXML
    private Label usename;
    @FXML
    private Label specialite;
    
    private Stage primaryStage ;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        afficherlist();
    }    
     public void afficherlist(){
     IFormateurDAO dao = new FormateurDAO();
     List<Formateur> listformateurs = new ArrayList<Formateur>();
     listformateurs = dao.getFormateurEnCours();
     for(Formateur f : listformateurs){
     liste.add(f);}
     affiche.setItems(liste);
     
     affiche.getSelectionModel().selectedItemProperty().addListener((Observable, oldValue, newValue)-> Affichedetaill(newValue));
     
     }

    private void Affichedetaill(Formateur formateurs) {
        if (formateurs !=null){
        nom.setText(formateurs.getNom()+"");
        prenom.setText(formateurs.getPrenom()+"");
        usename.setText(formateurs.getUsername()+"");
        specialite.setText(formateurs.getSpecialite());
        
        }
        else {
            nom.setText("");
            prenom.setText("");
            usename.setText("");
            specialite.setText("");
        }
        
    }

    @FXML
    private void supp(ActionEvent event) {
        Formateur formateurs = new Formateur();
        formateurs = affiche.getSelectionModel().getSelectedItem();
        if(formateurs != null){
        affiche.getItems().remove(formateurs);
        new FormateurDAO().removeFormateur(formateurs.getId());
        }
        else {
        Alert alert = new Alert(AlertType.WARNING);
        alert.initOwner(primaryStage);
        alert.setTitle("selectionner un formateur");
        alert.setHeaderText("aucun selection");
        alert.setContentText("cliquer svp sur un formateur");
        alert.showAndWait();
        
        
        }
    }

    @FXML
    private void accepter(ActionEvent event) {
        Formateur formateurs = new Formateur();
        formateurs = affiche.getSelectionModel().getSelectedItem();
        if(formateurs != null){
        affiche.getItems().remove(formateurs);
        new FormateurDAO().accepteFormateur(formateurs);
        }
        
    }

    @FXML
    private void openfile(ActionEvent event) throws IOException {
        Desktop d = Desktop.getDesktop();
        f1=affiche.getSelectionModel().getSelectedItem();
        System.out.println("aaaaaa"+f1.getPath());
           System.out.println(f1.getPath()+"+++++++++++++++++++++++++++++++++++++++++++++++++++"+f1.toString());

//d.open(new File("D:/fichier/"+f1.getPath())); 
  d.open(new File("D:/aymen/"+f1.getPath()));
    }

    @FXML
    private void retour(ActionEvent event) {
        try {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("/authentification/GUI/aymen/comite.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            app_stage.setScene(home_page_scene);
            app_stage.show();
        } catch (IOException ex) {
            Logger.getLogger(AffichageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
