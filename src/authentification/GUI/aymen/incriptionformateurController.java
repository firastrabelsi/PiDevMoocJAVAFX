/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.aymen;


import authentification.DAO.FormateurDAO;
import authentification.ENTITIES.Formateur;
import authentification.IDAO.IFormateurDAO;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.omg.CORBA.portable.InputStream;

/**
 * FXML Controller class
 *
 * @author Aymen
 */
public class incriptionformateurController implements Initializable {
     Formateur f = new Formateur();


    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;
    @FXML
    private TextField email;
    @FXML
    private PasswordField password;
    @FXML
    private TextField username;
    @FXML
    private ChoiceBox<String> cb;
    


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cb.getItems().addAll("IOS","Android","Windows Phone");
        
    }    
    
    /*@FXML
    public void openFile(ActionEvent event) throws IOException{ 
    
        Desktop d = Desktop.getDesktop();
        d.open(new File("D:/majdi.mp4"));
        
        
        
    }*/
    
    @FXML
    private void upload(ActionEvent event) throws IOException { 
        FileChooser fileChooser = new FileChooser();
       
        File file = fileChooser.showOpenDialog(null);
        String path = file.getAbsolutePath();
        System.out.println(path);
        System.out.println(file.getName());
        
        Path source = Paths.get(path);
        Path target = Paths.get("D:/aymen/"+file.getName());
        f.setNom(nom.getText());
        f.setPrenom(prenom.getText());
        f.setEmail(email.getText());
        f.setSpecialite(cb.getValue());
        f.setUsername(username.getText());
        f.setPassword(password.getText());     
        f.setPath(file.getName());
        FormateurDAO f1da = new FormateurDAO();
        f1da.inscritFormateur(f);
        try {
            System.out.println(Files.copy(source, target));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
    

    @FXML
    private void inscription(ActionEvent event) { 
//        try {
            Formateur f = new Formateur();
            f.setNom(nom.getText());
            f.setPrenom(prenom.getText());
            f.setEmail(email.getText());
            f.setSpecialite(cb.getValue());
            f.setUsername(username.getText());
            f.setPassword(password.getText());
            
            IFormateurDAO dao = new FormateurDAO();
            dao.inscritFormateur(f);
            
//            Parent home_page_parent = FXMLLoader.load(getClass().getResource("/authentification/GUI/acceuil.fxml"));
//            Scene home_page_scene = new Scene(home_page_parent);
//            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//
//            app_stage.setScene(home_page_scene);
//            app_stage.show();
//        } catch (IOException ex) {
//            Logger.getLogger(incriptionformateurController.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    @FXML
    private void retour(ActionEvent event) {
        try {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("/authentification/GUI/acceuil.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            app_stage.setScene(home_page_scene);
            app_stage.show();
        } catch (IOException ex) {
            Logger.getLogger(incriptionformateurController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
