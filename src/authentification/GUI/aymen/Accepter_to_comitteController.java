/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.aymen;


import authentification.DAO.FormateurDAO;
import authentification.ENTITIES.Formateur;
import authentification.IDAO.IFormateurDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author Aymen
 */
public class Accepter_to_comitteController implements Initializable {

    @FXML
    private ListView<Formateur> affiche;
    ObservableList<Formateur> liste = FXCollections.observableArrayList();
    @FXML
    private Label nom;
    @FXML
    private Label prenom;
    @FXML
    private Label username;
    @FXML
    private Label note;
    
    private Stage primaryStage ;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
         afficherlist();
    }    
    
    private void afficherlist() {
       IFormateurDAO dao = new FormateurDAO();
     List<Formateur> listformateurs = new ArrayList<Formateur>();
     listformateurs = dao.addToComite();
     for(Formateur f : listformateurs){
     liste.add(f);
     affiche.setItems(liste);
     affiche.getSelectionModel().selectedItemProperty().addListener((Observable, oldValue, newValue)-> Affichedetaill(newValue));
     }
    }

    @FXML
    private void accepter(ActionEvent event) {
         Formateur formateurs = new Formateur();
         formateurs = affiche.getSelectionModel().getSelectedItem();
         if(formateurs != null){
        affiche.getItems().remove(formateurs);
        
        new FormateurDAO().aceepteToCommitte(formateurs);
        }
    }

    private void Affichedetaill(Formateur formateurs) {
    
        if (formateurs !=null){
        nom.setText(formateurs.getNom()+"");
        prenom.setText(formateurs.getPrenom()+"");
        username.setText(formateurs.getUsername()+"");
        note.setText(formateurs.getSpecialite());
        
        }
        else {
            nom.setText("");
            prenom.setText("");
            username.setText("");
            note.setText("");
        }
    }

    @FXML
    private void retour(ActionEvent event) {
        try {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("/authentification/GUI/aymen/comite.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            app_stage.setScene(home_page_scene);
            app_stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Accepter_to_comiteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
