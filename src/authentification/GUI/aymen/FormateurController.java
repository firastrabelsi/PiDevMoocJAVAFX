/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.aymen;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class FormateurController implements Initializable {

    @FXML
    private ImageView img1;
    @FXML
    private ImageView img;
    @FXML
    private ImageView img2;
    @FXML
    private Button deconn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void modifier(ActionEvent event) {
        try {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("modifier_profile_formateurs.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            app_stage.setScene(home_page_scene);
            app_stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FormateurController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void deconn(ActionEvent event) {
        try {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("/authentification/GUI/acceuil.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            app_stage.setScene(home_page_scene);
            app_stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FormateurController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @FXML
    private void gestionquiz(ActionEvent event) {
        try {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("/authentification/GUI/firas/FXMLQuizlist.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            app_stage.setScene(home_page_scene);
            app_stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FormateurController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
   
    
}
