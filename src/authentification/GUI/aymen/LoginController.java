/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.aymen;


import authentification.DAO.FormateurDAO;
import authentification.ENTITIES.Formateur;
import authentification.IDAO.IFormateurDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Aymen
 */
public class LoginController implements Initializable {
    
    static String USERNAME;
    static Formateur user;

    @FXML
    private TextField username;
    @FXML
    private TextField email;
    @FXML
    private Button login;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void inscriform(ActionEvent event) throws IOException {
        
        
     Parent home_page_parent = FXMLLoader.load(getClass().getResource("inscription.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private void login(ActionEvent event) throws IOException {
        
        String role;
        String ROLE;
        USERNAME=username.getText();
        IFormateurDAO dao = new FormateurDAO();
        role = dao.veriflogin1(username.getText(), email.getText());
        System.out.println(role);
        
        if (role != null)
        {
          System.out.println(role);
            ROLE = role.substring(15, 25);
            System.out.println(ROLE);
            
            if(ROLE.equals("ROLE_FORMA"))
            {
               ((Node) event.getSource()).getScene().getWindow().hide(); 
            FXMLLoader loader = new FXMLLoader();
                
            loader.setLocation(getClass().getResource("modifier_profile_formateurs.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
                
            } else if ("ROLE_COMIT".equals(ROLE)){
                
                ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("affichage.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
            
            }
            else 
        {System.out.println("jbbuububububu");}
            
        }
        else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Message d'erreur");
        alert.setHeaderText(null);
        alert.setContentText("Votre login ou mot de passe est incorrect !");

        alert.showAndWait();
    }
        
    }
    
    
    
}
