/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.aymen;

import authentification.DAO.FormateurDAO;
import authentification.ENTITIES.Formateur;
import static authentification.GUI.LoginController.USERNAME;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Aymen
 */
public class Modifier_comiteController implements Initializable {

    @FXML
    private ImageView img1;
    @FXML
    private TextField username;
    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;
    @FXML
    private TextField email;
    @FXML
    private ImageView img;
    @FXML
    private Button retour;
    @FXML
    private Button Modifier;

    /**
     * Initializes the controller class.
     */
    
    public void AfficheAction(){
       FormateurDAO formateur = new FormateurDAO();
       Formateur format = formateur.findFormateurByUserName(USERNAME);
       System.out.println(USERNAME);
        System.out.println(format);
       if (format!=null){
       username.setText(format.getUsername());
       nom.setText(format.getNom());
       prenom.setText(format.getPrenom());
       email.setText(format.getEmail());
       
       }
       else{
       username.setText("champs vide");
       nom.setText("champs vide");
       prenom.setText("champ vide");
       email.setText("champ vide");
       }
        
    
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        AfficheAction();
    }    

    @FXML
    private void retour(ActionEvent event) {
        try {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("/authentification/GUI/aymen/comite.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            app_stage.setScene(home_page_scene);
            app_stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Modifier_comiteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void modifier(ActionEvent event) {
        try {
            int id=0;
            FormateurDAO formateur = new FormateurDAO();
            Formateur format = formateur.findFormateurByUserName(USERNAME);
            format.setUsername(username.getText());
            format.setNom(nom.getText());
            format.setPrenom(prenom.getText());
            format.setEmail(email.getText());
            System.out.println(format);
            id = formateur.findFormateurId(USERNAME);
            System.out.println(id);
            formateur.editComite(format, id);
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("/authentification/GUI/aymen/comite.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            app_stage.setScene(home_page_scene);
            app_stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Modifier_profile_formateursController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
