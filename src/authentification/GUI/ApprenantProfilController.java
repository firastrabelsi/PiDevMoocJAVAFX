/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.OrganismeDAO;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.apprenant;
import authentification.ENTITIES.organisme;
import static authentification.GUI.LoginController.USERNAME;
import authentification.IDAO.IapprenantDAO;
import authentification.IDAO.IorganismeDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class ApprenantProfilController implements Initializable {

    @FXML
    private Label label;

    

    @FXML
    private void profilAction(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("afficherDonneeProfil.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    @FXML
    private void seDeconnecterAction(ActionEvent event) throws IOException {
        USERNAME = null;
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("acceuil.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    @FXML
    private void CoursAction(ActionEvent event) throws IOException {
 ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("/authentification/GUI/anwer/AcceuilApprenant.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }

    @FXML
    private void listerOrganisme(ActionEvent event) throws IOException {
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("OrganismeList.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    @FXML
    private void listerquiz(ActionEvent event) throws IOException {

        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("/authentification/GUI/firas/FXMLQuizlist.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }

    @FXML
    private void listerFormateur(ActionEvent event) throws IOException {

    }

    @FXML
    private void SedésinscrireAction(ActionEvent event) throws IOException {

        apprenant a = new apprenant();
        IapprenantDAO dao = new apprenantDAO();

        int id = dao.findApprenantId(USERNAME);
        System.out.println(id);
        dao.deleteApprenant(id);
        USERNAME = null;
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("acceuil.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        label.setText(USERNAME);
    }

    private Object getScene() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
