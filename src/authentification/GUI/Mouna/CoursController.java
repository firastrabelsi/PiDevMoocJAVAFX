/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Mouna;

import authentification.DAO.CoursDAO;
import authentification.ENTITIES.Cours;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class CoursController implements Initializable {

    @FXML
    private TableColumn<Cours, String> c1;
    @FXML
    private TableColumn<Cours, String> c2;
    @FXML
    private TableColumn<Cours, Integer> c3;
    @FXML
    private TableColumn<Cours, Date> c4;
    @FXML
    private TableColumn<Cours, Date> c5;
    @FXML
    private TableColumn<Cours, String> c6;
    @FXML
    Button Chapitre;
    @FXML
    public TableView<Cours> table;
    @FXML
    Button retour;

    /**
     * Initializes the controller class.
     */
    //  @Override
    public static CoursController coursController;

    public CoursController() {
        coursController = this;
    }

    // int id = FormationController.formationController.table.getSelectionModel().getSelectedItem().getIdForm();

    public void initialize(URL url, ResourceBundle rb) {
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setTableMenuButtonVisible(true);
        c1.setCellValueFactory(new PropertyValueFactory<Cours, String>("titre"));
        c2.setCellValueFactory(new PropertyValueFactory<Cours, String>("description"));
        c3.setCellValueFactory(new PropertyValueFactory<Cours, Integer>("etat"));
        c4.setCellValueFactory(new PropertyValueFactory<Cours, Date>("datedeb"));
        c5.setCellValueFactory(new PropertyValueFactory<Cours, Date>("datefin"));
        c6.setCellValueFactory(new PropertyValueFactory<Cours, String>("difficulte"));

        List<Cours> cs = new ArrayList<>();
       cs = (new CoursDAO().DisplayAllObject());
        ObservableList<Cours> res = FXCollections.observableArrayList(cs);
        table.setItems(res);
        coursController = this;
    }

    public void onChapitreClick() {
        try {
            new SwitchScreens().Switch(Chapitre, "/authentification/GUI/Mouna/chapitre.fxml");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        
    }
     public void Retour() {
        try {

            new SwitchScreens().Switch(retour, "Formation.fxml");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
   /* public void table() {
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setTableMenuButtonVisible(true);
        c1.setCellValueFactory(new PropertyValueFactory<Cours, String>("titre"));
        c2.setCellValueFactory(new PropertyValueFactory<Cours, String>("description"));
        c3.setCellValueFactory(new PropertyValueFactory<Cours, Integer>("etat"));
        c4.setCellValueFactory(new PropertyValueFactory<Cours, Date>("datedeb"));
        c5.setCellValueFactory(new PropertyValueFactory<Cours, Date>("datefin"));
        c6.setCellValueFactory(new PropertyValueFactory<Cours, String>("difficulte"));

        List<Cours> cs = new ArrayList<>();
        // cs = (new CoursDao().displayallcoursformationva(id));
        ObservableList<Cours> res = FXCollections.observableArrayList(cs);
        table.setItems(res);
    } */
}

        // TODO

