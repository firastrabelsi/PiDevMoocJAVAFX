/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Mouna;

import authentification.DAO.ChapitreDao;
import authentification.DAO.CoursDAO;
import authentification.ENTITIES.Chapitre;
import authentification.GUI.anwer.CoursValiderApprenantController;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class ChapitreController implements Initializable {

    @FXML
    private TextField nom;
    @FXML
    private TextField description;
    @FXML
    private Button valider;
    @FXML
    private Button supprimer;
    @FXML
    private Button ajouter;
    @FXML
    private Button modifier;
    @FXML
    Label ID;
    @FXML
    TableView<Chapitre> table;
    public String anom, acontenu;
    @FXML
    TableColumn<Chapitre, String> NomChapitre;
    @FXML
    TableColumn<Chapitre, String> Description;
    @FXML Button Retour;

    /**
     * Initializes the controller class.
     */
    public static ChapitreController chapitreController;
    int id = CoursValiderApprenantController.coursController.tableview.getSelectionModel().getSelectedItem().getId();

    public void initialize(URL url, ResourceBundle rb) {
        ID.setText(String.valueOf(id));
        NomChapitre.setCellValueFactory(new PropertyValueFactory<Chapitre, String>("Nom"));
        Description.setCellValueFactory(new PropertyValueFactory<Chapitre, String>("Description"));
        ChapitreDao chapitreDAO = new ChapitreDao();
        List<Chapitre> res = new ArrayList<>();
        res = chapitreDAO.DisplayAllChapCours(id);
        ObservableList<Chapitre> chs = FXCollections.observableArrayList(res);
        for (Chapitre c : res) {
            System.out.println(c.getNom());
        }
        table.setItems(chs);
    }
    @FXML
    public void Retour()
    {
        try
        {
            new SwitchScreens().Switch(Retour, "/authentification/GUI/anwer/CoursValiderApprenant.fxml");
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    public void delete() {

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("CONFIRMATION");
        alert.setContentText("Do You Really Want To Delete This?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Object o = table.getSelectionModel().getSelectedItem();
            new ChapitreDao().deleteObject((Chapitre) o);
            table();
        } else {
            alert.close();
        }
    }
    //mm

    private void table() {

        try {
            ID.setText(String.valueOf(id));
            NomChapitre.setCellValueFactory(new PropertyValueFactory<Chapitre, String>("Nom"));
            Description.setCellValueFactory(new PropertyValueFactory<Chapitre, String>("Description"));
            ChapitreDao chapitreDAO = new ChapitreDao();
            List<Chapitre> res = new ArrayList<>();
            res = chapitreDAO.DisplayAllChapCours(id);
            ObservableList<Chapitre> chs = FXCollections.observableArrayList(res);
            for (Chapitre c : res) {
                System.out.println(c.getNom());
            }
            table.setItems(chs);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    @FXML
    public void modifier(ActionEvent event) {
        Chapitre o = (Chapitre) table.getSelectionModel().getSelectedItem();
        nom.setText(o.getNom());
        description.setText(o.getDescription());
        anom = nom.getText();
        acontenu = description.getText();
        valider.setVisible(true);
    }

    @FXML
    public void onValiderClick() {
        Chapitre chapitres = new Chapitre();
        ChapitreDao chapitresDAO = new ChapitreDao();
   //chapitres.setIdchap(chapitresDAO.findByNom(jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString()).getIdchap());
        chapitres.setId(table.getSelectionModel().getSelectedItem().getId());
        chapitres.setNom(nom.getText());
        chapitres.setDescription(description.getText());

        chapitresDAO.updateObject(chapitres);

        table();
        System.out.println("haa");
         }
    
    
    @FXML public void ajouter (ActionEvent event)  {
        System.out.println("mrigel");
        Alert  alert=new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        if(nom.getText().equals("") || description.getText().equals("") )
        {
            alert.setContentText("Remplissez tous les champs");
            alert.showAndWait();
        }
        else 
        {

            Chapitre chapitres;
            chapitres = new Chapitre (nom.getText(),description.getText());
            ChapitreDao chapitresDAO= new ChapitreDao();
//            try
//            {
            CoursDAO coursDAO = new CoursDAO();
        chapitres.setCour(coursDAO.findObjectById(id));
        System.out.println(chapitres.getCour().getId());
            
                chapitresDAO.insertObject(chapitres);
            }

                System.out.println("tout est bien");

table();  
//    }
    
       
}
}
    