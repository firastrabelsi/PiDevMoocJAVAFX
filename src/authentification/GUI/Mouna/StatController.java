/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Mouna;

import authentification.DAO.FormationDao;
import authentification.ENTITIES.Formation;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Shape;
import javafx.animation.Timeline;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class StatController implements Initializable {

    @FXML
    private PieChart PieChart;
    Arc movedSlice = null;
    double slideAngle;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
        List<Formation> list = new ArrayList<Formation>();
        list = (new FormationDao().DisplayAllObject());
        // List<Formation> list =FormationDao.getInstance; 
        for (Formation f : list) {
            PieChart.Data data = new PieChart.Data(f.getNomFormation(), f.getDuree());
            pieChartData.add(data);
        }

        PieChart.setTitle("Durée Des Formations");

        PieChart.setData(pieChartData);

    }

    private void setPieBit(final PieChart pie, PieChart.Data data) {
        data.getNode().setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent t) {
                Region selectedPiece = (Region) t.getSource();
                Arc selectedArc = (Arc) selectedPiece.impl_getStyleMap();
                if (movedSlice != null) {
                    slideSlice(true, movedSlice, 30);
                    if (movedSlice == selectedArc) {
                        movedSlice = null;
                        return;
                    }
                }
                slideAngle
                        = Math.abs(selectedArc.getStartAngle() + (selectedArc.getLength() / 2));
                slideSlice(false, selectedArc, 30);
                movedSlice = selectedArc;
            }
        });
    }

    private void slideSlice(boolean in, Arc piece, int slideLength) {
        double x = piece.getCenterX();
        double y = piece.getCenterY();

        double xDistance = slideLength * Math.cos(Math.toRadians(slideAngle));
        double yDistance = slideLength * Math.sin(Math.toRadians(slideAngle));
        if (in) {
            xDistance = x - xDistance;
            yDistance = y - yDistance;
        }
        DoubleProperty centreX = piece.centerXProperty();
        DoubleProperty centreY = piece.centerYProperty();

        Timeline slideTimeLine = new Timeline();
        slideTimeLine.getKeyFrames().add(new KeyFrame(Duration.ZERO,
                new KeyValue(centreX, x),
                new KeyValue(centreY, y)));
        slideTimeLine.getKeyFrames().add(new KeyFrame(new Duration(500),
                new KeyValue(centreX, x),
                new KeyValue(centreY, y)));
        slideTimeLine.playFromStart();
    }
}
