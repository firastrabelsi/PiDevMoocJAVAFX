/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Mouna;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class AcceuilController implements Initializable {

    @FXML
    Button formation;
    @FXML
    Button consultation;
    @FXML
    Button ajouter;

    /**
     * Initializes the controller class.
     */
    public static AcceuilController acceuilController;

    public AcceuilController() {
        acceuilController = this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        acceuilController = this;

        // TODO
    }

    public void onFormationClick() {
        try {
            new SwitchScreens().Switch(formation, "/authentification/GUI/Mouna/Formation.fxml");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void onReclamationClick() {
        try {
            new SwitchScreens().Switch(ajouter, "/authentification/GUI/Mouna/Reclamation.fxml");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
