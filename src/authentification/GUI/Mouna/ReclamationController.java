/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Mouna;

import authentification.DAO.ReclamationDao;
import authentification.ENTITIES.Reclamation;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import moooc.notification;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class ReclamationController implements Initializable {
     @FXML
    private TextField msg;
     @FXML
    private Button envoie;
public static String message;
    /**
     * Initializes the controller class.
     */
     public static ReclamationController reclamationController;

    public ReclamationController() {
        reclamationController = this;
    }
     
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        reclamationController = this;
        
        // TODO
    }  
        public void ajouter() {
         try {
           ReclamationController.message=msg.getText();
           // notification n=new notification();
            // n.start(new Stage());
              Reclamation reclamation;
            reclamation =new Reclamation(msg.getText());
            ReclamationDao reclamationDAO = new ReclamationDao();
            reclamationDAO.insertObject(reclamation);
         } catch (Exception ex) {
             Logger.getLogger(ReclamationController.class.getName()).log(Level.SEVERE, null, ex);
         }
        }
    
}
