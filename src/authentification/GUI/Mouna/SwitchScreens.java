/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Mouna;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 *
 * @author Dell
 */
public class SwitchScreens {
    public SwitchScreens() {
    }
    
    public void Switch(Button btn, String ressource) throws IOException
    {
        Stage stage;
        stage = (Stage)btn.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource(ressource));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    public void init(Stage stage, String ressource) throws IOException
    {
        Parent root = FXMLLoader.load(getClass().getResource(ressource));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
}
