/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.Mouna;

import authentification.DAO.FormationDao;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.Formation;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import moooc.mainos;

/**
 * FXML Controller class
 *
 * @author Dell
 */
public class FormationController implements Initializable {

    @FXML
    private TextField nom;
    @FXML
    private TextField duree;
    @FXML
    private TextField level;
    @FXML
    private TableColumn<Formation, String> c1;
    @FXML
    private TableColumn<Formation, Integer> c2;
    @FXML
    private TableColumn<Formation, String> c3;
    public TableView<Formation> table;
     @FXML private  PieChart PieChart;
    @FXML private Button statistique;
    @FXML
    Button cours;
    @FXML
    private Button valider;
    @FXML
    Button retour;
    public static FormationController formationController;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setTableMenuButtonVisible(true);
        c1.setCellValueFactory(new PropertyValueFactory<Formation, String>("nomFormation"));
        c2.setCellValueFactory(new PropertyValueFactory<Formation, Integer>("duree"));
        c3.setCellValueFactory(new PropertyValueFactory<Formation, String>("level"));
        List<Formation> cs = new ArrayList<Formation>();
        cs = (new FormationDao().DisplayAllObject());
        ObservableList<Formation> res = FXCollections.observableArrayList(cs);
        table.setItems(res);
        formationController = this;
    }

    public void ajouter() {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        if (nom.getText().equals("") || duree.getText().equals("") || level.getText().equals("")) {
            alert.setContentText("Remplissez tous les champs");
            alert.showAndWait();
        } else {

            Formation formation;
            formation = new Formation(nom.getText(), Integer.parseInt(duree.getText()), level.getText());
            FormationDao formationDAO = new FormationDao();

            formationDAO.insertObject(formation);
            table();
        }

    }

    public void table() {

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setTableMenuButtonVisible(true);
        c1.setCellValueFactory(new PropertyValueFactory<Formation, String>("nomFormation"));
        c2.setCellValueFactory(new PropertyValueFactory<Formation, Integer>("duree"));
        c3.setCellValueFactory(new PropertyValueFactory<Formation, String>("level"));

//     
        List<Formation> cs = new ArrayList<Formation>();
        cs = (new FormationDao().DisplayAllObject());
        ObservableList<Formation> res = FXCollections.observableArrayList(cs);
        table.setItems(res);

    }
public void cours() {

        try {
            new SwitchScreens().Switch(cours, "/authentification/GUI/anwer/CoursValiderApprenant.fxml");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    @FXML
    public void delete() {

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(" Confirmation Dialog");
        alert.setContentText("Voulez vous vraiment Supprimer cette formation?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {

            Formation o = table.getSelectionModel().getSelectedItem();
            new FormationDao().deleteObject(o);
            table();

        } else {
            alert.close();
        }

    }

    @FXML
    public void modifier() {

        Formation o = (Formation) table.getSelectionModel().getSelectedItem();
        nom.setText(o.getNomFormation());
        duree.setText(o.getDuree() + "");
        level.setText(o.getLevel());
        valider.setVisible(true);

    }

    @FXML
    public void onValiderClick() {
        Formation formation = new Formation();
        FormationDao formationDAO = new FormationDao();
        formation.setIdForm(table.getSelectionModel().getSelectedItem().getIdForm());
        formation.setNomFormation(nom.getText());
        formation.setDuree(Integer.parseInt(duree.getText()));
        formation.setLevel(level.getText());
        formationDAO.updateObject(formation);

        table();

    }
    @FXML
    public void statisique (){
//        
   mainos inscription = new mainos();
        Stage stage = new Stage();
        stage.setHeight(600);
        stage.setWidth(600);
        stage.sizeToScene();
        stage.show();
        try {
            inscription.start(stage);
        } catch (Exception ex) {
        
        
        
    }
}
    public void Retour() {
        try {

            new SwitchScreens().Switch(retour, "/authentification/GUI/Mouna/Acceuil.fxml");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
