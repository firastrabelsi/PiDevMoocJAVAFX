/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.OrganismeDAO;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.apprenant;
import authentification.ENTITIES.organisme;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class ListedesOrganismesValidesDASHController implements Initializable {
    
    
    @FXML
    private ListView<String> listview;
    @FXML
    private Label labelUserName;
    @FXML
    private Label labeladresse;
    @FXML
    private Label labelEmail;
    @FXML
    private Label UserName1;
    @FXML
    private Label adresse;
    @FXML
    private Label Email;
    @FXML
    private Label  nomsociete;

    
    List <organisme> Organismes = new ArrayList<>();
    
     public void showListApprenant(){
       ObservableList<String> listeOrganisme = FXCollections.observableArrayList();
       
         OrganismeDAO organisme= new OrganismeDAO();
        Organismes =organisme .displayAllOrganisme();
        for (organisme c : Organismes) {
                        listeOrganisme.add(c.getUserName());
                        listview.setItems(listeOrganisme);

        }
}
     
     
    @FXML
    private void AfficheAction(ActionEvent event) {
         OrganismeDAO organisme= new OrganismeDAO();
        String UserName =listview.getSelectionModel().getSelectedItem();
        organisme appr=organisme.findOrganismeByUserName(UserName);
       // categories= new CategorieDAO().findAll();
        if (appr!=null) {
            UserName1.setText(appr.getUserName());
            adresse.setText(appr.getAdresse());
            Email.setText(appr.getEmail());
            nomsociete.setText(appr.getNomDeLaSociete());
           /* labdatefin.setText(cour.getDatefin()+"");
            labdifficulte.setText(cour.getDifficulte());
            //System.out.println(cour.getCategorie());
            //labcatégorie.setText(categories.stream().filter(e->cour.getCategorie().equals(e)).findFirst().get().getCate());
            */
        }
        else{
        
        UserName1.setText("");
        adresse.setText("");
        Email.setText("");
        nomsociete.setText("");
       /* labdatefin.setText("");
        labdifficulte.setText("");
        labcatégorie.setText("");*/
        }

    }
    
    @FXML
    private void RetourAction(ActionEvent event) throws IOException
    { 
     ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("acceuilDash.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(p));
        stage.show();
        
    }
    
    
     /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       showListApprenant();
    }    
    
}
