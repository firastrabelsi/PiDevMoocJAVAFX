/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.firas;

import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.exam;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.timeline;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.total;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.totalpoint;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import authentification.DAO.AssignementDAO;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.Assignement;
import static authentification.GUI.LoginController.USERNAME;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.st;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Firas
 */
public class FXMLResultatController implements Initializable {

    @FXML
    private Text resultattext;
    @FXML
    private Text scoretext;
    MediaPlayer mediaplayer;
    Media musicsucce = new Media("file:///C:/Users/pc/Downloads/congratulations.mp3");
    Media musicechoue = new Media("file:///C:/Users/pc/Downloads/FailSoundEffect.mp3");
                Image image;


    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
                
   
    @FXML
    private ImageView img1; 
    @FXML
    private ImageView img2; 
                
     @FXML
    private void suivant(ActionEvent event) throws IOException {
    ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/ApprenantProfil.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        st.close();
        apprenantDAO appdao= new apprenantDAO();
        AssignementDAO adao= new AssignementDAO();
        Assignement a = new Assignement();
        float x = 100 * total / totalpoint;
        if (x > 50) {
            mediaplayer = new MediaPlayer(musicsucce);
            mediaplayer.setAutoPlay(true);
        } else {
            mediaplayer = new MediaPlayer(musicechoue);
            mediaplayer.setAutoPlay(true);
        }
        scoretext.setText(Integer.toString(total) + "/" + Integer.toString(totalpoint));
        resultattext.setText(Float.toString(x) + "%");
        
        a.setQuiz(exam.getID());
        a.setScore((int) x);
        a.setBadge(2);
        a.setUser(appdao.findApprenantId(USERNAME));
        adao.ajouter(a);
        
        
        if(exam.getType().equals("Final")){
        if (x >0) {
            
            Document document = new Document();
            
            try {
                PdfWriter.getInstance(document, new FileOutputStream("C:/Users/pc/Desktop/Certificat.pdf"));

                document.open();
                document.add(new Paragraph("Bravo vous avez reussit votre test Final."));
                

                document.add(new Paragraph("Votre score est egale a "+x));

                document.close();

            } catch (FileNotFoundException | DocumentException e) {
            }
// TODO Auto-generated catch block

        } else {
        }
        }
    }

}
