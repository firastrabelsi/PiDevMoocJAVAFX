/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.firas.Quiz.ChoixMultiple;

import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.exam;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.firas1;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.qinst;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.total;
import authentification.UTIL.VoiceReaderService;
import authentification.ENTITIES.Reponse;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.st;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Firas
 */
public class FXMLChoixMultiple3Controller implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private Label nomquiz ;
    @FXML
    private CheckBox chk1;
    @FXML
    private CheckBox chk2;
    @FXML
    private CheckBox chk3;
    @FXML
    private CheckBox chk4;
    @FXML
    private CheckBox chk5;
    @FXML
    private GridPane gp;
    /**
     *
     */
    @FXML
    public static Label closeclose ;
          

    
    
    List<Reponse> listrep = new ArrayList<>();
    int scorerep=0;
    boolean test;
    private Thread voiceReadingThread;
    @FXML
    public void speak(ActionEvent event) throws IOException
    {
    
    
    voiceReadingThread = new Thread () {
            public void run() {
                VoiceReaderService voiceService = new VoiceReaderService();
               
                voiceService.setVoice( "kevin16");
                voiceService.setText(qinst.getQuestiontext());
                voiceService.read();
            }
    
    
    
    };        voiceReadingThread.start();

            }
    public void suivant(ActionEvent event) throws IOException
    {
        test=false;
        scorerep=0;
        listrep = qinst.getListreponse();
        ///chk1
        if(chk1.isSelected())
        {
        if(listrep.get(0).isReponsecorrectradio())
        {
            test=true;
        }
        else
        {
            test=false;

        }
        }
        ///chk2
        if(chk2.isSelected())
        {
        if(listrep.get(1).isReponsecorrectradio())
        {
            test=true;

        }
        else
        {
            test=false;

        }
        }
        //chk3
        if(chk3.isSelected())
        {
        if(listrep.get(2).isReponsecorrectradio())
        {
            test=true;
        }
        else
        {
        test=false;

        }
        }

        
        if (test==true){
            scorerep=qinst.getPoint();
        }
                total=total+scorerep;

        /// fin des condition de recuperation 
        
        if (exam.getListquestion().size()-1==firas1)
        {
           ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/firas/FXMLResultat.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show(); 
        }
        else
        {
        firas1++;
        qinst = exam.getListquestion().get(firas1);
        listrep = qinst.getListreponse();
        if (qinst.getType().equals("Choix unique"))
        {
        if(listrep.size()==1)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ChoixUnique/FXMLChoixUnique1.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else if (listrep.size()==2)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ChoixUnique/FXMLChoixUnique2.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else if (listrep.size()==3)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ChoixUnique/FXMLChoixUnique3.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else if (listrep.size()==4)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ChoixUnique/FXMLChoixUnique4.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ChoixUnique/FXMLChoixUnique5.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        }
        
        else if (qinst.getType().equals("Choix multiple"))
        {
        if(listrep.size()==1)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLChoixMultiple1.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else if (listrep.size()==2)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLChoixMultiple2.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else if (listrep.size()==3)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLChoixMultiple3.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else if (listrep.size()==4)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLChoixMultiple4.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLChoixMultiple5.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        }
        
        else if (qinst.getType().equals("Reponse par texte"))
        {
        if(listrep.size()==1)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ReponseText/FXMLReponseText1.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else if (listrep.size()==2)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ReponseText/FXMLReponseText2.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else if (listrep.size()==3)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ReponseText/FXMLReponseText3.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else if (listrep.size()==4)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ReponseText/FXMLReponseText4.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        else
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ReponseText/FXMLReponseText5.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));                    st=stage;

            stage.show();
                }
        }
               
        }          
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        nomquiz.setText(qinst.getQuestiontext());
        listrep = qinst.getListreponse();

        
        chk1.setText(listrep.get(0).getReponsetext());
        chk2.setText(listrep.get(1).getReponsetext());
        chk3.setText(listrep.get(2).getReponsetext());
    }    
    
    
    
}
