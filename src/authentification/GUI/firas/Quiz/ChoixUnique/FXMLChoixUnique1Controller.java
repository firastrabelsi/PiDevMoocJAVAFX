/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.firas.Quiz.ChoixUnique;

import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.exam;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.firas1;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.qinst;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.total;
import authentification.UTIL.VoiceReaderService;
import authentification.ENTITIES.Reponse;
import static authentification.GUI.firas.FXMLPasserQuizEntrainementController.st;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Firas
 */
public class FXMLChoixUnique1Controller implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Label nomquiz ;
    @FXML
    private RadioButton chk1;
    @FXML
    private RadioButton chk2;
    @FXML
    private RadioButton chk3;
    @FXML
    private RadioButton chk4;
    @FXML
    private RadioButton chk5;
    @FXML
    private GridPane gp;
    /**
     *
     */
    @FXML
    public static Label closeclose ;
    
    
    
    List<Reponse> listrep = new ArrayList<>();
    int scorerep=0;
    boolean test;
    final ToggleGroup group = new ToggleGroup();

    private Thread voiceReadingThread;
    @FXML
    public void speak(ActionEvent event) throws IOException
    {
    
    
    voiceReadingThread = new Thread () {
            public void run() {
                VoiceReaderService voiceService = new VoiceReaderService();
               
                voiceService.setVoice( "kevin16");
                voiceService.setText(qinst.getQuestiontext());
                voiceService.read();
            }
    
    
    
    };        voiceReadingThread.start();

            }
    
    public void suivant(ActionEvent event) throws IOException
    {
        test=false;
        scorerep=0;
        listrep = qinst.getListreponse();
            if (group.getSelectedToggle() != null) {
            if (group.getSelectedToggle().getUserData().toString().equals("1"))
            {
                test=true;
            }
            }




        if (test==true){
            scorerep=qinst.getPoint();
        }
                total=total+scorerep;

        /// fin des condition de recuperation 
        
        if (exam.getListquestion().size()-1==firas1)
        {
           ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/FXMLResultat.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show(); 
        }
        else
        {
        firas1++;
        qinst = exam.getListquestion().get(firas1);
        listrep = qinst.getListreponse();
        if (qinst.getType().equals("Choix unique"))
        {
        if(listrep.size()==1)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLChoixUnique1.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else if (listrep.size()==2)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLChoixUnique2.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else if (listrep.size()==3)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLChoixUnique3.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else if (listrep.size()==4)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLChoixUnique4.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLChoixUnique5.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        }
        
        else if (qinst.getType().equals("Choix multiple"))
        {
        if(listrep.size()==1)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ChoixMultiple/FXMLChoixMultiple1.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else if (listrep.size()==2)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ChoixMultiple/FXMLChoixMultiple2.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else if (listrep.size()==3)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ChoixMultiple/FXMLChoixMultiple3.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else if (listrep.size()==4)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ChoixMultiple/FXMLChoixMultiple4.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ChoixMultiple/FXMLChoixMultiple5.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        }
        
        else if (qinst.getType().equals("Reponse par texte"))
        {
        if(listrep.size()==1)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ReponseText/FXMLReponseText1.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else if (listrep.size()==2)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ReponseText/FXMLReponseText2.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else if (listrep.size()==3)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ReponseText/FXMLReponseText3.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else if (listrep.size()==4)
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ReponseText/FXMLReponseText4.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        else
                {
            ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/authentification/GUI/firas/Quiz/ReponseText/FXMLReponseText5.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));st=stage;
            stage.show();
                }
        }
        }    
                
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        nomquiz.setText(qinst.getQuestiontext());
        listrep = qinst.getListreponse();

        
        chk1.setText(listrep.get(0).getReponsetext());
        chk1.setToggleGroup(group);
        chk1.setUserData(listrep.get(0).isReponsecorrectradio());

    }      
      
    
}
