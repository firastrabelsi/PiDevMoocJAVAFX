/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.firas;

import static authentification.GUI.firas.QuizAddController.idquiz;
import authentification.DAO.QuestionDAO;
import authentification.DAO.ReponseDAO;
import authentification.ENTITIES.Question;
import authentification.ENTITIES.Reponse;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Firas
 */
public class FXMLAddQuestionController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ImageView img;
    @FXML
    private ImageView img1;
    @FXML
    TextArea qtext;
    @FXML
    TextArea r1text;
    @FXML
    TextArea r2text;
    @FXML
    TextArea r3text;
    @FXML
    TextArea r4text;
    @FXML
    TextArea r5text;
    @FXML
    ChoiceBox choitype;
    @FXML
    TextField point;
    @FXML
    TextField rc1;
    @FXML
    TextField rc2;
    @FXML
    TextField rc3;
    @FXML
    TextField rc4;
    @FXML
    TextField rc5;
    @FXML
    CheckBox c1;
    @FXML
    CheckBox c2;
    @FXML
    CheckBox c3;
    @FXML
    CheckBox c4;
    @FXML
    CheckBox c5;

    QuestionDAO qdao = new QuestionDAO();
    ReponseDAO rdao = new ReponseDAO();
    Question question = new Question();
    Reponse reponse1 = new Reponse();
    Reponse reponse2 = new Reponse();
    Reponse reponse3 = new Reponse();
    Reponse reponse4 = new Reponse();
    Reponse reponse5 = new Reponse();
    ObservableList<String> typelist = FXCollections.observableArrayList("Choix unique", "Choix multiple", "Reponse par texte");

    @FXML
    private void TerminerAction(ActionEvent event) throws IOException {

        question.setQuestiontext(qtext.getText());
        question.setType(choitype.getSelectionModel().getSelectedItem().toString());
        question.setPoint(Integer.parseInt(point.getText()));
        qdao.ajouter(question, idquiz);

        ///////////////////////////111
        reponse1.setReponsetext(r1text.getText());
        reponse1.setReponsecorrecttext(rc1.getText());
        if (c1.isSelected()) {
            reponse1.setReponsecorrectradio(true);
        } else {
            reponse1.setReponsecorrectradio(false);
        }
        System.out.println(reponse1);
        if (r1text.getText().equals("")) {
            rdao.ajouter(reponse1, qdao.getquestionID(question.getQuestiontext()));
        }

        //////////////////////////22222
        reponse2.setReponsetext(r2text.getText());
        reponse2.setReponsecorrecttext(rc2.getText());
        if (c2.isSelected()) {
            reponse2.setReponsecorrectradio(true);
        } else {
            reponse2.setReponsecorrectradio(false);
        }
        System.out.println(reponse2);
        if (r2text.getText().equals("")) {
            rdao.ajouter(reponse2, qdao.getquestionID(question.getQuestiontext()));
        }
        //////////////////////////333333
        reponse3.setReponsetext(r3text.getText());
        reponse3.setReponsecorrecttext(rc3.getText());
        if (c3.isSelected()) {
            reponse3.setReponsecorrectradio(true);
        } else {
            reponse3.setReponsecorrectradio(false);
        }
        System.out.println(reponse3);
        if (r3text.getText().equals("")) {
            rdao.ajouter(reponse3, qdao.getquestionID(question.getQuestiontext()));
        }

        //////////////////////////44444
        reponse4.setReponsetext(r4text.getText());
        reponse4.setReponsecorrecttext(rc4.getText());
        if (c4.isSelected()) {
            reponse4.setReponsecorrectradio(true);
        } else {
            reponse4.setReponsecorrectradio(false);
        }
        System.out.println(reponse4);
        if (r4text.getText().equals("")) {
            rdao.ajouter(reponse4, qdao.getquestionID(question.getQuestiontext()));
        }

        //////////////////////////555555
        reponse5.setReponsetext(r5text.getText());
        reponse5.setReponsecorrecttext(rc5.getText());
        if (c5.isSelected()) {
            reponse5.setReponsecorrectradio(true);
        } else {
            reponse5.setReponsecorrectradio(false);
        }
        System.out.println(reponse5);
        if (r5text.getText().equals("")) {
            rdao.ajouter(reponse5, qdao.getquestionID(question.getQuestiontext()));
        }

        //////////////////////////
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("FXMLQuizlist.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }

    @FXML
    private void SuivantAction(ActionEvent event) throws IOException {

        question.setQuestiontext(qtext.getText());
        question.setType(choitype.getSelectionModel().getSelectedItem().toString());
        question.setPoint(Integer.parseInt(point.getText()));
        qdao.ajouter(question, idquiz);

        ///////////////////////////111
        reponse1.setReponsetext(r1text.getText());
        reponse1.setReponsecorrecttext(rc1.getText());
        if (c1.isSelected()) {
            reponse1.setReponsecorrectradio(true);
        } else {
            reponse1.setReponsecorrectradio(false);
        }
        System.out.println(reponse1);
        if (r1text.getText().equals("")) {
            rdao.ajouter(reponse1, qdao.getquestionID(question.getQuestiontext()));
        }

        //////////////////////////22222
        reponse2.setReponsetext(r2text.getText());
        reponse2.setReponsecorrecttext(rc2.getText());
        if (c2.isSelected()) {
            reponse2.setReponsecorrectradio(true);
        } else {
            reponse2.setReponsecorrectradio(false);
        }
        System.out.println(reponse2);
        if (r2text.getText().equals("")) {
            rdao.ajouter(reponse2, qdao.getquestionID(question.getQuestiontext()));
        }
        //////////////////////////333333
        reponse3.setReponsetext(r3text.getText());
        reponse3.setReponsecorrecttext(rc3.getText());
        if (c3.isSelected()) {
            reponse3.setReponsecorrectradio(true);
        } else {
            reponse3.setReponsecorrectradio(false);
        }
        System.out.println(reponse3);
        if (r3text.getText().equals("")) {
            rdao.ajouter(reponse3, qdao.getquestionID(question.getQuestiontext()));
        }

        //////////////////////////44444
        reponse4.setReponsetext(r4text.getText());
        reponse4.setReponsecorrecttext(rc4.getText());
        if (c4.isSelected()) {
            reponse4.setReponsecorrectradio(true);
        } else {
            reponse4.setReponsecorrectradio(false);
        }
        System.out.println(reponse4);
        if (r4text.getText().equals("")) {
            rdao.ajouter(reponse4, qdao.getquestionID(question.getQuestiontext()));
        }

        //////////////////////////555555
        reponse5.setReponsetext(r5text.getText());
        reponse5.setReponsecorrecttext(rc5.getText());
        if (c5.isSelected()) {
            reponse5.setReponsecorrectradio(true);
        } else {
            reponse5.setReponsecorrectradio(false);
        }
        System.out.println(reponse5);
        if (r5text.getText().equals("")) {
            rdao.ajouter(reponse5, qdao.getquestionID(question.getQuestiontext()));
        }

        //////////////////////////
        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("FXMLAddQuestion.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }

    @FXML
    private void RetourAction(ActionEvent event) throws IOException {
  
    ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("FXMLQuizlist.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        choitype.setItems(typelist);
    }

}
