/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.GUI.anwer;

import authentification.DAO.CategorieDAO;
import authentification.DAO.ImageDao;
import authentification.ENTITIES.Categories;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.ImageM;
import static authentification.GUI.anwer.ModifierCorsController.c;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class ModifierCategoriesController implements Initializable {
    public static Categories cat = new Categories();
    CategorieDAO catdao = new CategorieDAO();

    @FXML
    private TextField fil_type;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println(cat);
      fil_type.setText(cat.getCate());
    }    

    @FXML
    private void btn_modimage(ActionEvent event) throws IOException {
        
        
Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choisir image...");
       
        File file = fileChooser.showOpenDialog(fil_type.getScene().getWindow());
                if (file!= null) {
                    suppimg();
        Image image = new Image(file.toURI().toString());
       

        String Copystr = "";
        String Copystr1 = "";
        Random randomGenerator = new Random();
        int ran = randomGenerator.nextInt(100);

         Copystr1 ="m" + ran  + ".jpeg";
        Copystr = ImageM.CURRENT_DIR + "/" + Copystr1;
        copyFile(file, new File(Copystr));

        System.out.println(Copystr1);

          ImageM Media = new ImageM();
         ImageDao MediaDao = new ImageDao();
   
        Media.setName("test");
         
           Media.setPath(Copystr1);

      Media.setId(cat.getImage_id());
      
        Media.setPath(Copystr1);

     MediaDao.updaImageById(Media);                
                
               System.out.println(Media);
                
                
                }

    }

    @FXML
    private void btn_Modifier(ActionEvent event) {
        cat.setCate(fil_type.getText());
if (fil_type.getText() == null || fil_type.getText().length() == 0)        
{
           Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Invalide Champs");
            alert.setHeaderText("S'il vous plaît corriger les champs non valides ");
            alert.setContentText( "Type Categorie :(");
            alert.showAndWait();
}else{
            
Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Modifier");
        alert.setHeaderText("Categories  bien Modifier");
        alert.setContentText("merci :) ");
        alert.showAndWait();
    catdao.modifierCategorie(cat);

}
    }

    @FXML
    private void btn_retour(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("ConsulterCategories.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Consulter Categorie");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }
    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();

            // previous code: destination.transferFrom(source, 0, source.size());
            // to avoid infinite loops, should be:
            long count = 0;
            long size = source.size();
            while ((count += destination.transferFrom(source, count, size
                    - count)) < size)
                ;
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }
public void suppimg(){

        int im = cat.getImage_id();
        //supprimer Video
        String pa = catdao.SelectImage(im);
        File file = new File("D:/uploads/" + pa);

        file.deleteOnExit();


}
    
}
