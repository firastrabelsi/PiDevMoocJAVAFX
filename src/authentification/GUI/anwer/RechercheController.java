/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.GUI.anwer;

import authentification.DAO.CategorieDAO;
import authentification.DAO.CoursDAO;
import authentification.ENTITIES.Categories;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.SuivCours;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class RechercheController implements Initializable {
  CoursDAO coursdao = new CoursDAO();
        CategorieDAO categoriesdao = new CategorieDAO();

    @FXML
    private TableView<Cours> tableview;
 @FXML
    private ComboBox<Categories> categoriecombo;
   @FXML
    private TextField titreTextField;
    ObservableList<Cours> obslist = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        categoriecombo.setItems(FXCollections.observableArrayList(categoriesdao.findAll()));


   TableColumn<Cours, String> titre = new TableColumn<>("Titre");
        titre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        TableColumn<Cours, String> description = new TableColumn<>("Description");
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        TableColumn<Cours, String> dureedecours = new TableColumn<>("Dureedecours");
        dureedecours.setCellValueFactory(new PropertyValueFactory<>("dureedecours"));
        TableColumn<Cours, String> difficulte = new TableColumn<>("Difficulte");
        difficulte.setCellValueFactory(new PropertyValueFactory<>("difficulte"));
        TableColumn<Cours, String> categorie = new TableColumn<>("Categorie");
        categorie.setCellValueFactory(new PropertyValueFactory<>("categorie"));

        tableview.getColumns().addAll(titre, description, dureedecours, difficulte, categorie);
        tableview.setItems(coursdao.listcoursvalider());
tableview.setRowFactory(tv -> {
            TableRow<Cours> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    
                    try {
                        RechercherCoursValideController.rcval=row.getItem();
                        Parent home_page_parent = FXMLLoader.load(getClass().getResource("RechercherCoursValide.fxml"));
                        Scene home_page_scene = new Scene(home_page_parent);
                        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                    app_stage.setTitle(" Cours : "+row.getItem().getTitre());

                        app_stage.setScene(home_page_scene);
                        app_stage.show();
                    } catch (IOException ex) {
                        Logger.getLogger(CoursValiderPourOrForController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                  
                   

                }
            });
            return row;
        });                
    }    
    @FXML

    private void TableUpdate(SortEvent<Cours> event) {
tableview.getItems().clear();
        tableview.setItems(coursdao.listcoursvalider());
        tableview.refresh();
    }

    @FXML
    private void btn_Acceuil(ActionEvent event) throws IOException {
 Parent home_page_parent = FXMLLoader.load(getClass().getResource("AcceuilApprenant.fxml"));
                        Scene home_page_scene = new Scene(home_page_parent);
                        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                    app_stage.setTitle(" Accueil Apprenant ");

                        app_stage.setScene(home_page_scene);
                        app_stage.show();
    }
private Cours getSelectedCours() {
        final Cours selectedCours = tableview.getSelectionModel().getSelectedItem();
        return selectedCours;
    }

    

    @FXML
    private void btn_rech(ActionEvent event) {
        
     int catid = categoriecombo.getValue().getId();
     String tit = titreTextField.getText().trim();
        List<Cours> lsc = new ArrayList<>();
tableview.getItems().clear();
obslist.clear();

        lsc= coursdao.listcoursvalider();
        for (Cours cours : lsc) {
            if (cours.getTitre().equals(tit) || cours.getCategorie().getId()== catid) {
            obslist.add(cours);

                        tableview.setItems(obslist);

                
            }
            
        }
        
    }
    
}
