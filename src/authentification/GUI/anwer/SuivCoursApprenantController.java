/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.GUI.anwer;

import authentification.DAO.SuivCoursDAO;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.SuivCours;
import authentification.ENTITIES.apprenant;
import authentification.GUI.LoginController;
import static authentification.GUI.anwer.VoirSuivCoursFormaOrganController.vsfo;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class SuivCoursApprenantController implements Initializable {
    SuivCoursDAO suivcoursdao = new SuivCoursDAO();

    @FXML
    private TableView<SuivCours> tableview;
    ObservableList<SuivCours> listesuiv = FXCollections.observableArrayList();
    List<SuivCours> lisuiv = new ArrayList<SuivCours>();
    apprenantDAO apdao = new apprenantDAO();




    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    TableColumn<SuivCours, Integer> id = new TableColumn<>("ID");
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn<SuivCours, String> nomcors = new TableColumn<>("Titre Cours");
        nomcors.setCellValueFactory(new PropertyValueFactory<>("nomcors"));
        TableColumn<SuivCours, String> nomappren = new TableColumn<>("Nom Apprenant");
        nomappren.setCellValueFactory(new PropertyValueFactory<>("nomappren"));
       

        tableview.getColumns().addAll(id, nomcors, nomappren);
        //tableview.setItems(coursdao.listcoursvalider());
         lisuiv = suivcoursdao.listsuivcours();
for (SuivCours s : lisuiv) {
//MOdifier dans l'integration 
    if (s.getApprenant().getId()== apdao.findApprenantByUserName(LoginController.USERNAME).getId()) {
        
    
            System.out.println("mmmm"+s.toString());

            listesuiv.add(s);
            tableview.setItems(listesuiv);
}
        }

        int x = suivcoursdao.listsuivcoursApprenant().size();
        System.out.println("ddd"+x);
tableview.setRowFactory(tv -> {
            TableRow<SuivCours> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    
                  Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Supprimer suivi");
        alert.setHeaderText("Supprimer suivi !!!!");
        alert.setContentText("Est ce que vous voulez supprimer ce suivi!!");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {

     suivcoursdao.supprimersuivById(row.getItem().getId());
            TableUpdate();
        }
                   

                }
            });
            return row;
        });         
    }    
    @FXML
    private void TableUpdate() {
  tableview.getItems().clear();
         lisuiv = suivcoursdao.listsuivcours();
for (SuivCours s : lisuiv) {
//MOdifier dans l'integration 
    if (s.getApprenant().getId()== apdao.findApprenantByUserName(LoginController.USERNAME).getId()) {
        
    
            System.out.println("mmmm"+s.toString());

            listesuiv.add(s);
            tableview.setItems(listesuiv);
}
        }
        tableview.refresh();
        
    }

    @FXML
    private void RetourCours(ActionEvent event) throws IOException {
Parent home_page_parent = FXMLLoader.load(getClass().getResource("AcceuilApprenant.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Suivi Apprenant");

        app_stage.setScene(home_page_scene);
        app_stage.show();
        
    }
private SuivCours getSelectedsuiv() {
        final SuivCours selectedsuiv = tableview.getSelectionModel().getSelectedItem();
        return selectedsuiv;
    }

   

   
    
}
