/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.GUI.anwer;

import authentification.DAO.CoursDAO;
import authentification.DAO.SuivCoursDAO;
import authentification.DAO.VideoDao;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.SuivCours;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class VoirSuivCoursFormaOrganController implements Initializable {
    CoursDAO coursdao = new CoursDAO();
    SuivCoursDAO suivcoursdao = new SuivCoursDAO();
    static Cours vsfo= new Cours();
    @FXML
    private TableView<SuivCours> tableview;
    ObservableList<SuivCours> listesuiv = FXCollections.observableArrayList();
    List<SuivCours> lisuiv = new ArrayList<SuivCours>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TableColumn<SuivCours, String> nomcors = new TableColumn<>("Cours");
        nomcors.setCellValueFactory(new PropertyValueFactory<>("nomcors"));
        TableColumn<SuivCours, String> nomappren = new TableColumn<>("Apprenant");
        nomappren.setCellValueFactory(new PropertyValueFactory<>("nomappren"));
        

        tableview.getColumns().addAll(nomcors, nomappren);
          lisuiv = suivcoursdao.listsuivcours();
for (SuivCours s : lisuiv) {
//MOdifier dans l'integration 
    if (s.getCours().getId()==vsfo.getId()) {
        
    
            System.out.println("mmmm"+s.toString());

            listesuiv.add(s);
            tableview.setItems(listesuiv);
}
        }
        
        
        
 tableview.setRowFactory(tv -> {
            TableRow<SuivCours> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    System.out.println("aaanwer"+vsfo);
                   Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Supprimer suivi");
        alert.setHeaderText("Supprimer suivi !!!!");
        alert.setContentText("Est ce que vous voulez supprimer ce suivi!!");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {

     suivcoursdao.supprimersuivById(row.getItem().getId());
            TableUpdate();
        }

                }
            });
            return row;
        });


    }    

    @FXML
    private void TableUpdate() {
   tableview.getItems().clear();
      for (SuivCours s : lisuiv) {
//MOdifier dans l'integration 
    if (s.getApprenant().getId()== vsfo.getId()) {
        
    
            System.out.println("mmmm"+s.toString());

            listesuiv.add(s);
            tableview.setItems(listesuiv);
}
        }
        tableview.refresh();
    }

    @FXML
    private void RetourCours(ActionEvent event) throws IOException {
   
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("ConsulterCoursValiderformOrgan.fxml"));
                        Scene home_page_scene = new Scene(home_page_parent);
                        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                                            app_stage.setTitle(" Cours : "+vsfo.getTitre());

                        app_stage.setScene(home_page_scene);
                        app_stage.show();

        
    }


}
