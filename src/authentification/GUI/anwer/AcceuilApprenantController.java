/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.GUI.anwer;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class AcceuilApprenantController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void btn_coursCours(ActionEvent event) throws IOException {
 Parent home_page_parent = FXMLLoader.load(getClass().getResource("CoursValiderApprenant.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle(" Cours  Valide ");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private void btn_consultersuiv(ActionEvent event) throws IOException {
 Parent home_page_parent = FXMLLoader.load(getClass().getResource("SuivCoursApprenant.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Suivi Apprenant");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private void btn_rech(ActionEvent event) throws IOException {
 Parent home_page_parent = FXMLLoader.load(getClass().getResource("Recherche.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Rechercher Cours");

        app_stage.setScene(home_page_scene);
        app_stage.show();        
        
    }
     @FXML
    private void retour(ActionEvent event) throws IOException {
 ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("/authentification/GUI/ApprenantProfil.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.show();
    }
}
