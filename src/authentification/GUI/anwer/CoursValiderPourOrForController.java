/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.GUI.anwer;

import authentification.DAO.CoursDAO;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.apprenant;
import authentification.GUI.LoginController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class CoursValiderPourOrForController implements Initializable {
    CoursDAO coursdao = new CoursDAO();
    apprenantDAO apdao = new apprenantDAO();
    

    @FXML
    private TableView<Cours> tableview;
  

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TableColumn<Cours, String> titre = new TableColumn<>("Titre");
        titre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        TableColumn<Cours, String> description = new TableColumn<>("Description");
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        TableColumn<Cours, String> dureedecours = new TableColumn<>("Dureedecours");
        dureedecours.setCellValueFactory(new PropertyValueFactory<>("dureedecours"));
        TableColumn<Cours, String> difficulte = new TableColumn<>("Difficulte");
        difficulte.setCellValueFactory(new PropertyValueFactory<>("difficulte"));
        TableColumn<Cours, String> categorie = new TableColumn<>("Categorie");
        categorie.setCellValueFactory(new PropertyValueFactory<>("categorie"));

        tableview.getColumns().addAll(titre, description, dureedecours, difficulte, categorie);
        // a modfier dans l'integration
        tableview.setItems(coursdao.listeCoursformateurOrOrganismeValider(apdao.findApprenantByUserName(LoginController.USERNAME).getId()));

        tableview.setRowFactory(tv -> {
            TableRow<Cours> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    
                    try {
                        ConsulterCoursValiderformOrganController.cvfo=row.getItem();
                        VoirSuivCoursFormaOrganController.vsfo=row.getItem();
                        Parent home_page_parent = FXMLLoader.load(getClass().getResource("ConsulterCoursValiderformOrgan.fxml"));
                        Scene home_page_scene = new Scene(home_page_parent);
                        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                app_stage.setTitle(" Cours : "+row.getItem().getTitre());

                        app_stage.setScene(home_page_scene);
                        app_stage.show();
                    } catch (IOException ex) {
                        Logger.getLogger(CoursValiderPourOrForController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                  
                   

                }
            });
            return row;
        });        
        
    }   
  

    @FXML
    private void ModifierCours(ActionEvent event) throws IOException {
        ModifierCorsController.c = getSelectedCours();
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("ModifierCors.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle(" Modifier Cours");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    private Cours getSelectedCours() {
        final Cours selectedCours = tableview.getSelectionModel().getSelectedItem();
        return selectedCours;
    }

   

   
    @FXML
    private void TableUpdate(SortEvent<Cours> event) {
   tableview.getItems().clear();
        //modifier dans l'integration
        tableview.setItems(coursdao.listeCoursformateurOrOrganismeValider(apdao.findApprenantByUserName(LoginController.USERNAME).getId()));
        tableview.refresh();
    }

    @FXML
    private void btn_Acceuil(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("Acceuilformorgan.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Accueil Cours Formateur Organisme");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

  
    
}
