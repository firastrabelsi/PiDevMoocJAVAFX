/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.GUI.anwer;

import authentification.DAO.CoursDAO;
import authentification.DAO.VideoDao;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.Cours;
import authentification.GUI.LoginController;
import static authentification.GUI.anwer.AfficherCoursNonValiderFOController.cnvfo;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class AfficheDetailleCoursAvaliderController implements Initializable {
CoursDAO coursdao = new CoursDAO();
    static Cours afdcaval = new Cours();
    apprenantDAO apdaou = new apprenantDAO();

    @FXML
    private Label formateur;
    @FXML
    private Label categorie;
    @FXML
    private Label titre;
    @FXML
    private Label description;
    @FXML
    private Label difficulte;
    @FXML
    private Label duree;
    @FXML
    private Button play;
    @FXML
    private Button pause;
    @FXML
    private Button stop;
    @FXML
    private Button full;
    @FXML
    private Button resize;
    @FXML
    private MediaView me1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        formateur.setText(afdcaval.getFormateur().getUsername());
titre.setText(afdcaval.getTitre());
        categorie.setText(afdcaval.getCategorie().getCate());
        duree.setText(String.valueOf(afdcaval.getDureedecours()));
        difficulte.setText(afdcaval.getDifficulte());
        description.setText(afdcaval.getDescription());
        System.out.println(afdcaval);
        
        
        
        
       String path = coursdao.SelectVideo(afdcaval.getVedio_id());
 File file = new File("D:/uploads/" + path);

        String source = file.toURI().toString();

        Media media = new Media(source);

        MediaPlayer mediaPlayer = new MediaPlayer(media);
        me1.setFitHeight(160);
        me1.setFitWidth(300);
        me1.setX(370);
        me1.setY(365);

        me1.setMediaPlayer(mediaPlayer);
      
        play.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                mediaPlayer.play();
            }
        });
        pause.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                mediaPlayer.pause();
            }
        });
        stop.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                mediaPlayer.stop();
            }
        });
        full.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                me1.setFitHeight(250);
                me1.setFitWidth(800);
                me1.setX(400);
                me1.setY(400);
            }
        });
        resize.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                me1.setFitHeight(160);
                me1.setFitWidth(300);
                me1.setX(370);
                me1.setY(365);
            }
        });        
    }    

    @FXML
    private void Retourner(ActionEvent event) throws IOException {
  Parent home_page_parent = FXMLLoader.load(getClass().getResource("CoursAvalider.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private void valider(ActionEvent event) {
        
        
        
        
        
  Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Valider Cours");
        alert.setHeaderText("Valider cours !!!!");
        alert.setContentText("Est ce que vous voulez Valider ce cours!!");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {

      

            //valider cours
            coursdao.modifieretat(afdcaval.getId());

        }
    }
    
}
