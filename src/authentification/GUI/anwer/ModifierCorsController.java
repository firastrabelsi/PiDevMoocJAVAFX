/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.anwer;

import authentification.DAO.CategorieDAO;
import authentification.DAO.CoursDAO;
import authentification.DAO.VideoDao;
import authentification.ENTITIES.Categories;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.VideoM;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class ModifierCorsController implements Initializable {

    public static Cours c = new Cours();
    CategorieDAO categoriesdao = new CategorieDAO();
    CoursDAO cdao = new CoursDAO();
    @FXML
    private TextField titreTextField;
    @FXML
    private TextArea descriptioneditProbTextArea;
    @FXML
    private TextField dureedecourstextefieald;
    @FXML
    private ComboBox<Categories> categoriecomboajout;
    @FXML
    private ComboBox<String> difficucomboajout;
    @FXML
    private Button Retourner;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        categoriecomboajout.setItems(FXCollections.observableArrayList(categoriesdao.findAll()));
        difficucomboajout.getItems().addAll("Facile", "Moyenne", "Deficile");
        titreTextField.setText(c.getTitre());
        descriptioneditProbTextArea.setText(c.getDescription());
        dureedecourstextefieald.setText(String.valueOf(c.getDureedecours()));
        categoriecomboajout.setValue(c.getCategorie());
        difficucomboajout.setValue(c.getDifficulte());
    }

    @FXML
    private void Modifer(ActionEvent event) {
        if (verification()) {

            c.setTitre(titreTextField.getText());
            c.setDescription(descriptioneditProbTextArea.getText());
            c.setDureedecours(Integer.parseInt(dureedecourstextefieald.getText()));
            c.setDifficulte(difficucomboajout.getValue());
            c.setCategorie(categoriecomboajout.getValue());
        }
        cdao.modifierCours(c);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Modifier");
        alert.setHeaderText("Cours bien modifier");
        alert.setContentText("merci :) ");
        alert.showAndWait();
    }

    @FXML

    private void ModifierVideo(ActionEvent event) throws IOException {
        

        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choisir image...");

        File file = fileChooser.showOpenDialog(Retourner.getScene().getWindow());
        if(file==null)
            System.out.println(c.getId()+"anwer saadaoui  111");
        


        if (file!= null) {
            System.out.println("anwer saadaoui  1");
            suppved();
            System.out.println("anwer saadaoui  2");

        Image image = new Image(file.toURI().toString());

        String Copystr = "";
        String Copystr1 = "";
        Random randomGenerator = new Random();
        int ran = randomGenerator.nextInt(100);

        Copystr1 = "m" + ran + ".mp4";
                    System.out.println(Copystr1+"anwer saadaoui  3");

        Copystr = VideoM.CURRENT_DIR + "/" + Copystr1;
        copyFile(file, new File(Copystr));

        System.out.println(Copystr1);

        VideoM Media = new VideoM();
        VideoDao MediaDao = new VideoDao();

        Media.setName("1");

        Media.setPath(Copystr1);

        Media.setPath(Copystr1);
        Media.setId(c.getVedio_id());
            System.out.println(c.getId()+"anwer saadaoui  11");


        MediaDao.updaVideoById(Media);}
    }

    @FXML
    private void handleRouterner(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("AfficherFormatOganCoursNonValider.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle(" Cours Non Valide  Formateur Organisme");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();

            // previous code: destination.transferFrom(source, 0, source.size());
            // to avoid infinite loops, should be:
            long count = 0;
            long size = source.size();
            while ((count += destination.transferFrom(source, count, size
                    - count)) < size)
                ;
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    private boolean verification() {
        String errorMessage = "";

        if (titreTextField.getText() == null || titreTextField.getText().length() == 0) {
            errorMessage += "Invalide Titre!\n";
        }
        if (descriptioneditProbTextArea.getText() == null || descriptioneditProbTextArea.getText().length() == 0) {
            errorMessage += "Invalide Description!\n";
        }
        if (dureedecourstextefieald.getText() == null || dureedecourstextefieald.getText().length() == 0) {
            errorMessage += "Invalide Duree!\n";
        } else {
            // try to parse the postal code into an int
            try {
                Integer.parseInt(dureedecourstextefieald.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Invalide Duree (doit être un entier)!\n";
            }
        }
        if (difficucomboajout.getValue() == null || difficucomboajout.getValue().length() == 0) {
            errorMessage += "Invalide champs Difficulté!\n";
        }
        if (categoriecomboajout.getValue() == null || categoriecomboajout.getValue().getCate().length() == 0) {
            errorMessage += "Invalide champs Categorie!\n";
        }
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // afichher  message de erreur 

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Invalide Champs");
            alert.setHeaderText("S'il vous plaît corriger les champs non valides ");
            alert.setContentText(errorMessage + ":(");
            alert.showAndWait();
            return false;
        }
    }
    
public void suppved(){

        int iv = c.getVedio_id();
        //supprimer Video
        String pa = cdao.SelectVideo(iv);
        File file = new File("D:/uploads/" + pa);

        file.deleteOnExit();


}

}
