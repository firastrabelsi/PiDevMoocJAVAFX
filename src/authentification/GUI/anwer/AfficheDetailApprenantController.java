/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.anwer;

import authentification.DAO.CoursDAO;
import authentification.DAO.SuivCoursDAO;
import authentification.DAO.VideoDao;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.SuivCours;
import authentification.ENTITIES.apprenant;
import authentification.GUI.LoginController;
import authentification.GUI.Majdi.CommentController;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class AfficheDetailApprenantController implements Initializable {
    int id_suiv;
            List<SuivCours> lisuiv = new ArrayList<SuivCours>();
    SuivCoursDAO suivcoursdao = new SuivCoursDAO();
    apprenantDAO apdaou = new apprenantDAO();
    

    CoursDAO coursdao = new CoursDAO();
    public static Cours afdcapp ;
    @FXML
    private Label formateur;
    @FXML
    private Label categorie;
    @FXML
    private Label titre;
    @FXML
    private Label description;
    @FXML
    private Button btn_sincrire;
    @FXML
    private Label difficulte;
    @FXML
    private Label duree;
    @FXML
    private Button play;
    @FXML
    private Button pause;
    @FXML
    private Button stop;
    @FXML
    private Button full;
    @FXML
    private Button resize;
    @FXML
    private MediaView me1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        titre.setText(afdcapp.getTitre());
        categorie.setText(afdcapp.getCategorie().getCate());
        duree.setText(String.valueOf(afdcapp.getDureedecours()));
        difficulte.setText(afdcapp.getDifficulte());
        description.setText(afdcapp.getDescription());
        System.out.println(afdcapp);

        String path = coursdao.SelectVideo(afdcapp.getVedio_id());
        File file = new File("D:/uploads/" + path);

        String source = file.toURI().toString();

        Media media = new Media(source);

        MediaPlayer mediaPlayer = new MediaPlayer(media);
        me1.setFitHeight(160);
        me1.setFitWidth(300);
        me1.setX(370);
        me1.setY(365);

        me1.setMediaPlayer(mediaPlayer);

        play.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                mediaPlayer.play();
            }
        });
        pause.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                mediaPlayer.pause();
            }
        });
        stop.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                mediaPlayer.stop();
            }
        });
        full.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                me1.setFitHeight(250);
                me1.setFitWidth(800);
                me1.setX(400);
                me1.setY(400);
            }
        });
        resize.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                me1.setFitHeight(160);
                me1.setFitWidth(300);
                me1.setX(370);
                me1.setY(365);
            }
        });
        if (VerifInscription()==0)
        {
        btn_sincrire.setText("s'inscrire");
        
        }else
        {
                btn_sincrire.setText("désinscrire");

        
        }     
    }

    @FXML
    private void Retourner(ActionEvent event) throws IOException {
  Parent home_page_parent = FXMLLoader.load(getClass().getResource("CoursValiderApprenant.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle(" Cours  Valide ");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private void sincrire(ActionEvent event) {
//modfier dans intégration        
        int nb;
        nb = VerifInscription();
        if(nb==0){
   ////Modifier dans integration
        
Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("s'inscrire");
        alert.setHeaderText("s'inscrire dans un cours !!!!");
        alert.setContentText("Est ce que vous voulez s'inscrire dans ce cours !!");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            
             SuivCours su = new SuivCours();
            su.setCours(afdcapp);
            apprenant ap = new apprenant();
            ap = apdaou.findApprenantByUserName(LoginController.USERNAME);
                    
            su.setApprenant(ap);
        suivcoursdao.ajouterSuivCours(su);
                 btn_sincrire.setText("désinscrire");
           

        }
        }else 
        {
  Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("désinscrire");
        alert.setHeaderText("désinscrire a partire d'un cours !!!!");
        alert.setContentText("Est ce que vous voulez désinscrire a partire d'un cours !!");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
         suivcoursdao.supprimersuivById(id_suiv);
                 btn_sincrire.setText("s'inscrire");

           

        }
        
        
        }
            
        
        
    }
    public int  VerifInscription(){
        int nbv=0;
         lisuiv = suivcoursdao.listsuivcours();
         for (SuivCours s : lisuiv) {
//MOdifier dans l'integration 
    if (s.getApprenant().getId()== apdaou.findApprenantByUserName(LoginController.USERNAME).getId() && afdcapp.getId()== s.getCours().getId() ) {
        
    nbv++;
            System.out.println("anwer"+s.toString());
            id_suiv=s.getId();
System.out.println(id_suiv+"999999999999999999999999");
           
}
        }
         return nbv;


    }
    @FXML
    private void handleButtonTOP5Action(ActionEvent event) {
          try {

            FXMLLoader update = new FXMLLoader(getClass().getResource("authentification/GUI/Majdi/Comment.fxml"));

            Scene scene = new Scene((Pane) update.load());
            Stage stage = new Stage();
stage.getIcons().setAll(new Image(getClass().getResource("authentification/GUI/Majdi/icon.png").toExternalForm()));
            
            stage.setScene(scene);

           // stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(CommentController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
     }
    
    
}
