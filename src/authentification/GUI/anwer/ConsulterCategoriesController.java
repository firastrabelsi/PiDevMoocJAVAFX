/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.anwer;

import authentification.DAO.CategorieDAO;
import authentification.DAO.ImageDao;
import authentification.DAO.SuivCoursDAO;
import authentification.DAO.VideoDao;
import authentification.ENTITIES.Categories;
import authentification.ENTITIES.Cours;
import authentification.ENTITIES.ImageM;
import static authentification.GUI.anwer.AfficheDetailleCoursAvaliderController.afdcaval;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class ConsulterCategoriesController implements Initializable {
    CategorieDAO catdao = new CategorieDAO();

    @FXML
    private TableView<Categories> tableview;
    @FXML
    private ImageView imagv1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TableColumn<Categories, String> id = new TableColumn<>("id");
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn<Categories, String> cate = new TableColumn<>("Type Categorie");
        cate.setCellValueFactory(new PropertyValueFactory<>("cate"));
        tableview.getColumns().addAll(id, cate);
        tableview.setItems(catdao.findAll());
 tableview.setRowFactory(tv -> {
            TableRow<Categories> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    
                        Categories ca =row.getItem();
                               String path = catdao.SelectImage(ca.getImage_id());
 File file = new File("D:/uploads/" + path);
         Image image = new Image(file.toURI().toString());

        imagv1.setImage(image);

 imagv1.setFitHeight(660);
                imagv1.setFitWidth(350);
                imagv1.setX(50);
                imagv1.setY(50);                     
                   

                }
            });
            return row;
        });

    }



    @FXML
    private void btn_Ajou(ActionEvent event) throws IOException {
  Parent home_page_parent = FXMLLoader.load(getClass().getResource("ajouterCategorie.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Ajouter Categorie ");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private void btn_mod(ActionEvent event) throws IOException {
        ModifierCategoriesController.cat=getSelectedCategorie();
        System.out.println(ModifierCategoriesController.cat );
  Parent home_page_parent = FXMLLoader.load(getClass().getResource("ModifierCategories.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Modifier Categorie ");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private void btn_sup(ActionEvent event) {

        Categories supcat = getSelectedCategorie();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Supprimer Categorie");
        alert.setHeaderText("Supprimer Categorie !!!!");
        alert.setContentText("Est ce que vous voulez supprimer ce Categorie!!");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {

            ImageDao   imgDao = new ImageDao();
            int im = supcat.getImage_id();
            //supprimer image
            String pa = catdao.SelectImage(im);
            File file = new File("D:/uploads/" + pa);

            file.deleteOnExit();

            imgDao.removeImageById(im);

            //supprimer cours
            catdao.supprimerCategorieById(supcat.getId());
            TableUpdate();

        }

    }
    private Categories getSelectedCategorie() {
        final Categories selectedCat = tableview.getSelectionModel().getSelectedItem();
        return selectedCat;
    }
 @FXML
    private void TableUpdate() {
 tableview.getItems().clear();
        //modifier dans l'integration
        tableview.setItems(catdao.findAll());
        tableview.refresh();
    }

    @FXML
    private void btn_accueil(ActionEvent event) throws IOException {
  Parent home_page_parent = FXMLLoader.load(getClass().getResource("Acceuilformorgan.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Accueil Cours Formateur Organisme");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

}
