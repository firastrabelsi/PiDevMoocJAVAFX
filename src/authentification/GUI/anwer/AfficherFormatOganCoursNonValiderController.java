/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI.anwer;

import authentification.DAO.CoursDAO;
import authentification.DAO.VideoDao;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.Cours;
import authentification.GUI.LoginController;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Anouar
 */
public class AfficherFormatOganCoursNonValiderController implements Initializable {

    CoursDAO coursdao = new CoursDAO();

    @FXML
    private TableView<Cours> tableview;
    @FXML
    private Button ajouter;
    @FXML
    private Button modifier;
    @FXML
    private Button supprimer;
    @FXML
    private Button voir;
   
    apprenantDAO apdaou = new apprenantDAO();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TableColumn<Cours, String> titre = new TableColumn<>("Titre");
        titre.setCellValueFactory(new PropertyValueFactory<>("titre"));
        TableColumn<Cours, String> description = new TableColumn<>("Description");
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        TableColumn<Cours, String> dureedecours = new TableColumn<>("Dureedecours");
        dureedecours.setCellValueFactory(new PropertyValueFactory<>("dureedecours"));
        TableColumn<Cours, String> difficulte = new TableColumn<>("Difficulte");
        difficulte.setCellValueFactory(new PropertyValueFactory<>("difficulte"));
        TableColumn<Cours, String> categorie = new TableColumn<>("Categorie");
        categorie.setCellValueFactory(new PropertyValueFactory<>("categorie"));

        tableview.getColumns().addAll(titre, description, dureedecours, difficulte, categorie);
        // a modfier dans l'integration
        tableview.setItems(coursdao.listeCoursformateurOrOrganismeNonValider(apdaou.findApprenantByUserName(LoginController.USERNAME).getId()));
        //supprimer cours
        tableview.setRowFactory(tv -> {
            TableRow<Cours> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    try {
                        AfficherCoursNonValiderFOController.cnvfo=row.getItem();
                        ModifierCorsController.c = getSelectedCours();
                        Parent home_page_parent = FXMLLoader.load(getClass().getResource("AfficherCoursNonValiderFO.fxml"));
                        Scene home_page_scene = new Scene(home_page_parent);
                        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                    app_stage.setTitle("Afficher Cours Non Valide");

                        app_stage.setScene(home_page_scene);
                        app_stage.show();
                    } catch (IOException ex) {
                        Logger.getLogger(AfficherFormatOganCoursNonValiderController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                   

                }
            });
            return row;
        });

    }

    @FXML
    public void TableUpdate() {
        tableview.getItems().clear();
        //modifier dans l'integration
        tableview.setItems(coursdao.listeCoursformateurOrOrganismeNonValider(apdaou.findApprenantByUserName(LoginController.USERNAME).getId()));
        tableview.refresh();
    }

    @FXML
    private void AjouterCours(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("AjouterCours.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Ajouter Cours");


        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private void ModifierCours(ActionEvent event) throws IOException {
        ModifierCorsController.c = getSelectedCours();
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("ModifierCors.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Modifier Cours");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    public void SupprimerCours(ActionEvent event) {

        Cours supc = getSelectedCours();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Supprimer Cours");
        alert.setHeaderText("Supprimer cours !!!!");
        alert.setContentText("Est ce que vous voulez supprimer ce cours!!");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {

            VideoDao videoDao = new VideoDao();
            int iv = supc.getVedio_id();
            //supprimer Video
            String pa = coursdao.SelectVideo(iv);
            File file = new File("D:/uploads/" + pa);

            file.deleteOnExit();

            videoDao.removeVideoById(iv);

            //supprimer cours
            coursdao.supprimerCoursById(supc.getId());
            TableUpdate();

        }

    }


  

    private Cours getSelectedCours() {
        final Cours selectedCours = tableview.getSelectionModel().getSelectedItem();
        return selectedCours;
    }

    @FXML
    private void btn_acceuil(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("Acceuilformorgan.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Accueil Cours Formateur Organisme");

        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

}
