/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.OrganismeDAO;
import authentification.ENTITIES.organisme;
import static authentification.GUI.LoginController.USERNAME;
import authentification.IDAO.IorganismeDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class TerminerInscriptionOrganismeController implements Initializable {
    
    @FXML
    private Label label1;

    @FXML
    private TextField site;
     
    @FXML
    private TextField certificat;
      
      
    @FXML
    private TextField num;
   
    @FXML
    private ImageView image ;
    @FXML
    private ImageView img ;
    @FXML
    private ImageView img1 ;
    
     @FXML
    private void ValiderAction(ActionEvent event) throws IOException {
    int resultat;
         organisme a = new organisme();
         IorganismeDAO dao = new OrganismeDAO();
         a=dao.findOrganismeByUserName(USERNAME);
         a.setCertificat(certificat.getText());
         a.setNum(Integer.parseInt(num.getText()));
         a.setSite(site.getText());
        
       
         
         resultat=dao.insertOrganismeATerminer(a);
         
         if (resultat==1)
             
         { 
        ((Node) event.getSource()).getScene().getWindow().hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("OrganismeProfil.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
        
         
         }
         else
         { 
         label1.setText("Erreur d'inscription !!!!!Veuillez essayer une autre fois");
         }
         
         
    }
    
    @FXML
    private void   RetourAction(ActionEvent event) throws IOException {
        
     ((Node) event.getSource()).getScene().getWindow().hide(); 
            FXMLLoader loader = new FXMLLoader();
                
            loader.setLocation(getClass().getResource("acceuil.fxml"));
            loader.load();
            Parent p = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.setTitle("Inscription");
            stage.show();   
    }
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
