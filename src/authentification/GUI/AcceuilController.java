/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class AcceuilController implements Initializable {

    @FXML
    private Label label;
    @FXML
    private ImageView img;
    @FXML
    private ImageView img1;
   
    @FXML
    private void inscriptionAction(ActionEvent event) throws IOException
    { 
        ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("choix.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
         stage.setTitle("Choix");
        stage.setScene(new Scene(p));
        stage.show();    
    
    }
    
    @FXML
    private void connexionAction(ActionEvent event) throws IOException
    { 
        ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Login.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
         stage.setTitle("Login");
        stage.setScene(new Scene(p));
        stage.show();    
    
    }  
    
    @FXML
    private void AcceuilAction(ActionEvent event) throws IOException
    { 
        ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("acceuil.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
        stage.setTitle("Acceuil");
        stage.setScene(new Scene(p));
        stage.show();
    }
    
    @FXML
    private void FormateurAction(ActionEvent event) throws IOException
    { 
        
    }
    @FXML
    private void OrganismeAction(ActionEvent event) throws IOException
    { 
        
    }
    
    @FXML
    private void ContactUsAction(ActionEvent event) throws IOException
    { 
         ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/authentification/GUI/aymen/SendMail.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
         stage.setTitle("Contacter Nous");
        stage.setScene(new Scene(p));
        stage.show();
    }
    @FXML
    private void CoursAction(ActionEvent event) throws IOException
    { 
         ((Node)event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/authentification/GUI/anwer/Acceuil.fxml"));
        loader.load();
        Parent p =loader.getRoot();
        Stage stage=new Stage();
         stage.setTitle("Cours");
        stage.setScene(new Scene(p));
        stage.show();
        
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
