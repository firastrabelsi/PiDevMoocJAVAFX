/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.GUI;

import authentification.DAO.FormateurDAO;
import authentification.DAO.OrganismeDAO;
import authentification.DAO.apprenantDAO;
import authentification.ENTITIES.apprenant;
import authentification.IDAO.IFormateurDAO;
import authentification.IDAO.IapprenantDAO;
import authentification.IDAO.IorganismeDAO;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * FXML Controller class
 *
 * @author pc
 */
public class LoginController implements Initializable {

    public static String USERNAME;
    
    static apprenant user;
    @FXML
    private TextField userName;
    @FXML
    private TextField Email;

    @FXML
    private Label label;
    @FXML
    private Label l1;

    @FXML
    private ImageView image;
    @FXML
    private ImageView img1;
    @FXML
    private ImageView img2;
    @FXML
    private ImageView img3;

    @FXML
    private void RetourAction(ActionEvent event) throws IOException {

        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("acceuil.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.setTitle("Inscription");
        stage.show();
    }

    @FXML
    private void fbAction(ActionEvent event) throws IOException {

        String accessToken = "";

        System.setProperty("webdriver.chrome.driver", "D:\\piMoocJava\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        driver.get("https://graph.facebook.com/oauth/authorize?type=user_agent&client_id=1699178627004465&redirect_uri=http://mooc.com&scope=user_about_me,"
                + "user_actions.books,user_actions.fitness,user_actions.music,user_actions.news,user_actions.video,user_activities,user_birthday,user_education_history,"
                + "user_events,user_friends,user_games_activity,user_groups,user_hometown,user_interests,user_likes,user_location,user_photos,user_relationship_details,"
                + "user_relationships,user_religion_politics,user_status,user_tagged_places,user_videos,user_website,user_work_history,ads_management,ads_read,email,"
                + "manage_notifications,manage_pages,publish_actions,read_friendlists,read_insights,read_mailbox,read_page_mailboxes,read_stream,rsvp_event");

        while (true) {
            if (!driver.getCurrentUrl().contains("facebook.com")) {

                String url = driver.getCurrentUrl();
                int state = 0;
                for (int i = 0; i < url.length(); i++) {

                    switch (state) {
                        case (0): {
                            if (url.charAt(i) == '=') {
                                state = 1;
                            }
                        }
                        break;
                        case (1): {

                            if (url.charAt(i) == '&') {
                                state = 2;
                            } else {
                                accessToken += url.charAt(i);
                            }

                        }
                        break;
                        case (2): {
                        }
                        break;
                    }

                }

                driver.quit();
                System.out.println(accessToken);

                FacebookClient fbClient = new DefaultFacebookClient(accessToken);

                User me = fbClient.fetchObject("me", User.class);

                // l1.setText("Current User : "+me.getName());
                USERNAME = me.getName();
                System.out.println(me.getEmail());
                System.out.println(USERNAME);

                IapprenantDAO dao = new apprenantDAO();
                apprenant appr = dao.findApprenantByUserName(USERNAME);
                System.out.println(appr);
                if (appr.getEmail() == null) {
                    ((Node) event.getSource()).getScene().getWindow().hide();
                    FXMLLoader loader = new FXMLLoader();

                    loader.setLocation(getClass().getResource("choix.fxml"));
                    loader.load();
                    Parent p = loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                } else {
                    ((Node) event.getSource()).getScene().getWindow().hide();
                    FXMLLoader loader = new FXMLLoader();

                    loader.setLocation(getClass().getResource("ApprenantProfil.fxml"));
                    loader.load();
                    Parent p = loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                }

            }
        }
    }

    @FXML
    private void btnloginaction(ActionEvent event) throws IOException {
        String role;
        String ROLE;
        USERNAME = userName.getText();
        int ID;
        IapprenantDAO dao = new apprenantDAO();
        role = dao.veriflogin(userName.getText(), Email.getText());
        System.out.println(role);
        System.out.println(role);
        ROLE = role.substring(15, 25);
        System.out.println(role);
        if (role != null) {

            /*  System.out.println(role);
             ROLE = role.substring(15, 25);
             System.out.println(ROLE);*/
            if (ROLE.equals("ROLE_ADMIN")) {
                ((Node) event.getSource()).getScene().getWindow().hide();
                FXMLLoader loader = new FXMLLoader();

                loader.setLocation(getClass().getResource("acceuilDash.fxml"));
                loader.load();
                Parent p = loader.getRoot();
                Stage stage = new Stage();
                stage.setScene(new Scene(p));
                stage.show();

            } else if ("ROLE_APPRE".equals(ROLE)) {

                ((Node) event.getSource()).getScene().getWindow().hide();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("ApprenantProfil.fxml"));
                loader.load();
                Parent p = loader.getRoot();
                Stage stage = new Stage();
                stage.setScene(new Scene(p));
                stage.show();
            } else if ("ROLE_ORGAN".equals(ROLE)) {
                System.out.println("im hereee");
                IorganismeDAO DAO = new OrganismeDAO();
                String etat;
                etat = DAO.verifEtat(userName.getText(), Email.getText());
                System.out.println(etat);
                if (etat.equals("valide a terminer")) {
                    ((Node) event.getSource()).getScene().getWindow().hide();
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("terminerInscriptionOrganisme.fxml"));
                    loader.load();
                    Parent p = loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                    USERNAME = userName.getText();
                } else if (etat.equals("valide")) {
                    USERNAME = userName.getText();
                    ((Node) event.getSource()).getScene().getWindow().hide();
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("OrganismeProfil.fxml"));

                    loader.load();
                    Parent p = loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                } else {
                    label.setText("Votre inscription est en attente de validation");

                }
            } else if (ROLE.equals("ROLE_FORMA")) {

                System.out.println(new FormateurDAO().veriflogin1(userName.getText(), Email.getText()) + "++++++++++");
                if (!new FormateurDAO().veriflogin1(userName.getText(), Email.getText()).equals("")) {
                    ((Node) event.getSource()).getScene().getWindow().hide();
                    FXMLLoader loader = new FXMLLoader();

                    loader.setLocation(getClass().getResource("/authentification/GUI/aymen/formateur.fxml"));
                    loader.load();
                    Parent p = loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                    System.out.println(USERNAME);
                }

            } else if ("ROLE_COMIT".equals(ROLE)) {
                USERNAME = userName.getText();
                ((Node) event.getSource()).getScene().getWindow().hide();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/authentification/GUI/aymen/comite.fxml"));
                loader.load();
                Parent p = loader.getRoot();
                Stage stage = new Stage();
                stage.setScene(new Scene(p));
                stage.show();

            }

        } else {
            
            label.setText("Verifier vos données");
//            Alert alert = new Alert(AlertType.INFORMATION);
//            alert.setTitle("Message d'erreur");
//            alert.setHeaderText("Erreur");
//            alert.setContentText("Votre login ou mot de passe est incorrect !");
//
//            alert.showAndWait();
        }
    }

    
    @FXML
    private void twitterAction(ActionEvent event) throws IOException {

        ((Node) event.getSource()).getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("acceuil.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(p));
        stage.setTitle("Inscription");
        stage.show();
    }
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
