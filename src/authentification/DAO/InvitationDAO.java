/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;

import authentification.ENTITIES.invitation;
import authentification.IDAO.IinvitationDAO;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pc
 */
public class InvitationDAO implements IinvitationDAO {
     Connection cnx;

    public InvitationDAO() {
        cnx = MyConnection.getInstance();
    }
    
      //ajouter invitation

    @Override
    public int insertInvitation(invitation invit) {
        Calendar now = Calendar.getInstance();
        String query = "INSERT INTO utilisateur(source_id,destination_id,dateinvitation,etat )VALUES (?,?,?,?)";

        try {

            PreparedStatement Pst = cnx.prepareStatement(query);

            Pst.setInt(1, invit.getSource());
            Pst.setInt(2, invit.getDestination());
            Pst.setString(3, now.toString());
            Pst.setString(4, "Attente");
            
            Pst.executeUpdate();
            System.out.println("invitation  ajouté!");
            return 1;
            
        } catch (SQLException ex) {
            System.out.println("invitation non ajouté!" + "******" + ex);
            return 0;
        }

    }
    
    /**
     *
     * @param id
     * @return
     */
    @Override
    public List<invitation> AfficherInvitation(int id) {
     List<invitation> invit = new ArrayList<>();
        
     String requete = "select * from invitation where source_id= ? and etat='Attente'";
     try {
     PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, Integer.toString(id));
            ResultSet resultat = ps.executeQuery();
    
     while (resultat.next()) {  
     invitation f = new invitation();
                f.setId(resultat.getInt(1));
                f.setSource(resultat.getInt(2));
                f.setDestination(resultat.getInt(3));
                f.setDateInvitation(resultat.getString(4));
                f.setEtat(resultat.getString(5));
     invit.add(f);
                
     }
     } catch (Exception e) {
     }
     return invit;
     }
    
    
    
    @Override
    public invitation findinvitationbyid(int id) {
        invitation appr = new invitation();
        String requete = "select * from invitation where id="+id;
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                appr.setId(resultat.getInt(1));
                appr.setSource(resultat.getInt(2));
                appr.setDestination(resultat.getInt(3));
                appr.setDateInvitation(resultat.getString(4));
                appr.setEtat(resultat.getString(5));

            }
            System.out.println(appr);
            return appr;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche d'organisme " + ex.getMessage());
            return null;
        }
        
        
        
        
        
        
        
    }

    
     @Override
    public void confirmer(int id)
    {             String requete = "UPDATE invitation SET etat=? where id= ?";

         try {
            PreparedStatement pst = cnx.prepareStatement(requete);
             pst.setString(1, "Confirmer");
             pst.setString(2, Integer.toString(id));
             pst.executeUpdate();
         } catch (SQLException ex) {
             Logger.getLogger(InvitationDAO.class.getName()).log(Level.SEVERE, null, ex);
         }
        }
    
     @Override
    public void Rejeter(int id)
    {
         try {
             String requete = "DELETE FROM invitation WHERE id=?";
             PreparedStatement ps = cnx.prepareStatement(requete);
             ps.setString(1, Integer.toString(id));
             ps.executeUpdate();
         } catch (SQLException ex) {
             Logger.getLogger(InvitationDAO.class.getName()).log(Level.SEVERE, null, ex);
         }
        }
    
    
    
    
    
}
