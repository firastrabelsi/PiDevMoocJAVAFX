/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;
import authentification.ENTITIES.apprenant;
import authentification.IDAO.IorganismeDAO;
import authentification.ENTITIES.organisme;
import authentification.IDAO.IapprenantDAO;
import authentification.UTIL.MyConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;








public class OrganismeDAO implements IorganismeDAO {

    Connection cnx;

    public OrganismeDAO() {
        cnx = MyConnection.getInstance();
    }

    //ajouter Organisme

    @Override
    public void insertOrganisme(organisme org) {

        String query = "INSERT INTO utilisateur(username,nom,prenom,email,password,enabled,username_canonical, email_canonical,salt, locked, expired, roles, credentials_expired,etat,adresse,nomDeLaSociete,url ,certificat,site,num,note,specialite,etat_c )VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try {

            PreparedStatement Pst = cnx.prepareStatement(query);

            Pst.setString(1, org.getUserName());
            Pst.setString(2, "");
            Pst.setString(3, "");
            Pst.setString(4, org.getEmail());
            Pst.setString(5, org.getPassword());
            Pst.setInt(6, 1);
            Pst.setString(7,org.getUserName() );
            Pst.setString(8, org.getEmail());
            Pst.setString(9, "");
            Pst.setInt(10, 0);
            Pst.setInt(11, 0);
            Pst.setString(12, "a:1:{i:0;s:14:\"ROLE_ORGANISME\";}"); 

            Pst.setInt(13, 0);
            Pst.setString(14, "non valide");
            Pst.setString(15, org.getAdresse()); 
            Pst.setString(16, org.getNomDeLaSociete()); 
            Pst.setString(17, ""); 
            Pst.setInt(18, 0);
            Pst.setString(19, "");
            Pst.setInt(20, 0);
            Pst.setInt(21, 0);
            
            Pst.setString(22, "");
            Pst.setInt(23,0);
            Pst.executeUpdate();
            System.out.println("Organisme  ajouté!");

        } catch (SQLException ex) {
            System.out.println("Organisme non ajouté!" + "******" + ex);

        }

    }
    
    @Override
    public String verifEtat(String username, String email) {
        String etat="";
        String h="tarartarat";
        organisme appr = new organisme();
        String requete = "select etat from utilisateur where username=? And email=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, username);
            ps.setString(2, email);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                etat=resultat.getString(1);

            }
            if (etat!="null")
            {return etat;}
            else
            { return h;}
        } catch (SQLException ex) {
            System.out.println("erreur lors de la récuperation à jour " + ex.getMessage());
            return null;
        }
    }
    
    
     ////////////////////////ADMIN Organisme////////////////////////////
     //aficher la liste de tt des organisme validés
    
    @Override
    public ArrayList<organisme> displayAllOrganisme() {
        ArrayList<organisme> listeOrganisme = new ArrayList<>();

        String requete = "select * from utilisateur where etat='valide' ";
        try {
            Statement statement = cnx.createStatement();
            ResultSet resultat = statement.executeQuery(requete);

            while (resultat.next()) {

                organisme appr = new organisme();
                appr.setId(resultat.getInt(1));
                appr.setUserName(resultat.getString(2));
                appr.setAdresse(resultat.getString(3));
                appr.setNomDeLaSociete(resultat.getString(4));
                appr.setEmail(resultat.getString(5));
                appr.setPassword(resultat.getString(6));

                listeOrganisme.add(appr);

            }
            return listeOrganisme;

        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement de la liste des Apprenants " + ex.getMessage());
            return null;
        }
    }
    

    @Override
    public organisme findOrganismeByUserName(String UserName) {
        organisme appr = new organisme();
        String requete = "select * from utilisateur where UserName=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, UserName);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                appr.setId(resultat.getInt(1));
                appr.setUserName(resultat.getString(2));
                appr.setNomDeLaSociete(resultat.getString(22));
                appr.setAdresse(resultat.getString(21));
                appr.setEmail(resultat.getString(4));
                appr.setPassword(resultat.getString(8));
                appr.setCertificat(resultat.getString(26));
                appr.setSite(resultat.getString(25));
                appr.setNum(resultat.getInt(24));

            }
            return appr;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche d'organisme " + ex.getMessage());
            return null;
        }
        
        
        
        
        
    }

          
    
    
    
    
    ///////////////////////////////////////////Organisme non valide////////////////////
    
    @Override
    public ArrayList<organisme> displayAllOrganismeNonValide() {
        ArrayList<organisme> listeOrganisme = new ArrayList<>();

        String requete = "select * from utilisateur where etat='non valide' ";
        try {
            Statement statement = cnx.createStatement();
            ResultSet resultat = statement.executeQuery(requete);

            while (resultat.next()) {

                organisme appr = new organisme();
                appr.setId(resultat.getInt(1));
                appr.setUserName(resultat.getString(2));
                appr.setAdresse(resultat.getString(21));
                appr.setNomDeLaSociete(resultat.getString(22));
                appr.setEmail(resultat.getString(4));
                appr.setPassword(resultat.getString(6));

                listeOrganisme.add(appr);

            }
            return listeOrganisme;

        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement de la liste des Organisme non valides " + ex.getMessage());
            return null;
        }
    }
    
    //modifier 
    @Override
    public void updateOrganisme(organisme appr, int id) {

        String query = "UPDATE utilisateur SET etat='valide a terminer'  WHERE id=" + id;

        try {
            PreparedStatement Pst = cnx.prepareStatement(query);

           
            Pst.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(" une Erreur s'est produite lors  de la modification!!!");

        }
    }

    //suprimer Organisme
    @Override
    public void deleteOrganisme(int id) {
        String requete = "DELETE FROM utilisateur WHERE id=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setInt(1, id);
            ps.executeUpdate();
            System.out.println("Organisme supprimée");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }

    }

  /////////////////////////////////// Dupliction Organisme final/////////////////////
    
     @Override
    public organisme findOrganismeByUserNameTotal(String UserName) {
        organisme appr = new organisme();
        String requete = "select * from utilisateur where UserName=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, UserName);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                appr.setId(resultat.getInt(1));
                appr.setUserName(resultat.getString(2));
                appr.setNomDeLaSociete(resultat.getString(22));
                appr.setAdresse(resultat.getString(21));
                appr.setEmail(resultat.getString(4));
                appr.setPassword(resultat.getString(8));
                appr.setCertificat(resultat.getString(26));
                appr.setSite(resultat.getString(25));
                appr.setNum(resultat.getInt(24));

            }
            return appr;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche d'organisme " + ex.getMessage());
            return null;
        }
    }
    
    
   @Override
    public int findOrganismetId(String Username) {
    int Id = 0;
        organisme appr = new organisme();
        String requete = "select id from utilisateur where username=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, Username);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                Id=resultat.getInt(1);
                System.out.println(Id);            }
            return Id ;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche de l'id " + ex.getMessage());
            return 0;
        }
    
    }
    
    @Override
      public void updateOrganismefinal(organisme appr, int id) {

        String query = "UPDATE utilisateur SET userName=? ,adresse=? ,certificat=? ,site=?,num=?,email=? ,password=?,username_canonical=?,email_canonical=? ,nomDeLaSociete=?  WHERE id="+id;

        try {
            PreparedStatement Pst = cnx.prepareStatement(query);

            Pst.setString(1, appr.getUserName());
            Pst.setString(2, appr.getAdresse());
            Pst.setString(3, appr.getCertificat());
            Pst.setString(4, appr.getSite());
            Pst.setInt(5, appr.getNum());
            Pst.setString(6, appr.getEmail());
            Pst.setString(7, appr.getPassword());
            Pst.setString(8, appr.getUserName());
            Pst.setString(9, appr.getEmail());
            Pst.setString(10, appr.getNomDeLaSociete());
            
            

            Pst.executeUpdate();
            System.out.println("success");

        } catch (SQLException ex) {
            System.out.println(" une Erreur s'est produite lors  de la modification!!!" + ex);

        }
    }  
   
    //ajouter Organisme etat valide reste a terminer

    @Override
    public int insertOrganismeATerminer(organisme org ) {

        String query = "UPDATE utilisateur SET num=?,site=? ,certificat=?, etat=? where id="+org.getId() ;

        try {

            PreparedStatement Pst = cnx.prepareStatement(query);
            
            Pst.setInt(1, org.getNum());
            Pst.setString(2, org.getSite());
            Pst.setString(3, org.getCertificat());
            Pst.setString(4, "valide");
            Pst.executeUpdate();

            System.out.println("Organisme  ajouté!");
            return 1;
        } catch (SQLException ex) {
            System.out.println("Organisme non ajouté!" + "******" + ex);
            return 0;    
        }

    }
     
}
