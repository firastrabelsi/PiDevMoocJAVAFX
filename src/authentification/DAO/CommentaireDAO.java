/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;

import authentification.DAO.apprenantDAO;
import java.text.DateFormat;
import java.util.Date;
import authentification.ENTITIES.Commentaire;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Bali Majdi
 */
public class CommentaireDAO {

    private Connection cnn;
    private Statement stm;
    private PreparedStatement pstm;
    Date d = new Date();
    static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public CommentaireDAO() {
        cnn = new MyConnection().getInstance();
    }

    public void getAll() {

        try {

            stm = cnn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM commentaire");
            while (rs.next()) {
                System.out.println(rs.getInt("id"));

                System.out.println(rs.getString("contenu"));

                d = rs.getDate("date");
                String dat = dateFormat.format(d);
                System.out.println(dat + "2");

            }
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void Commenter(Commentaire cmt) {

        String requete = "insert into commentaire (contenu,date,personne_id,cours_id) values (?,?,?,?)";
        try {
            CommentaireDAO c = new CommentaireDAO();
            java.util.Date date_util = new java.util.Date();

            java.sql.Date date_sql = new java.sql.Date(date_util.getTime());
            PreparedStatement ps = cnn.prepareStatement(requete);
            System.out.println("l'id est égale *********** " + getLastID());
////            ps.setInt(1, c.getLastID() + 1);
            ps.setDate(2, date_sql);
            ps.setString(1, cmt.getContenu());
  ps.setInt(3, cmt.getUt().getId());
            ps.setInt(4, cmt.getCours().getId());
           
            ps.executeUpdate();
            System.out.println("Ajout effectué avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de l'insertion " + ex.getMessage());
        }
    }

    public void SupprimerCommentaire(int id) {
        String requete = "delete from Commentaire where id=?";
        try {
            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setInt(1, id);
            ps.executeUpdate();
            System.out.println("Commentaire supprimé");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }
    }

    public void updateCommentaire(Commentaire cmt) {
        String requete = "update Commentaire set contenu=? where id=?";
        try {
            java.util.Date date_util = new java.util.Date();
            String dat = dateFormat.format(date_util);

            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setString(1, cmt.getContenu() + "\n" + " Mise à jour le " + dat);
            ps.setInt(2, cmt.getId());

            ps.executeUpdate();
            System.out.println("Mise à jour effectuée avec succès");
            

        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise à jour " + ex.getMessage());
        }
    }

    public int getLastID() {
        int i = 0;
        try {

            stm = cnn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM commentaire");
            while (rs.next()) {
                System.out.println(rs.getInt("id"));

                System.out.println(rs.getString("contenu"));

                d = rs.getDate("date");
                String dat = dateFormat.format(d);
                System.out.println(dat + "2");
                i = rs.getInt("id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }

    public List<Commentaire> getList(int id) {
        List<Commentaire> LC = new ArrayList<Commentaire>();

        String requete = "SELECT * FROM commentaire where cours_id=?";
        try {
            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                Commentaire c = new Commentaire();
                c.setId(rs.getInt("id"));

                c.setContenu(rs.getString("contenu"));

                c.setDate(rs.getDate("date"));

                apprenantDAO ut = new apprenantDAO();
                c.setUt(ut.findApprenantById(rs.getInt("personne_id")));

                LC.add(c);

                System.out.println(c.toString() + "................");

            }
        } catch (SQLException ex) {
            Logger.getLogger(CommentaireDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return LC;
    }

}
