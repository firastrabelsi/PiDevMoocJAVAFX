package authentification.DAO;

import authentification.ENTITIES.Cours;
import authentification.GUI.LoginController;
import authentification.IDAO.ICoursDAO;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CoursDAO implements ICoursDAO {

    private Connection cnn;
    private PreparedStatement pstm;

    public CoursDAO() {
        cnn = MyConnection.getInstance();
    }

    public void ajouterCous(Cours c) {
        apprenantDAO apdao = new apprenantDAO();
        try {
            // a modfier dans integration
            pstm = cnn.prepareStatement("INSERT into cours (categorie_id,vedio_id,formateur_id,titre,description,difficulte,etat,dureedecours) values (?,(select id from video ORDER BY id DESC LIMIT 1),?,?,?,?,?,?)");
//            if (c.getFormateur() != null) {
//                pstm.setInt(2, c.getFormateur().getId());
//                System.out.println("==============");
//
//            } else if (c.getOrgani() != null) {
//                pstm.setInt(2, c.getOrgani().getId());
//
//            } else {
//                pstm.setObject(2, null);
//            }
            pstm.setInt(2, apdao.findApprenantByUserName(LoginController.USERNAME).getId());
            pstm.setInt(1, c.getCategorie().getId());

            pstm.setString(3, c.getTitre());
            pstm.setString(4, c.getDescription());
            pstm.setString(5, c.getDifficulte());
            pstm.setString(6, "Nonvalider");
            pstm.setInt(7, c.getDureedecours());
            pstm.executeUpdate();
            System.out.println("Cours ajouté");

        } catch (SQLException ex) {

            System.out.println("Cours non ajouté");

            Logger.getLogger(CategorieDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void modifierCours(Cours c) {
        String requete = "update cours set titre=?,description=?,difficulte=?,categorie_id=?,dureedecours=?,etat=? where id=?";
        try {
            pstm = cnn.prepareStatement(requete);
            pstm.setString(1, c.getTitre());
            pstm.setString(2, c.getDescription());
            pstm.setInt(5, c.getDureedecours());
            pstm.setString(3, c.getDifficulte());
            pstm.setString(6, "Nonvalider");
            pstm.setInt(4, c.getCategorie().getId());
            pstm.setInt(7, c.getId());
            pstm.executeUpdate();
            System.out.println("Mise à jour effectuée avec succès" + c.getId());
        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise à jour " + ex.getMessage());
        }
    }

    public ObservableList<Cours> findAll() {
        ObservableList<Cours> listecours = FXCollections.observableArrayList();
        String requete = "select * from cours";
        try {
            Statement statement = cnn.createStatement();
            ResultSet resultat = statement.executeQuery(requete);
            CategorieDAO categorieDAO = new CategorieDAO();

            while (resultat.next()) {
                Cours c = new Cours();
                c.setId(resultat.getInt(1));
                c.setVedio_id(resultat.getInt(2));
                c.setTitre(resultat.getString(5));
                c.setDescription(resultat.getString(6));
                c.setDureedecours(resultat.getInt(7));
                c.setDifficulte(resultat.getString(8));
                c.setEtat(resultat.getString(9));
                c.setCategorie(categorieDAO.findCategoriesById(resultat.getInt(3)));

                listecours.add(c);
            }
            return listecours;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des depots " + ex.getMessage());
            return null;
        }
    }

    public void supprimerCoursById(int id) {
        String requete = "delete from cours where id=?";
        try {
            pstm = cnn.prepareStatement(requete);
            pstm.setInt(1, id);
            pstm.executeUpdate();
            System.out.println("Cours supprimé");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }
    }

    public void modifieretat(int id) {
        String requete = "update cours set etat=? where id=?";
        try {
            pstm = cnn.prepareStatement(requete);
            pstm.setString(1, "valider");
            pstm.setInt(2, id);
            pstm.executeUpdate();
            System.out.println("Mise à jour effectuée avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise à jour de etat " + ex.getMessage());
        }
    }

    public ObservableList<Cours> listcoursnonvalider() {
        ObservableList<Cours> listecours = FXCollections.observableArrayList();
        String requete = "select * from cours where etat = ?  ";
        try {
            pstm = cnn.prepareStatement(requete);
            pstm.setString(1, "Nonvalider");

            ResultSet resultat = pstm.executeQuery();
            CategorieDAO categorieDAO = new CategorieDAO();

            while (resultat.next()) {
                Cours c = new Cours();
                c.setId(resultat.getInt(1));
                c.setVedio_id(resultat.getInt(2));
                c.setTitre(resultat.getString(5));
                c.setDescription(resultat.getString(6));
                c.setDureedecours(resultat.getInt(7));
                c.setDifficulte(resultat.getString(8));
                c.setEtat(resultat.getString(9));
                c.setCategorie(categorieDAO.findCategoriesById(resultat.getInt(3)));

                listecours.add(c);
            }
            return listecours;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des cours non valider " + ex.getMessage());
            return null;
        }
    }

    public ObservableList<Cours> listcoursvalider() {
        ObservableList<Cours> listecours = FXCollections.observableArrayList();
        String requete = "select * from cours where etat = ?  ";
        try {
            pstm = cnn.prepareStatement(requete);
            pstm.setString(1, "valider");

            ResultSet resultat = pstm.executeQuery();
            CategorieDAO categorieDAO = new CategorieDAO();

            while (resultat.next()) {
                Cours c = new Cours();
                c.setId(resultat.getInt(1));
                c.setVedio_id(resultat.getInt(2));
                c.setTitre(resultat.getString(5));
                c.setDescription(resultat.getString(6));
                c.setDureedecours(resultat.getInt(7));
                c.setDifficulte(resultat.getString(8));
                c.setEtat(resultat.getString(9));
                c.setCategorie(categorieDAO.findCategoriesById(resultat.getInt(3)));

                listecours.add(c);
            }
            return listecours;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des cours  valider " + ex.getMessage());
            return null;
        }
    }

    public String SelectVideo(Integer v_id) {

        String p = "";
        String requete = "select path from video where id=?";
        try {
            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setInt(1, v_id);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {

                p = (resultat.getString(1));

            }
            return p;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return p;
        }
    }

    public ObservableList<Cours> findCoursBytitre(String titre, int id) {
        CategorieDAO categorieDAO = new CategorieDAO();

        ObservableList<Cours> con = FXCollections.observableArrayList();
        try {
            String sql = "select * from cours where titre like '%" + titre + "%'" + "and organisme_id=" + id;
            ResultSet resultat = cnn.createStatement().executeQuery(sql);
            while (resultat.next()) {

                Cours c = new Cours();
                c.setId(resultat.getInt(1));
                c.setVedio_id(resultat.getInt(2));
                c.setTitre(resultat.getString(5));
                c.setDescription(resultat.getString(6));
                c.setDureedecours(resultat.getInt(7));
                c.setDifficulte(resultat.getString(8));
                c.setCategorie(categorieDAO.findCategoriesById(resultat.getInt(3)));

                con.add(c);
            }
        } catch (Exception ex) {
            Logger.getLogger(CategorieDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    public ObservableList<Cours> listcoursFormateurOrOrganisme(int id) {
        ObservableList<Cours> listecours = FXCollections.observableArrayList();
        String requete = "select * from cours where organisme_id = ?  ";
        try {
            pstm = cnn.prepareStatement(requete);
            pstm.setInt(1, id);

            ResultSet resultat = pstm.executeQuery();
            CategorieDAO categorieDAO = new CategorieDAO();

            while (resultat.next()) {
                Cours c = new Cours();
                c.setId(resultat.getInt(1));
                c.setTitre(resultat.getString(5));
                c.setDescription(resultat.getString(6));
                c.setDureedecours(resultat.getInt(7));
                c.setDifficulte(resultat.getString(8));
                c.setEtat(resultat.getString(9));
                c.setCategorie(categorieDAO.findCategoriesById(resultat.getInt(3)));
                c.setVedio_id(resultat.getInt(2));
                listecours.add(c);
            }
            return listecours;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des concté " + ex.getMessage());
            return null;
        }
    }

    public Cours rechercherCoursbyid(int id) {
        String requete = "select * from cours where id = ?  ";
        try {
            pstm = cnn.prepareStatement(requete);
            pstm.setInt(1, id);

            ResultSet resultat = pstm.executeQuery();
            CategorieDAO categorieDAO = new CategorieDAO();
            Cours c = new Cours();

            while (resultat.next()) {
                c.setId(resultat.getInt(1));
                c.setVedio_id(resultat.getInt(2));
                c.setTitre(resultat.getString(5));
                c.setDescription(resultat.getString(6));
                c.setDureedecours(resultat.getInt(7));
                c.setDifficulte(resultat.getString(8));
                c.setEtat(resultat.getString(9));
                c.setCategorie(categorieDAO.findCategoriesById(resultat.getInt(3)));

            }
            return c;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des concté " + ex.getMessage());
            return null;
        }
    }

    public ObservableList<Cours> findCoursBytitre2(String titre) {
        CategorieDAO categorieDAO = new CategorieDAO();

        ObservableList<Cours> con = FXCollections.observableArrayList();
        try {
            String sql = "select * from cours where titre like '%" + titre + "%'";
            ResultSet resultat = cnn.createStatement().executeQuery(sql);
            while (resultat.next()) {

                Cours c = new Cours();
                c.setId(resultat.getInt(1));
                c.setVedio_id(resultat.getInt(2));
                c.setTitre(resultat.getString(5));
                c.setDescription(resultat.getString(6));
                c.setDureedecours(resultat.getInt(7));
                c.setDifficulte(resultat.getString(8));
                c.setCategorie(categorieDAO.findCategoriesById(resultat.getInt(3)));

                con.add(c);
            }
        } catch (Exception ex) {
            Logger.getLogger(CategorieDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    public ObservableList<Cours> listeCoursformateurOrOrganismeNonValider(int id) {
        CategorieDAO categorieDAO = new CategorieDAO();
        List<Cours> l = new ArrayList<Cours>();
        ObservableList<Cours> con = FXCollections.observableArrayList();
        String sql = "select * from cours where formateur_id = ? And etat = ?";

        try {
            PreparedStatement ps = cnn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, "Nonvalider");
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {

                Cours c = new Cours();
                c.setId(resultat.getInt(1));
                c.setVedio_id(resultat.getInt(2));
                c.setTitre(resultat.getString(5));
                c.setDescription(resultat.getString(6));
                c.setDureedecours(resultat.getInt(7));
                c.setDifficulte(resultat.getString(8));
                c.setCategorie(categorieDAO.findCategoriesById(resultat.getInt(3)));
                l.add(c);
                con.add(c);
            }
        } catch (Exception ex) {
            Logger.getLogger(CategorieDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    public ObservableList<Cours> listeCoursformateurOrOrganismeValider(int id) {
        CategorieDAO categorieDAO = new CategorieDAO();
        List<Cours> l = new ArrayList<Cours>();
        ObservableList<Cours> con = FXCollections.observableArrayList();
        String sql = "select * from cours where formateur_id = ? And etat = ?";

        try {
            PreparedStatement ps = cnn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, "valider");
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {

                Cours c = new Cours();
                c.setId(resultat.getInt(1));
                c.setVedio_id(resultat.getInt(2));
                c.setTitre(resultat.getString(5));
                c.setDescription(resultat.getString(6));
                c.setDureedecours(resultat.getInt(7));
                c.setDifficulte(resultat.getString(8));
                c.setCategorie(categorieDAO.findCategoriesById(resultat.getInt(3)));
                l.add(c);
                con.add(c);
            }
        } catch (Exception ex) {
            Logger.getLogger(CategorieDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    @Override
    public Cours findObjectById(int id) { 
        Cours c = new Cours();
        String requete = "select * from cours where id = ?";
        try {
            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                c.setId(resultat.getInt(1));
                c.setTitre(resultat.getString(2));
                c.setDescription(resultat.getString(3));
                c.setEtat(resultat.getString(4));
                c.setDifficulte(resultat.getString(5));
            }
            return c;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du cours " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Cours> DisplayAllObject() {
        List<Cours> listtests = new ArrayList<Cours>();
        String requete = "select * from cours";
        try {
            PreparedStatement ps = cnn.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery(requete);
            while (resultat.next()) {
                Cours f = new Cours();
                f.setId(resultat.getInt(1));
                f.setTitre(resultat.getString(2));
                f.setDescription(resultat.getString(3));
                f.setEtat(resultat.getString(4));
                
                f.setDifficulte(resultat.getString(7));

                listtests.add(f);
            }
            return listtests;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des cours " + ex.getMessage());
            return null;
        }
    }

    public Cours rechercherCoursbyid2(int id) {
        String requete = "select note,nbreV from cours where id = ?  ";
        try {
            pstm = cnn.prepareStatement(requete);
            pstm.setInt(1, id);

            ResultSet resultat = pstm.executeQuery();
            
            Cours c = new Cours();

            while (resultat.next()) {
                c.setNote(resultat.getInt(1));
                c.setNbVote(resultat.getInt(2));
                System.out.println("test test ggggggggg"+c.getNbVote()+"zzzzzz"+c.getNote());

            }
            return c;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des concté " + ex.getMessage());
            return null;
        }
    }
        public void updateCours(Cours cr) {
        String requete = "update cours set note=?, nbreV=? where id=?";
        try {
            
            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setFloat(1, cr.getNote());
            ps.setInt(2, cr.getNbVote());
            ps.setInt(3, cr.getId());

            ps.executeUpdate();
            System.out.println("Mise à jour effectuée avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise à jour " + ex.getMessage());
        }
    }

}
