/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;

import authentification.ENTITIES.Formation;
import authentification.IDAO.IFormationDao;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class FormationDao implements IFormationDao {

    private Connection cnx;

    public FormationDao() {
        cnx = MyConnection.getInstance();
    }

    @Override
    public void insertObject(Formation val) {
        String requete = "insert into formation (nomForm,duree,level) values (?,?,?)";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, val.getNomFormation());
            ps.setInt(2, val.getDuree());
            ps.setString(3, val.getLevel());

            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(FormationDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void updateObject(Formation val) {
        String requete = "update formation set nomForm = ?, duree = ?, level = ? where IdForm = " + val.getIdForm();
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, val.getNomFormation());
            ps.setInt(2, val.getDuree());
            ps.setString(3, val.getLevel());
            ps.executeUpdate();
            System.out.println("Mise à jour effectuée avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise à jour " + ex.getMessage());
        }

    }

    @Override
    public void deleteObject(Formation val) {
        String requete = "delete from formation where IdForm = ?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setInt(1, val.getIdForm());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }

    }

    @Override
    public Formation findObjectById(int id) {
        Formation f = new Formation();
        String requete = "select * from formation where IdForm = ?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                f.setIdForm(resultat.getInt(4));
                f.setNomFormation(resultat.getString(1));
                f.setDuree(resultat.getInt(2));
                f.setLevel(resultat.getString(3));
            }
            return f;
        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du formation " + ex.getMessage());
            return null;
        }

    }

    @Override
    public List<Formation> DisplayAllObject() {
        List<Formation> listtests = new ArrayList<Formation>();
        String requete = "select * from formation";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery(requete);
            while (resultat.next()) {
                Formation f = new Formation();
                f.setIdForm(resultat.getInt(4));
                f.setNomFormation(resultat.getString(1));
                f.setDuree(resultat.getInt(2));
                f.setLevel(resultat.getString(3));
                listtests.add(f);
            }
            return listtests;

        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des formation " + ex.getMessage());
            return null;
        }

    }

    @Override
    public Formation findByNom(String nomformation) {
        Formation f = new Formation();
        String requete = "select * from formation where nomForm= ?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, nomformation);
            ResultSet resultat = ps.executeQuery();

            while (resultat.next()) {
                f.setIdForm(resultat.getInt(4));
                f.setNomFormation(resultat.getString(1));
                f.setDuree(resultat.getInt(2));
                f.setLevel(resultat.getString(3));
            }
            return f;
        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du cours " + ex.getMessage());
            return null;
        }

    }

}
