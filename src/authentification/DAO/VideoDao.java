/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;

import authentification.ENTITIES.VideoM;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class VideoDao {

    private Connection cnn;

    public VideoDao() {
        cnn = MyConnection.getInstance();

    }

    public void addVideo(VideoM p) {
        String req = "insert into video (updated_at,name,path) values (?,?,?)";
        try {
            PreparedStatement ps = cnn.prepareStatement(req);
            ps.setTimestamp(1, java.sql.Timestamp.from(java.time.Instant.now()));
            ps.setTimestamp(1, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
            ps.setString(2, p.getName());
            ps.setString(3, p.getPath());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(VideoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void removeVideoById(int id_v) {
        String requete = "delete from video where id=?";
        try {
            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setInt(1, id_v);
            ps.executeUpdate();
            System.out.println("Video supprimé");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }

    }

    public void updaVideoById(VideoM p) {
        String requete = "update video set updated_at=?,name=?,path=? where id=?";
        try {
            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setTimestamp(1, java.sql.Timestamp.from(java.time.Instant.now()));
            ps.setTimestamp(1, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
            ps.setString(2, p.getName());
            ps.setString(3, p.getPath());
            ps.setInt(4, p.getId());
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise ajours de vedio " + ex.getMessage());
        }

    }

}
