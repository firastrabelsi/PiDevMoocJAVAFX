/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package authentification.DAO;

import authentification.ENTITIES.Categories;
import authentification.IDAO.ICategorieDAO;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class CategorieDAO implements ICategorieDAO{
    private Connection cnn;
    private PreparedStatement pstm;

    public CategorieDAO() {
        cnn = MyConnection.getInstance(); 

    }

    public void ajouterCategorie(Categories c){
        try {
            pstm = cnn.prepareStatement("INSERT into categorie (image_id,cate) values ((select id from image ORDER BY id DESC LIMIT 1),?)");
            pstm.setString(1, c.getCate());
            pstm.executeUpdate();
            System.out.println("categorie ajouté");

        } catch (SQLException ex) {
            Logger.getLogger(CategorieDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }    
 public void supprimerCategorieById(int id) {
        String requete = "delete from categorie where id=?";
        try {
             pstm = cnn.prepareStatement(requete);
            pstm.setInt(1, id);
            pstm.executeUpdate();
            System.out.println("Categorie supprimé");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }
    }
    public void modifierCategorie(Categories c) {
        String requete = "update categorie set cate=? where id=?";
        try {
             pstm = cnn.prepareStatement(requete);
            pstm.setString(1, c.getCate());
            pstm.setInt(2, c.getId());
            pstm.executeUpdate();
            System.out.println("Mise à jour effectuée avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise à jour " + ex.getMessage());
        }
    }
    public ObservableList<Categories> findAll() {
        ObservableList<Categories> listecategories =  FXCollections.observableArrayList();
        String requete = "select * from categorie";
        try {
            Statement statement = cnn
                    .createStatement();
            ResultSet resultat = statement.executeQuery(requete);

            while (resultat.next()) {
                Categories c = new Categories();
                c.setId(resultat.getInt(1));
                c.setImage_id(resultat.getInt(2));
                c.setCate(resultat.getString(3));
                listecategories.add(c);
            }
            return listecategories;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des depots " + ex.getMessage());
            return null;
        }
    }
public Categories findCategoriesById(int id) {
        Categories categories = new Categories();
        String requete = "select * from categorie where id =?";
        try {
             pstm = cnn.prepareStatement(requete);
            pstm.setInt(1, id);
            ResultSet resultat = pstm.executeQuery();
            while (resultat.next()) {
                 categories.setId(resultat.getInt(1));
                categories.setImage_id(resultat.getInt(2));
                categories.setCate(resultat.getString(3));
            }
            return categories;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du categories " + ex.getMessage());
            return null;
        }
}
            public String SelectImage(Integer image_id) {
            String p = "";
        String requete = "select path from image where id=?";
        try {
            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setInt(1, image_id);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
          
                      p=  (resultat.getString(1));
              
            }
            return p;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return p;
        }
    }
    
}
