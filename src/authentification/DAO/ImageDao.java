/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;

import authentification.ENTITIES.ImageM;
import authentification.ENTITIES.VideoM;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ImageDao {
    private Connection cnn;

    public ImageDao() {
        cnn = MyConnection.getInstance();
    }


    public void addImage(ImageM p) {
        String req = "insert into image (updated_at,name,path) values (?,?,?)";
        try {
            PreparedStatement ps = cnn.prepareStatement(req);
             ps.setTimestamp(1, java.sql.Timestamp.from(java.time.Instant.now()));
ps.setTimestamp(1, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
            ps.setString(2, p.getName());
            ps.setString(3, p.getPath());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ImageDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
//     public void ajouterMedia2(ImageM p, Post C) {
//   
//
//               // Ajout Commentaire
//     
//        String queryComment = "insert into poste (Titre,Contenu, image_id,v_id,date_published,user) values (?,?,(select id from image ORDER BY id DESC LIMIT 1),(select id from video ORDER BY id DESC LIMIT 1),?,?)";
//        
//        try{
//              PreparedStatement ps2 = cnx.prepareStatement(queryComment);
//            ps2.setString(1, C.getTitre());
//               ps2.setString(2, C.getContenu());
//                     ps2.setTimestamp(3, java.sql.Timestamp.from(java.time.Instant.now()));
//ps2.setTimestamp(3, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
//ps2.setString(4, C.getUser());
//        
//            ps2.executeUpdate();
//     
//        }catch (SQLException exception) {
//            Logger.getLogger(PostDao.class.getName()).log(Level.SEVERE, null, exception);
//        }   
//  
//    }
     
          public void removeImageById(int id) {
        String requete = "delete from image where id=?";
        try {
            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setInt(1, id);
            ps.executeUpdate();
            System.out.println("Image supprimé");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }
    }
          
 public void updaImageById(ImageM p) {
        String requete = "update image set updated_at=?,name=?,path=? where id=?";
        try {
            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setTimestamp(1, java.sql.Timestamp.from(java.time.Instant.now()));
            ps.setTimestamp(1, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
            ps.setString(2, p.getName());
            ps.setString(3, p.getPath());
            ps.setInt(4, p.getId());
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise ajours de vedio " + ex.getMessage());
        }}
    
}
