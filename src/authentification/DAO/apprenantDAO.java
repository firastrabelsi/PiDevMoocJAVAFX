package authentification.DAO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import authentification.IDAO.IapprenantDAO;
import authentification.ENTITIES.apprenant;
import authentification.UTIL.MyConnection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class apprenantDAO implements IapprenantDAO {

    Connection cnx;

    public apprenantDAO() {
        cnx = MyConnection.getInstance();
    }

    //ajouter apprenant
    @Override
    public int insertApprenant(apprenant appr) {

        String query = "INSERT INTO utilisateur(username,nom,prenom,email,password,enabled,username_canonical, email_canonical,salt, locked, expired, roles, credentials_expired,etat,adresse,nomDeLaSociete,url,note,specialite,imageName,etat_c,updatedAt,site,num,certificat )VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try {

            PreparedStatement Pst = cnx.prepareStatement(query);

            Pst.setString(1, appr.getUserName());
            Pst.setString(2, appr.getNom());
            Pst.setString(3, appr.getPrenom());
            Pst.setString(4, appr.getEmail());
            Pst.setString(5, appr.getPassword());
            Pst.setInt(6, 1);
            Pst.setString(7, appr.getUserName());
            Pst.setString(8, appr.getEmail());
            Pst.setString(9, "");
            Pst.setInt(10, 0);
            Pst.setInt(11, 0);
            Pst.setString(12, "a:1:{i:0;s:14:\"ROLE_APPRENANT\";}");

            Pst.setInt(13, 0);
            Pst.setString(14, "");
            Pst.setString(15, "");
            Pst.setString(16, "");
            Pst.setString(17, "");
            Pst.setInt(18, 0);
            Pst.setString(19, "");
            Pst.setString(20, "");
            Pst.setInt(21, 0);
            Pst.setDate(22, new Date(0, 0, 0));
            Pst.setString(23, "");
            Pst.setInt(24, 0);
            Pst.setString(25, "");
            Pst.executeUpdate();
            System.out.println("Apprenant  ajouté!");
            return 1;

        } catch (SQLException ex) {
            System.out.println("Apprenant non ajouté!" + "******" + ex);
            return 0;
        }

    }

    public List<apprenant> getApprenantEnCours() {
        List<apprenant> apprenants = new ArrayList<apprenant>();

        String requete = "select * from utilisateur where enabled= 0";
        try {
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            while (rs.next()) {
                apprenant f = new apprenant();
                f.setId(Integer.parseInt(rs.getString("id")));
                f.setEmail(rs.getString("email"));
                // f.setEtat(Integer.parseInt(rs.getString("enabled")));
                f.setNom(rs.getString("nom"));
                f.setPrenom(rs.getString("prenom"));
                f.setUserName(rs.getString("username"));
                f.setPassword(rs.getString("password"));
                apprenants.add(f);

            }
        } catch (Exception e) {
        }
        return apprenants;
    }

    //modifier apprenant
    @Override
    public void updateApprenant(apprenant appr, int id) {

        String query = "UPDATE utilisateur SET userName=? ,nom=? ,prenom=? ,email=? ,password=?,username_canonical=?,email_canonical=?  WHERE id=" + id;

        try {
            PreparedStatement Pst = cnx.prepareStatement(query);

            Pst.setString(1, appr.getUserName());
            Pst.setString(2, appr.getNom());
            Pst.setString(3, appr.getPrenom());
            Pst.setString(4, appr.getEmail());
            Pst.setString(5, appr.getPassword());
            Pst.setString(6, appr.getUserName());
            Pst.setString(7, appr.getEmail());

            Pst.executeUpdate();
            System.out.println("success");

        } catch (SQLException ex) {
            System.out.println(" une Erreur s'est produite lors  de la modification!!!");

        }
    }

    //suprimer Apprenant
    @Override
    public void deleteApprenant(int id) {
        String requete = "DELETE FROM utilisateur WHERE id=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setInt(1, id);
            ps.executeUpdate();
            System.out.println("Apprenant supprimée");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }

    }

    @Override
    public apprenant findApprenantByEmail(String Email) {
        apprenant appr = new apprenant();
        String requete = "select * from utilisateur where Email=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, Email);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                appr.setId(resultat.getInt(1));
                appr.setUserName(resultat.getString(2));
                appr.setNom(resultat.getString(3));
                appr.setPrenom(resultat.getString(4));
                appr.setEmail(resultat.getString(5));
                appr.setPassword(resultat.getString(6));

            }
            return appr;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

   

    @Override
    public String veriflogin(String username, String email) {
        String role = "";
        String h = "tarartarat";
        apprenant appr = new apprenant();
        String requete = "select roles from utilisateur where username=? And email=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, username);
            ps.setString(2, email);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                role = resultat.getString(1);

            }
            if (role != "null") {
                return role;
            } else {
                return h;
            }
        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise à jour " + ex.getMessage());
            return null;
        }
    }

//////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////ADMINISTRATEUR/////////////////////////////////////
    @Override
    public apprenant findApprenantByUserName(String UserName) {
        apprenant appr = new apprenant();
        String requete = "select * from utilisateur where UserName=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, UserName);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                appr.setId(resultat.getInt(1));
                appr.setUserName(resultat.getString(2));
                appr.setNom(resultat.getString(3));
                appr.setPrenom(resultat.getString(4));
                appr.setEmail(resultat.getString(5));
                appr.setPassword(resultat.getString(6));

            }
            return appr;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du circuit " + ex.getMessage());
            return null;
        }
    }

    ////////////////////////ADMIN APPRENANT////////////////////////////
    //aficher la liste de tt les Apprenant
    @Override
    public ArrayList<apprenant> displayAllApprenant() {
        ArrayList<apprenant> listeApprenant = new ArrayList<>();

        String requete = "select * from utilisateur";
        try {
            String Role1;
            String ROLE;
            Statement statement = cnx.createStatement();
            ResultSet resultat = statement.executeQuery(requete);

            while (resultat.next()) {

                apprenant appr = new apprenant();
                appr.setId(resultat.getInt(1));
                appr.setUserName(resultat.getString(2));
                appr.setNom(resultat.getString(18));
                appr.setPrenom(resultat.getString(19));
                appr.setEmail(resultat.getString(4));
                appr.setPassword(resultat.getString(8));
                Role1 = this.veriflogin(appr.getUserName(), appr.getEmail());

                System.out.println(Role1);
                ROLE = Role1.substring(15, 25);
                System.out.println(ROLE);
                if (ROLE.equals("ROLE_APPRE")) {
                    listeApprenant.add(appr);
                    System.out.println(appr);
                } else {
                }
                // listeApprenant.add(appr);

            }
            return listeApprenant;

        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement de la liste des Apprenants " + ex.getMessage());
            return null;
        }

    }

    //////////////////////////////////////////////Annouar //////////////////
    @Override
    public apprenant findApprenantById(int id) {
        apprenant appr = new apprenant();
        String requete = "select * from utilisateur where id=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                appr.setId(resultat.getInt(1));
                appr.setUserName(resultat.getString(2));
                appr.setNom(resultat.getString(3));
                appr.setPrenom(resultat.getString(4));
                appr.setEmail(resultat.getString(5));
                appr.setPassword(resultat.getString(6));

            }
            return appr;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du circuit " + ex.getMessage());
            return null;
        }
    }

    
    
     @Override
    public int findApprenantId(String Username) {
        int Id = 0;
        apprenant appr = new apprenant();
        String requete = "select id from utilisateur where username=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, Username);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                // id.setId(resultat.getInt(1));
                Id = resultat.getInt(1);

            }
            return Id;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return 0;
        }

    }
   
}
