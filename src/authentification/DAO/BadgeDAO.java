/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;

import authentification.ENTITIES.Commentaire;
import authentification.UTIL.MyConnection;
import javafx.scene.image.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bali Majdi
 */
public class BadgeDAO {

    private Connection cnn;
    private Statement stm;
    private PreparedStatement pstm;
    private FileInputStream fis;
    private FileInputStream fis2;
    private FileInputStream fis3;
    Date d = new Date();
    static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public BadgeDAO() {
        cnn = new MyConnection().getInstance();
    }

    public void badge2(List<File> fl) {

        String requete = "insert into badge (b1,b2,b3) values (?,?,?)";

        try {

            PreparedStatement ps = cnn.prepareStatement(requete);
            try {
                File file;
                file = fl.get(0);
                fis = new FileInputStream(file);
                File file2;
                file2 = fl.get(1);
                fis2 = new FileInputStream(file2);

                File file3;
                file3 = fl.get(2);
                fis3 = new FileInputStream(file3);

            } catch (FileNotFoundException ex) {
                Logger.getLogger(BadgeDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ps.setBinaryStream(1, (InputStream) fis);
            ps.setBinaryStream(2, (InputStream) fis2);
            ps.setBinaryStream(3, (InputStream) fis3);

            ps.executeUpdate();
            System.out.println("Ajout effectué avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de l'insertion " + ex.getMessage());
        }

    }

    public void badge(File file) {

        String requete = "insert into badge (b1) values (?)";
        try {

            PreparedStatement ps = cnn.prepareStatement(requete);
            try {
                fis = new FileInputStream(file);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BadgeDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ps.setBinaryStream(1, (InputStream) fis);

            ps.executeUpdate();
            System.out.println("Ajout effectué avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de l'insertion " + ex.getMessage());
        }
    }

    public Image getBagde(int id, int score) throws IOException {
        Image image = null;
        String requete = "select * from badge where id=?";
        try {

            PreparedStatement ps = cnn.prepareStatement(requete);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {

                if (score >= 50 && score < 75) {
                    InputStream is = resultat.getBinaryStream("b3");
                    OutputStream os = new FileOutputStream(new File("photo.jpg"));
                    byte[] content = new byte[1024];
                    int size = 0;
                    while ((size = is.read(content)) != -1) {
                        os.write(content, 0, size);
                    }
                    os.close();
                    is.close();

                    image = new Image("file:photo.jpg", 50, 50, true, true);
                } else if (score >= 75 && score < 100) {
                    InputStream is = resultat.getBinaryStream("b2");
                    OutputStream os = new FileOutputStream(new File("photo.jpg"));
                    byte[] content = new byte[1024];
                    int size = 0;
                    while ((size = is.read(content)) != -1) {
                        os.write(content, 0, size);
                    }
                    os.close();
                    is.close();

                    image = new Image("file:photo.jpg", 50, 50, true, true);
                } else if (score == 100) {
                    InputStream is = resultat.getBinaryStream("b1");
                    OutputStream os = new FileOutputStream(new File("photo.jpg"));
                    byte[] content = new byte[1024];
                    int size = 0;
                    while ((size = is.read(content)) != -1) {
                        os.write(content, 0, size);
                    }
                    os.close();
                    is.close();

                    image = new Image("file:photo.jpg", 50, 50, true, true);
                }

            }

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
        return image;
    }

    public void UpdateBadge(int id, List<File> fl) {
        String requete = "update badge set b1=?, b2=?, b3=? where id=?";
        try {

            PreparedStatement ps = cnn.prepareStatement(requete);
            try {
                File file;
                file = fl.get(0);
                fis = new FileInputStream(file);
                File file2;
                file2 = fl.get(1);
                fis2 = new FileInputStream(file2);

                File file3;
                file3 = fl.get(2);
                fis3 = new FileInputStream(file3);

            } catch (FileNotFoundException ex) {
                Logger.getLogger(BadgeDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            ps.setBinaryStream(1, (InputStream) fis);
            ps.setBinaryStream(2, (InputStream) fis2);
            ps.setBinaryStream(3, (InputStream) fis3);
            ps.setInt(4, id);

            ps.executeUpdate();
            System.out.println("mise à jours effectué avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de mise à jours " + ex.getMessage());
        }
    }

}
