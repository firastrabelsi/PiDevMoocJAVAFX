/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;

import authentification.ENTITIES.Cours;
import authentification.ENTITIES.SuivCours;
import authentification.GUI.LoginController;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SuivCoursDAO {

    private Connection cnn;
    private PreparedStatement pstm;
    apprenantDAO apdao = new apprenantDAO();
    

    public SuivCoursDAO() {
        cnn =MyConnection.getInstance();

    }

    public void ajouterSuivCours(SuivCours c) {
        try {
            pstm = cnn.prepareStatement("INSERT into suivicours (cours_id,utilisateur,dateinscrip) values (?,?,?)");
            pstm.setInt(1, c.getCours().getId());
            System.out.println("zzzzzzzzzzzzz" + c.getCours().getId());
            pstm.setInt(2, apdao.findApprenantByUserName(LoginController.USERNAME).getId());
            System.out.println("zzzzzzzzzzzzz" + c.getApprenant().getId());

            pstm.setTimestamp(3, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));

            pstm.executeUpdate();
            System.out.println("suiv ajouté");

        } catch (SQLException ex) {
            Logger.getLogger(CategorieDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void supprimersuivById(int id) {
        String requete = "delete from suivicours where id=?";
        try {
            pstm = cnn.prepareStatement(requete);
            pstm.setInt(1, id);
            pstm.executeUpdate();
            System.out.println("suiv supprimé");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }
    }

    public ObservableList<SuivCours> listsuivcours() {
        ObservableList<SuivCours> listecours = FXCollections.observableArrayList();
        String requete = "select * from suivicours  ";
        try {
            pstm = cnn.prepareStatement(requete);

            ResultSet resultat = pstm.executeQuery();
            CoursDAO coursdao = new CoursDAO();

            while (resultat.next()) {
                SuivCours c = new SuivCours();
                apprenantDAO ap = new apprenantDAO();
                c.setId(resultat.getInt(1));

                c.setCours(coursdao.rechercherCoursbyid(resultat.getInt(2)));
                c.setApprenant(ap.findApprenantById(resultat.getInt(3)));
                c.setNomcors(coursdao.rechercherCoursbyid(resultat.getInt(2)).getTitre());
                c.setNomappren(ap.findApprenantById(resultat.getInt(3)).getNom());

                listecours.add(c);
            }
            return listecours;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des cours non valider " + ex.getMessage());
            return null;
        }
    }

    public ObservableList<SuivCours> listsuivcoursvaliderformatorgan(int idc) {
        ObservableList<SuivCours> listecours = FXCollections.observableArrayList();
        String requete = "select * from suivicours where cours_id=?  ";
        try {
            pstm = cnn.prepareStatement(requete);
            pstm.setInt(1, idc);

            ResultSet resultat = pstm.executeQuery();
            CoursDAO coursdao = new CoursDAO();
            apprenantDAO ap = new apprenantDAO();
            SuivCours c = new SuivCours();

            while (resultat.next()) {
                c.setId(resultat.getInt(1));
                c.setNomcors(coursdao.rechercherCoursbyid(resultat.getInt(2)).getTitre());
                c.setNomappren(ap.findApprenantById(resultat.getInt(3)).getNom());

                listecours.add(c);
            }
            return listecours;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des cours non valider " + ex.getMessage());
            return null;
        }
    }

    public List<SuivCours> listsuivcoursApprenant() {
        List<SuivCours> listecours = new ArrayList<SuivCours>();
        String requete = "select id,cours_id,utilisateur from suivicours where utilisateur = 6 ";
        try {
            pstm = cnn.prepareStatement(requete);
            // pstm.setInt(1, idc);

            ResultSet resultat = pstm.executeQuery();
            CoursDAO coursdao = new CoursDAO();
            apprenantDAO ap = new apprenantDAO();
            SuivCours c = new SuivCours();

            while (resultat.next()) {
                c.setId(resultat.getInt(1));
                c.setCours(coursdao.rechercherCoursbyid(resultat.getInt(2)));
                c.setApprenant(ap.findApprenantById(resultat.getInt(3)));
                c.setNomcors(coursdao.rechercherCoursbyid(resultat.getInt(2)).getTitre());
                c.setNomappren(ap.findApprenantById(resultat.getInt(3)).getNom());

                listecours.add(c);
            }
            return listecours;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des cours non valider " + ex.getMessage());
            return null;
        }
    }

}
