/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;

import authentification.ENTITIES.Reclamation;
import authentification.IDAO.IReclamationDao;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class ReclamationDao implements IReclamationDao {

    private Connection cnx;

    public ReclamationDao() {
        cnx = MyConnection.getInstance();
    }

    @Override
    public void insertObject(Reclamation val) {
        String requete = "insert into reclamation (message) values (?)";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, val.getMessage());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReclamationDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public Reclamation getlastReclamation() {
        String sql = "SELECT * FROM reclamation ORDER BY id DESC LIMIT 1";
        Reclamation r = new Reclamation();
        try {
            PreparedStatement stmt = cnx.prepareStatement(sql);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                r.setIdRec((int) result.getLong(1));
                r.setMessage(result.getString(2));
            } else {
                //System.out.println("Unable to find primary-key for created object!");
                throw new SQLException("Unable to find primary-key for created object!");
            }
        } catch (Exception e) {

        }

        return r;
    }
}

