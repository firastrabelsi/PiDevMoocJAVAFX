/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;

import authentification.ENTITIES.Formateur;
import authentification.IDAO.IFormateurDAO;
import authentification.DAO.FormateurDAO;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aymen
 */
public class FormateurDAO implements IFormateurDAO{

    private Connection cnx;

    public FormateurDAO()
    {
    cnx = MyConnection.getInstance(); 
    }
    
    @Override
    public void inscritFormateur(Formateur f) {
        String req="insert into utilisateur (nom, prenom, email, specialite, enabled, username, password, username_canonical, email_canonical,"
                + "salt, locked, expired, roles, credentials_expired, imageName, updatedAt ,etat_c,etat,adresse,nomDeLaSociete,url,site,num,certificat,note) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
     PreparedStatement ps=cnx.prepareStatement(req);
     ps.setString(1, f.getNom());
     ps.setString(2, f.getPrenom());
     ps.setString(3, f.getEmail());
     ps.setString(4, f.getSpecialite());
     ps.setInt(5, f.getEtat());
     ps.setString(6, f.getUsername());
     ps.setString(7, f.getPassword());
     
     ps.setString(8, f.getUsername());
     ps.setString(9, f.getEmail());
     ps.setString(10, "lkjbjk");
     ps.setInt(11, 0);
     ps.setInt(12, 0);
     ps.setString(13, "a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}");
     ps.setInt(14, 0);
     //ps.setString(15, "");
     
     //ps.setDate(16, new Date(0, 0, 0));
     ps.setString(15, f.getPath());
     ps.setTimestamp(16, java.sql.Timestamp.from(java.time.Instant.now()));
     ps.setTimestamp(16, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
     
     
     ps.setInt(17, 0);
     ps.setString(18, "");
     ps.setString(19, "");
     ps.setString(20, "");
     ps.setString(21, "");
     ps.setString(22, "");
     ps.setInt(23, 0);
     ps.setString(24, "");
     ps.setInt(25, 0);
     
     
     
     ps.executeUpdate();
     
        } catch (SQLException ex) {
            System.out.println(ex);        }
    }

    @Override
    public List<Formateur> getFormateurEnCours() {
        List<Formateur> formateurs = new ArrayList<Formateur>();
        
        String requete = "select * from utilisateur where enabled = 0  and roles = 'a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}'";
        try {
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            while (rs.next()) {  
                Formateur f = new Formateur();
                f.setId(Integer.parseInt(rs.getString("id")));
                f.setEmail(rs.getString("email"));
                f.setEtat(Integer.parseInt(rs.getString("enabled")));
                f.setNom(rs.getString("nom"));
                f.setPrenom(rs.getString("prenom"));
                f.setUsername(rs.getString("username"));
                f.setSpecialite(rs.getString("specialite"));
                f.setPassword(rs.getString("password"));
                f.setPath(rs.getString(29));
                formateurs.add(f);
                
            }
        } catch (Exception e) {
        }
return formateurs;
    }

    @Override
    public void accepteFormateur(Formateur f) {
        String requete = "update utilisateur set enabled = 1 where roles = 'a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}' and id ="+f.getId() ;
        
        try {
            PreparedStatement ps=cnx.prepareStatement(requete);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public void refuseFormateur(Formateur f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void aceepteToCommitte( Formateur f) {
            String requete = "update utilisateur set roles = 'a:1:{i:0;s:14:\"ROLE_COMITE\";}' , etat_c= 1  where note = 10 ";
        try {
            PreparedStatement ps=cnx.prepareStatement(requete);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }

    }
    

    @Override
    public void editFormateur(Formateur f, int idF) {
        String requete = "update utilisateur set nom = ? , prenom = ? , username = ? , email= ?  where id ="+idF;
        
        try {
            PreparedStatement ps=cnx.prepareStatement(requete);
            ps.setString(1, f.getNom());
            ps.setString(2, f.getPrenom());
            ps.setString(3, f.getUsername());
            ps.setString(4, f.getEmail());
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public void removeFormateur(int id) {
    String requete = "delete from utilisateur where id=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setInt(1, id);
            ps.executeUpdate();
            System.out.println("Formateur supprimé");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }
    }

    @Override
    public String veriflogin1(String username, String Email) {
         String role="";
        String h="tarartarat";
        Formateur forma = new Formateur();
         String requete = "select roles from utilisateur where username=? And email=? And enabled = 1";
          try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, username);
            ps.setString(2, Email);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                role=resultat.getString(1);

            }
            if (role!="null")
            {return role;}
            else
            { return h;}
        }catch (SQLException ex) {
            System.out.println("erreur " + ex.getMessage());
            return null;
        }
        
    }

    @Override
    public List<Formateur> addToComite() {
       List<Formateur> formateurs = new ArrayList<Formateur>();
        
        String requete = "select * from utilisateur where note = 10 and etat_c = 0  ";
        try {
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            while (rs.next()) {  
                Formateur f = new Formateur();
                f.setId(Integer.parseInt(rs.getString("id")));
                f.setEmail(rs.getString("email"));
                f.setEtat(Integer.parseInt(rs.getString("enabled")));
                f.setNote(Integer.parseInt(rs.getString("note")));
                f.setNom(rs.getString("nom"));
                f.setPrenom(rs.getString("prenom"));
                f.setUsername(rs.getString("username"));
                f.setPassword(rs.getString("password"));
                formateurs.add(f);
                
            }
        } catch (Exception e) {
        }
         return formateurs;
    }

    @Override
    public Formateur findFormateurByUserName(String UserName) {
        Formateur format = new Formateur();
        String requete = "SELECT id,username,nom,prenom,email,password FROM utilisateur WHERE username=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, UserName);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                format.setId(resultat.getInt(1));
                format.setUsername(resultat.getString(2));
                format.setNom(resultat.getString(3));
                format.setPrenom(resultat.getString(4));
                format.setEmail(resultat.getString(5));
                format.setPassword(resultat.getString(6));
                System.out.println(format);
            }
            return format;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du formateur " + ex.getMessage());
            return null;
        }
    }

    @Override
    public int findFormateurId(String Username) {
        int id = 0;
        Formateur format = new Formateur();
        String requete = "select id from utilisateur where username=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, Username);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
              // id.setId(resultat.getInt(1));
                id=resultat.getInt(1);

            }
            return id ;
    }   catch (SQLException ex) {
            System.out.println("erreur  " + ex.getMessage());
            return 0;
        }

    }

    
    @Override
    public int verifEtat(String username, String email) {
        int etat=0;
        String h="tarartarat";
         Formateur f = new Formateur();
        String requete = "select enabled from utilisateur where username=? And email=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, username);
            ps.setString(2, email);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                etat=resultat.getInt(1);

            }
            if (etat!=0)
            {return etat;}
            else
            { return 0;}
        } catch (SQLException ex) {
            System.out.println("erreur lors de la récuperation à jour " + ex.getMessage());
            return 0;
        }
    }

    @Override
    public void editComite(Formateur f, int idF) {
        
        String requete = "update utilisateur set nom = ? , prenom = ? , username = ? , email= ?  where roles = 'a:1:{i:0;s:14:\"ROLE_COMITE\";}' AND id ="+idF;
        
        try {
            PreparedStatement ps=cnx.prepareStatement(requete);
            ps.setString(1, f.getNom());
            ps.setString(2, f.getPrenom());
            ps.setString(3, f.getUsername());
            ps.setString(4, f.getEmail());
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        
    }
    

    
    

 
}

