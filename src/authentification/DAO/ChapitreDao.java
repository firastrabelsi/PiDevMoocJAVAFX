/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentification.DAO;

import authentification.ENTITIES.Chapitre;
import authentification.IDAO.IChapitreDao;
import authentification.UTIL.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class ChapitreDao implements IChapitreDao {

    private Connection cnx;

    public ChapitreDao() {
        cnx = MyConnection.getInstance();
    }

    @Override
    public void insertObject(Chapitre val) {
        String requete = "insert into chapitre (Nom,Description,id_cours) values (?,?,?)";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, val.getNom());
            ps.setString(2, val.getDescription());
            ps.setInt(3, val.getCour().getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ChapitreDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void updateObject(Chapitre val) {
        String requete = "update chapitre set Nom = ?,Description =? where idChap =" + val.getId();
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setString(1, val.getNom());
            ps.setString(2, val.getDescription());
            ps.executeUpdate();
            System.out.println("Mise à jour effectuée avec succès");

        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise à jour " + ex.getMessage());
        }
    }

    @Override
    public void deleteObject(Chapitre val) {
        String requete = "delete from chapitre where idChap = ?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setInt(1, val.getId());
            ps.executeUpdate();
            System.out.println("chapitre supprimée");

        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }

    }

    @Override
    public Chapitre findObjectById(int id) {
        Chapitre ch = new Chapitre();
        String requete = "select * from chapitre where idChap = ?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                ch.setId(resultat.getInt(1));
                ch.setNom(resultat.getString(2));
                ch.setDescription(resultat.getString(3));

            }
            return ch;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du chapitre " + ex.getMessage());
            return null;
        }

    }

    @Override
    public List<Chapitre> DisplayAllObject() {
        List<Chapitre> listtests = new ArrayList<>();
        String requete = "select * from chapitre";
        try {
            Statement statement = cnx.createStatement();
            ResultSet resultat = statement.executeQuery(requete);
            while (resultat.next()) {
                Chapitre ch = new Chapitre();
                ch.setId(resultat.getInt(1));
                ch.setNom(resultat.getString(2));
                ch.setDescription(resultat.getString(3));

                listtests.add(ch);
            }
            return listtests;

        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des chapitres " + ex.getMessage());
            return null;
        }

    }

    @Override
    public List<Chapitre> DisplayAllChapCours(int id) {
        List<Chapitre> listtests = new ArrayList<>();
        String requete = "select * from chapitre where id_cours=?";
        try {
            PreparedStatement ps = cnx.prepareStatement(requete);
            ps.setInt(1, id);

            ResultSet resultat = ps.executeQuery();

            while (resultat.next()) {
                Chapitre ch = new Chapitre();
                ch.setId(resultat.getInt(1));
                ch.setNom(resultat.getString(2));
                ch.setDescription(resultat.getString(3));
                CoursDAO coursDAO = new CoursDAO();
                ch.setCour(coursDAO.findObjectById(resultat.getInt(4)));

                listtests.add(ch);
            }
            return listtests;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement des chapitres " + ex.getMessage());
            return null;
        }

    }

}
